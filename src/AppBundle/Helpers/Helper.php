<?php
namespace AppBundle\Helper;

use Ddeboer\DataImport\Reader\CsvReader;
use Ddeboer\DataImport\Reader\ExcelReader;


class Helper
{
    const excel_ext = '.xlsx';
    
    const csv_ext = '.csv';
    
    const file_name = 'Executari';
    
    private $validTypeMapping = array(
            'asset_t1' => 'Imobil',
            'asset_t2' => 'Mobil'
            );
    
    public function getFileReader($format)
    {
        if('excel' == $format)
        {
            $file = new \SplFileObject(self::file_name . self::excel_ext);
            return new ExcelReader($file);
        }
        
        $file = new \SplFileObject(self::file_name . self::csv_ext);
        
        return new CsvReader($file);
    }
    
    public function getValidType($index) {
        $response = '';
        
        if(!empty(array_key_exists($this->validTypeMapping, $index)))
        {
            $response = $this->validTypeMapping[$index];
        }
        return $response;
    }
    
    
}