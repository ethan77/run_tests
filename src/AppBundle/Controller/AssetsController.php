<?php

namespace AppBundle\Controller;

require_once '../../vendor/autoload.php';
use Ddeboer\DataImport\Reader\CsvReader;
use Ddeboer\DataImport\Reader\ExcelReader;
use Symfony\Component\Config\Definition\Exception\Exception;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Helpers\ControllerHelper;
use Symfony\Component\HttpFoundation\ParameterBag;

class AssetsController extends Controller
{
    
    public function listAssetsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $product = Helper::getValidProduct($assetType);
        if(!empty($product)) {
            $fields = array_values($em->getClassMetadata('AppBundle:' . $product)->getFieldNames());
            $queryParams = array();
            
            foreach($fields as $key_name) {
                if(!empty($request->request->get($key_name))) {
                    $queryParams[$key_name] = $request->request->get($key_name);
                }
            }
            
//        $status = ControllerHelper::getMappedStatus($request->query->get('status',0));
//        $type = ControllerHelper::getMappedStatus($request->query->get('type',0));         
//        $sort_by = ControllerHelper::getSortingOrder($request->request->get('sort_by',0));

            $assets     = $em->getRepository('AppBundle:'.$product)->findBy($queryParams);
            $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $assets,
                $request->query->get('page', 1),
                10
            );
                                
            return $this->render('AcmeMainBundle:Article:list_' . $product . '.html.twig', array('pagination' => $pagination));     
        }
        //render some default page
    }
     
    public function showAssetDetailsAction()
    {
        $assetId = $request->query->get('id');
        $productName = Helper::getValidProduct($assetType);

        if(!empty($assetId) && !empty($productName)) {
            $em = $this->getDoctrine()->getManager();
            $asset = $em->getRepository('AppBundle:' . $productName )->findOneBy(array('id' => $assetId));

            return $this->render('executari/details_' . $productName .'.html.twig', array('asset' => $asset));            
        }
        
        /// redirect to some default pahe
    }
    
    public function sendOffer()
    {
        
    }
    
    //ajax called route
    public function addToFavourites(Request $request)
    {
        $assetId = $request->query->get('id');
        $assetType = $request->query->get('asset');
        if(!empty($assetId)) {
            $em = $this->getDoctrine()->getManager();
            $asset = $em->getRepository('AppBundle:Imobil')->findOneBy(array('id' => $assetId));
            if(!empty($asset)) {
                $em = $this->getEntityManager();
                
                $sql = $em->createQuery(
                        'INSERT INTO MAP_FAV (usserId, assetId, assetType) '
                        . 'values(:userId, :assetId, :assetType)');
                
                $params['userId'] = $userId; 
                $params['assetId'] = $assetId;
                $params['assetType'] = $assetType;
                
                $em = $this->entityManager->getConnection()->prepare($sql);
                
                $stmt->execute($params);
                
                $stmt->fetchAll(PDO::FETCH_COLUMN);
            }
        }
    }
    
    
    public function getFavouritesAction() {
        if(!empty($assetId)) {
            $em = $this->getDoctrine()->getManager();
            $asset = $em->getRepository('AppBundle:Imobil')->findOneBy(array('id' => $assetId));
            $user = $this->container->get('security.context')->getToken()->getUser();
            
            $em = $this->getEntityManager();
                
            $sql = $em->createQuery(
                    'SELECT assetId, assetType FROM MAP_FAV join '
                    . ''
                    . 'INTO MAP_FAV (usserId, assetId, assetType) '
                    . 'values(:userId, :assetId, :assetType)');
                
            $params['userId'] = $userId; 
            $params['assetId'] = $assetId;
            $params['assetType'] = $assetType;
                
            $em = $this->entityManager->getConnection()->prepare($sql);
                
            $stmt->execute($params);
                
             $stmt->fetchAll(PDO::FETCH_COLUMN);
             
             //return ajax response json
        }
    }
    
    /**
     * @Route("imobil/importAssets", name="list")
     */
    public function importAssetsAction(Request $request)
    {   
        $fileFormat = $request->get('format');
        $assetType =  $request->get('assetType');

        $reader = Helper::getFileReader($fileFormat);    
        $em = $this->getDoctrine()->getEntityManager();        
        $index = 1;       
        do 
            {
                $asset = Helper::getAsset($reader->getRow($index));            
                $em->persist($asset);
                $em->flush();            
                $index++;
            } 
        while($index < $reader->count());
      
        return $this->render('default/import_assets.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
        ]);
    }
}