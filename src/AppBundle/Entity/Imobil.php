<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="imobil")
 */
class Imobil
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer") 
     */
    private $id;
    
    /**
     * @ORM\Column(type="string")
     */
    public $idImobil = null;
    
    /**
     * @ORM\Column(type="integer")
     */
    public $crtNum = null;
    
    /**
     * @ORM\Column(type="string")
     */
    public $judet = null;
    
    /**
     * @ORM\Column(type="string")
     */
    public $oras = null;
    
    /**
     * @ORM\Column(type="string")
     */
    public $adresa = null;
    
    /**
     * @ORM\Column(type="string")
     */
    public $tipImobil = null;
    
    /**
     * @ORM\Column(type="string")
     */
    public $descriere = null;
    
    /**
     * @ORM\Column(type="decimal")
     */
    public $arieTeren = null;
    
    /**
     * @ORM\Column(type="decimal")
     */
    public $arieConstruita = null;
    
    /**
     * @ORM\Column(type="decimal")
     */
    public $arieUtila = null;
    
    /**
     * @ORM\Column(type="string")
     */
    public $stadiuImobil = null;
    
    /**
     * @ORM\Column(type="string")
     */
    public $modVanzare = null;
    
    /**
     * @ORM\Column(type="string")
     */
    public $numeExecutor = null;
    
    /**
     * @ORM\Column(type="integer")
     */
    public $nrDosarExecutor = null;
    
    /**
     * @ORM\Column(type="string")
     */
    public $numeLichidator = null;
    
    /**
     * @ORM\Column(type="integer")
     */
    public $nrDosarLichidator = null;
    
    /**
     * @ORM\Column(type="date")
     */
    public $dataLicitatie = null;
    
    /**
     * @ORM\Column(type="integer")
     */
    public $nrLicitatie = null;
    
    /**
     * @ORM\Column(type="float")
     */
    public $pretInitial = null;
    
    /**
     * @ORM\Column(type="float")
     */
    public $pretPornire = null;
    
    /**
     * @ORM\Column(type="string")
     */
    public $moneda = null;
   
    /**
     * @ORM\Column(type="boolean")
     */
    public $approved = null;
        
    /**
     * @ORM\Column(type="string")
     */
    public $intravilan = null;
    
    /**
     * @ORM\Column(type="decimal")
     */
    public $procentDiminuare = null;
    
    /**
     * @ORM\Column(type="float")
     */
    public $idMeta = null;
	   
    /**
     * @ORM\Column(type="boolean")
     */
    public $pending = null;
    
    /**
     * @ORM\Column(type="date")
     */
    public $dateInsert = null;
	
    /**
     * @ORM\Column(type="date")
     */
    public $dateUpdate = null;
    
    function getProcentDiminuare() {
        return $this->procentDiminuare;
    }
    
    function getApproved() {
        return $this->approved;
    }

    function setApproved($approved) {
        $this->approved = $approved;
    }

    function setProcentDiminuare($procentDiminuare) {
        $this->procentDiminuare = $procentDiminuare;
    }
        
    function getIdImobil() {
        return $this->idImobil;
    }

    function getIntravilan() {
        return $this->intravilan;
    }

    function setIdImobil($idImobil) {
        $this->idImobil = $idImobil;
    }

    function setIntravilan($intravilan) {
        $this->intravilan = $intravilan;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getCrtNum() {
        return $this->crtNum;
    }

    public function setCrtNum($crtNum) {
        $this->crtNum = $crtNum;
    }

    public function getJudet() {
        return $this->judet;
    }

    public function setJudet($judet) {
        $this->judet = $judet;
    }

    public function getOras() {
        return $this->oras;
    }

    public function setOras($oras) {
        $this->oras = $oras;
    }

    public function getAdresa() {
        return $this->adresa;
    }

    public function setAdresa($adresa) {
        $this->adresa = $adresa;
    }

    public function getTipImobil() {
        return $this->tipImobil;
    }

    public function setTipImobil($tipImobil) {
        $this->tipImobil = $tipImobil;
    }

    public function getDescriere() {
        return $this->descriere;
    }

    public function setDescriere($descriere) {
        $this->descriere = $descriere;
    }

    public function getArieTeren() {
        return $this->arieTeren;
    }

    public function setArieTeren($arieTeren) {
        $this->arieTeren = $arieTeren;
    }

    public function getArieConstruita() {
        return $this->arieConstruita;
    }

    public function setArieConstruita($arieConstruita) {
        $this->arieConstruita = $arieConstruita;
    }

    public function getArieUtila() {
        return $this->arieUtila;
    }

    public function setArieUtila($arieUtila) {
        $this->arieUtila = $arieUtila;
    }

    public function getStadiuImobil() {
        return $this->stadiuImobil;
    }

    public function setStadiuImobil($stadiuImobil) {
        $this->stadiuImobil = $stadiuImobil;
    }

    public function getModVanzare() {
        return $this->modVanzare;
    }

    public function setModVanzare($modVanzare) {
        $this->modVanzare = $modVanzare;
    }

    public function getNumeExecutor() {
        return $this->numeExecutor;
    }

    public function setNumeExecutor($numeExecutor) {
        $this->numeExecutor = $numeExecutor;
    }

    public function getNrDosarExecutor() {
        return $this->nrDosarExecutor;
    }

    public function setNrDosarExecutor($nrDosarExecutor) {
        $this->nrDosarExecutor = $nrDosarExecutor;
    }

    public function getNumeLichidator() {
        return $this->numeLichidator;
    }

    public function setNumeLichidator($numeLichidator) {
        $this->numeLichidator = $numeLichidator;
    }

    public function getNrDosarLichidator() {
        return $this->nrDosarLichidator;
    }

    public function setNrDosarLichidator($nrDosarLichidator) {
        $this->nrDosarLichidator = $nrDosarLichidator;
    }

    public function getDataLicitatie() {
        return $this->dataLicitatie;
    }

    public function setDataLicitatie($dataLicitatie) {
        $this->dataLicitatie = $dataLicitatie;
    }

    public function getNrLicitatie() {
        return $this->nrLicitatie;
    }

    public function setNrLicitatie($nrLicitatie) {
        $this->nrLicitatie = $nrLicitatie;
    }

    public function getPretInitial() {
        return $this->pretInitial;
    }

    public function setPretInitial($pretInitial) {
        $this->pretInitial = $pretInitial;
    }

    public function getPretPornire() {
        return $this->pretPornire;
    }

    public function setPretPornire($pretPornire) {
        $this->pretPornire = $pretPornire;
    }

    public function getMoneda() {
        return $this->moneda;
    }

    public function setMoneda($moneda) {
        $this->moneda = $moneda;
    }
    
    function getIdMeta() {
        return $this->idMeta;
    }

    function getPending() {
        return $this->pending;
    }

    function getDateInsert() {
        return $this->dateInsert;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function setIdMeta($idMeta) {
        $this->idMeta = $idMeta;
    }

    function setPending($pending) {
        $this->pending = $pending;
    }

    function setDateInsert($dateInsert) {
        $this->dateInsert = $dateInsert;
    }

    function setDateUpdate($dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }
}