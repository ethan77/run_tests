<?php

namespace UserBundle\Controller;

require_once '../../vendor/autoload.php';

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('UserBundle:Default:index.html.twig');
    }
    
    public function loginAction()
    {
        return $this->render('UserBundle:Security:login.html.twig');
    }
}
