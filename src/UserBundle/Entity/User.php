<?php
namespace UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    protected $favourites;
    
    public function __construct()
    {
        parent::__construct();
        // your own logic
    }
    
    /**
     * Get the most significant role
     *
     * @return string 
     */
    public function getRole()
    {
        if(in_array('ROLE_ADMIN', $this->roles)) $role = 'Administrator';
        else if(in_array('ROLE_MANAGER', $this->roles)) $role = 'Manager';
        else $role = 'User';
        return $role;
    }
    
    public function getParent()
    {
        return 'FOSUserBundle';
    }
    
    public function getFavourites()
    {
        
    }
}