<?php

/* head.html.twig */
class __TwigTemplate_c1648de3b48f6a52b063c404ef7f8d36a2e1cdb68400c6135e4fcd57ddb2ec5d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "head.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7a64c0e7a2f21a2ba76ddd34524619fb149628afe6697677be488a955f4dc72b = $this->env->getExtension("native_profiler");
        $__internal_7a64c0e7a2f21a2ba76ddd34524619fb149628afe6697677be488a955f4dc72b->enter($__internal_7a64c0e7a2f21a2ba76ddd34524619fb149628afe6697677be488a955f4dc72b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "head.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7a64c0e7a2f21a2ba76ddd34524619fb149628afe6697677be488a955f4dc72b->leave($__internal_7a64c0e7a2f21a2ba76ddd34524619fb149628afe6697677be488a955f4dc72b_prof);

    }

    // line 2
    public function block_head($context, array $blocks = array())
    {
        $__internal_5c7f86aa6818b7ac500f0242d189c870dcf61054a34d61d41d060304677ef0cb = $this->env->getExtension("native_profiler");
        $__internal_5c7f86aa6818b7ac500f0242d189c870dcf61054a34d61d41d060304677ef0cb->enter($__internal_5c7f86aa6818b7ac500f0242d189c870dcf61054a34d61d41d060304677ef0cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 3
        echo "    <!-- saved from url=(0043)http://realhomes.inspirythemes.biz/listing/ -->
    <!--<![endif]-->
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <link rel=\"profile\" href=\"http://gmpg.org/xfn/11\">
        <meta name=\"format-detection\" content=\"telephone=no\">
        <link rel=\"shortcut icon\" href=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2015/08/favicon.png\">
        <link rel=\"pingback\" href=\"http://realhomes.inspirythemes.biz/xmlrpc.php\">
        <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/yoz.css"), "html", null, true);
        echo "\">
        <title><span>Listing – Real Homes</span></title>
        <link rel=\"alternate\" type=\"application/rss+xml\" title=\"Real Homes » Listing Comments Feed\" href=\"http://realhomes.inspirythemes.biz/listing/feed/\">
        
        
        
        
        <style type=\"text/css\">
            img.wp-smiley,
            img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
            }
        </style>
        <link rel=\"stylesheet\" id=\"dsidx-css\" href=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/0e43ba"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"dsidxpress-icons-css\" href=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/dsidx-icons.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"dsidxpress-unconditional-css\" href=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/client.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"dsidxwidgets-unconditional-css\" href=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/client(1).css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"paypal-ipnpublic-css\" href=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/paypal-ipn-for-wordpress-public.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"paypal-ipnpublicDataTablecss-css\" href=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/jquery.dataTables.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"paypal-ipnpublicDataTable-css\" href=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/dataTables.responsive.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"font-awesome-css\" href=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/font-awesome.min.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"quick-and-easy-faqs-css\" href=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/quick-and-easy-faqs-public.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"quick-and-easy-testimonials-css\" href=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/quick-and-easy-testimonials-public.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"lidd_mc-css\" href=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/style.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"screen\">
        <link rel=\"stylesheet\" id=\"rs-plugin-settings-css\" href=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/settings.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <style id=\"rs-plugin-settings-inline-css\" type=\"text/css\">
            .tp-button.green.custom{font-size:16px;text-transform:uppercase;border-radius:0;box-shadow:none;text-shadow:none;padding:10px 15px; letter-spacing:1px;background:#ec894d}
        </style>
       ";
        // line 49
        echo "        <link rel=\"stylesheet\" id=\"bootstrap-css-css\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/bootstrap.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"responsive-css-css\" href=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/responsive.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"flexslider-css\" href=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/flexslider.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"pretty-photo-css-css\" href=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/prettyPhoto.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"swipebox-css\" href=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/swipebox.min.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"select2-css\" href=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/select2.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"main-css-css\" href=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/main.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"custom-responsive-css-css\" href=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/custom-responsive.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"parent-default-css\" href=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/style(1).css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"parent-custom-css\" href=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/custom.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <script type=\"text/javascript\" src=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/jquery.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/jquery-migrate.min.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/ed75b6"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/paypal-ipn-for-wordpress.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/dataTables.responsive.js"), "html", null, true);
        echo "\"></script>
        <link rel=\"EditURI\" type=\"application/rsd+xml\" title=\"RSD\" href=\"http://realhomes.inspirythemes.biz/xmlrpc.php?rsd\">
        <link rel=\"wlwmanifest\" type=\"application/wlwmanifest+xml\" href=\"http://realhomes.inspirythemes.biz/wp-includes/wlwmanifest.xml\">
        <meta name=\"generator\" content=\"WordPress 4.5.2\">
        <link rel=\"canonical\" href=\"http://realhomes.inspirythemes.biz/listing/\">
        <link rel=\"shortlink\" href=\"http://realhomes.inspirythemes.biz/?p=129\">
        ";
        // line 73
        echo "<style type=\"text/css\" id=\"dynamic-css\">
            .header-wrapper, #currency-switcher #selected-currency, #currency-switcher-list li{
            background-color:#252A2B;
            }
            #logo h2 a{
            color:#ffffff;
            }
            #logo h2 a:hover, #logo h2 a:focus, #logo h2 a:active{
            color:#4dc7ec;
            }
            .tag-line span{
            color:#8b9293;
            }
            .tag-line span{
            background-color:#343a3b;
            }
            .page-head h1.page-title span{
            color:#394041;
            }
            .page-head h1.page-title span{
            background-color:#f5f4f3;
            }
            .page-head p{
            color:#ffffff;
            }
            .page-head p{
            background-color:#37B3D9;
            }
            .header-wrapper, #contact-email, #contact-email a, .user-nav a, .social_networks li a, #currency-switcher #selected-currency, #currency-switcher-list li{
            color:#929A9B;
            }
            #contact-email a:hover, .user-nav a:hover{
            color:#b0b8b9;
            }
            #header-top, .social_networks li a, .user-nav a, .header-wrapper .social_networks, #currency-switcher #selected-currency, #currency-switcher-list li{
            border-color:#343A3B;
            }
            .main-menu ul li a{
            color:#afb4b5;
            }
            .main-menu ul li.current-menu-ancestor > a, .main-menu ul li.current-menu-parent > a, .main-menu ul li.current-menu-item > a, .main-menu ul li.current_page_item > a, .main-menu ul li:hover > a, .main-menu ul li ul, .main-menu ul li ul li ul{
            background-color:#ec894d;
            }
            .main-menu ul li.current-menu-ancestor > a, .main-menu ul li.current-menu-parent > a, .main-menu ul li.current-menu-item > a, .main-menu ul li.current_page_item > a, .main-menu ul li:hover > a, .main-menu ul li ul, .main-menu ul li ul li a, .main-menu ul li ul li ul, .main-menu ul li ul li ul li a{
            color:#ffffff;
            }
            .main-menu ul li ul li:hover > a, .main-menu ul li ul li ul li:hover > a{
            background-color:#dc7d44;
            }
            .slide-description h3, .slide-description h3 a{
            color:#394041;
            }
            .slide-description h3 a:hover, .slide-description h3 a:focus, .slide-description h3 a:active{
            color:#df5400;
            }
            .slide-description p{
            color:#8b9293;
            }
            .slide-description span{
            color:#df5400;
            }
            .slide-description .know-more{
            color:#ffffff;
            }
            .slide-description .know-more{
            background-color:#37b3d9;
            }
            .slide-description .know-more:hover{
            background-color:#2aa6cc;
            }
            .property-item{
            background-color:#ffffff;
            }
            .property-item, .property-item .property-meta, .property-item .property-meta span{
            border-color:#dedede;
            }
            .property-item h4, .property-item h4 a, .es-carousel-wrapper ul li h4 a{
            color:#394041;
            }
            .property-item h4 a:hover, .property-item h4 a:focus, .property-item h4 a:active, .es-carousel-wrapper ul li h4 a:hover, .es-carousel-wrapper ul li h4 a:focus, .es-carousel-wrapper ul li h4 a:active{
            color:#df5400;
            }
            .property-item .price, .es-carousel-wrapper ul li .price, .property-item .price small{
            color:#ffffff;
            }
            .property-item .price, .es-carousel-wrapper ul li .price{
            background-color:#4dc7ec;
            }
            .property-item figure figcaption{
            color:#ffffff;
            }
            .property-item figure figcaption{
            background-color:#ec894d;
            }
            .property-item p, .es-carousel-wrapper ul li p{
            color:#8b9293;
            }
            .more-details, .es-carousel-wrapper ul li p a{
            color:#394041;
            }
            .more-details:hover, .more-details:focus, .more-details:active, .es-carousel-wrapper ul li p a:hover, .es-carousel-wrapper ul li p a:focus, .es-carousel-wrapper ul li p a:active{
            color:#df5400;
            }
            .property-item .property-meta span{
            color:#394041;
            }
            .property-item .property-meta{
            background-color:#f5f5f5;
            }
            #footer .widget .title{
            color:#394041;
            }
            #footer .widget .textwidget, #footer .widget, #footer-bottom p{
            color:#8b9293;
            }
            #footer .widget ul li a, #footer .widget a, #footer-bottom a{
            color:#75797A;
            }
            #footer .widget ul li a:hover, #footer .widget ul li a:focus, #footer.widget ul li a:active, #footer .widget a:hover, #footer .widget a:focus, #footer .widget a:active, #footer-bottom a:hover, #footer-bottom a:focus, #footer-bottom a:active{
            color:#dc7d44;
            }
            #footer-bottom{
            border-color:#dedede;
            }
            .real-btn{
            color:#ffffff;
            }
            .real-btn{
            background-color:#ec894d;
            }
            .real-btn:hover, .real-btn.current{
            color:#ffffff;
            }
            .real-btn:hover, .real-btn.current{
            background-color:#e3712c;
            }
            @media (min-width: 980px) {
            .contact-number, .contact-number .outer-strip{
            background-color:#4dc7ec;
            }
            .contact-number{
            color:#e7eff7;
            }
            .contact-number .fa-phone{
            background-color:#37b3d9;
            }
            }
        </style>
        <script type=\"text/javascript\">
            var RecaptchaOptions = {
            \ttheme : 'custom', custom_theme_widget : 'recaptcha_widget'
            };
        </script>
        <!--[if lt IE 9]>
        <script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\")}}\"></script>
        <![endif]-->
        <meta name=\"generator\" content=\"Powered by Slider Revolution 5.0.4.1 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface.\">
        <!-- BEGIN GADWP v4.9.3.1 Universal Tracking - https://deconf.com/google-analytics-dashboard-wordpress/ -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-31893023-8', 'auto');
            ga('send', 'pageview');
        </script>
        <!-- END GADWP Universal Tracking -->
        <script type=\"text/javascript\">
            if (typeof localdsidx == \"undefined\" || !localdsidx) { var localdsidx = {}; };
            localdsidx.pluginUrl = \"/wp-content/plugins/dsidxpress/\";
            localdsidx.homeUrl = \"http://realhomes.inspirythemes.biz\";
        </script>\t\t\t\t<script type=\"text/javascript\">
            if (typeof localdsidx == \"undefined\" || !localdsidx) { var localdsidx = {}; };
            localdsidx.pluginUrl = \"/wp-content/plugins/dsidxpress/\";
            localdsidx.homeUrl = \"http://realhomes.inspirythemes.biz\";
        </script>
";
        
        $__internal_5c7f86aa6818b7ac500f0242d189c870dcf61054a34d61d41d060304677ef0cb->leave($__internal_5c7f86aa6818b7ac500f0242d189c870dcf61054a34d61d41d060304677ef0cb_prof);

    }

    public function getTemplateName()
    {
        return "head.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  199 => 73,  190 => 65,  186 => 64,  182 => 63,  178 => 62,  174 => 61,  170 => 60,  166 => 59,  162 => 58,  158 => 57,  154 => 56,  150 => 55,  146 => 54,  142 => 53,  138 => 52,  134 => 51,  130 => 50,  125 => 49,  118 => 43,  114 => 42,  110 => 41,  106 => 40,  102 => 39,  98 => 38,  94 => 37,  90 => 36,  86 => 35,  82 => 34,  78 => 33,  74 => 32,  50 => 11,  40 => 3,  34 => 2,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* {% block head %}*/
/*     <!-- saved from url=(0043)http://realhomes.inspirythemes.biz/listing/ -->*/
/*     <!--<![endif]-->*/
/*         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">*/
/*         <meta name="viewport" content="width=device-width, initial-scale=1.0">*/
/*         <link rel="profile" href="http://gmpg.org/xfn/11">*/
/*         <meta name="format-detection" content="telephone=no">*/
/*         <link rel="shortcut icon" href="http://realhomes.inspirythemes.biz/wp-content/uploads/2015/08/favicon.png">*/
/*         <link rel="pingback" href="http://realhomes.inspirythemes.biz/xmlrpc.php">*/
/*         <link rel="stylesheet" type="text/css" href="{{ asset("bundles/front/yoz.css")}}">*/
/*         <title><span>Listing – Real Homes</span></title>*/
/*         <link rel="alternate" type="application/rss+xml" title="Real Homes » Listing Comments Feed" href="http://realhomes.inspirythemes.biz/listing/feed/">*/
/*         */
/*         */
/*         */
/*         */
/*         <style type="text/css">*/
/*             img.wp-smiley,*/
/*             img.emoji {*/
/*             display: inline !important;*/
/*             border: none !important;*/
/*             box-shadow: none !important;*/
/*             height: 1em !important;*/
/*             width: 1em !important;*/
/*             margin: 0 .07em !important;*/
/*             vertical-align: -0.1em !important;*/
/*             background: none !important;*/
/*             padding: 0 !important;*/
/*             }*/
/*         </style>*/
/*         <link rel="stylesheet" id="dsidx-css" href="{{asset("bundles/front/0e43ba")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="dsidxpress-icons-css" href="{{asset("bundles/front/dsidx-icons.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="dsidxpress-unconditional-css" href="{{asset("bundles/front/client.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="dsidxwidgets-unconditional-css" href="{{asset("bundles/front/client(1).css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="paypal-ipnpublic-css" href="{{asset("bundles/front/paypal-ipn-for-wordpress-public.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="paypal-ipnpublicDataTablecss-css" href="{{asset("bundles/front/jquery.dataTables.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="paypal-ipnpublicDataTable-css" href="{{asset("bundles/front/dataTables.responsive.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="font-awesome-css" href="{{asset("bundles/front/font-awesome.min.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="quick-and-easy-faqs-css" href="{{asset("bundles/front/quick-and-easy-faqs-public.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="quick-and-easy-testimonials-css" href="{{asset("bundles/front/quick-and-easy-testimonials-public.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="lidd_mc-css" href="{{asset("bundles/front/style.css")}}" type="text/css" media="screen">*/
/*         <link rel="stylesheet" id="rs-plugin-settings-css" href="{{asset("bundles/front/settings.css")}}" type="text/css" media="all">*/
/*         <style id="rs-plugin-settings-inline-css" type="text/css">*/
/*             .tp-button.green.custom{font-size:16px;text-transform:uppercase;border-radius:0;box-shadow:none;text-shadow:none;padding:10px 15px; letter-spacing:1px;background:#ec894d}*/
/*         </style>*/
/*        {# <link rel="stylesheet" id="theme-roboto-css" href="{{asset("bundles/front/css.css")}} type="text/css" media="all">*/
/*         <link rel="stylesheet" id="theme-lato-css" href="{{asset("bundles/front/css(1).css")}} type="text/css" media="all">#}*/
/*         <link rel="stylesheet" id="bootstrap-css-css" href="{{asset("bundles/front/bootstrap.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="responsive-css-css" href="{{asset("bundles/front/responsive.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="flexslider-css" href="{{asset("bundles/front/flexslider.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="pretty-photo-css-css" href="{{asset("bundles/front/prettyPhoto.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="swipebox-css" href="{{asset("bundles/front/swipebox.min.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="select2-css" href="{{asset("bundles/front/select2.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="main-css-css" href="{{asset("bundles/front/main.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="custom-responsive-css-css" href="{{asset("bundles/front/custom-responsive.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="parent-default-css" href="{{asset("bundles/front/style(1).css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="parent-custom-css" href="{{asset("bundles/front/custom.css")}}" type="text/css" media="all">*/
/*         <script type="text/javascript" src="{{asset("bundles/front/jquery.js")}}"></script>*/
/*         <script type="text/javascript" src="{{asset("bundles/front/jquery-migrate.min.js")}}"></script>*/
/*         <script type="text/javascript" src="{{asset("bundles/front/ed75b6")}}"></script>*/
/*         <script type="text/javascript" src="{{asset("bundles/front/paypal-ipn-for-wordpress.js")}}"></script>*/
/*         <script type="text/javascript" src="{{asset("bundles/front/jquery.dataTables.min.js")}}"></script>*/
/*         <script type="text/javascript" src="{{asset("bundles/front/bootstrap.min.js")}}"></script>*/
/*         <script type="text/javascript" src="{{asset("bundles/front/dataTables.responsive.js")}}"></script>*/
/*         <link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://realhomes.inspirythemes.biz/xmlrpc.php?rsd">*/
/*         <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://realhomes.inspirythemes.biz/wp-includes/wlwmanifest.xml">*/
/*         <meta name="generator" content="WordPress 4.5.2">*/
/*         <link rel="canonical" href="http://realhomes.inspirythemes.biz/listing/">*/
/*         <link rel="shortlink" href="http://realhomes.inspirythemes.biz/?p=129">*/
/*         {#<link rel="alternate" type="application/json+oembed" href="http://realhomes.inspirythemes.biz/wp-json/oembed/1.0/embed?url=http%3A%2F%2Frealhomes.inspirythemes.biz%2Flisting%2F">*/
/*         <link rel="alternate" type="text/xml+oembed" href="http://realhomes.inspirythemes.biz/wp-json/oembed/1.0/embed?url=http%3A%2F%2Frealhomes.inspirythemes.biz%2Flisting%2F&amp;format=xml">*/
/*         #}<style type="text/css" id="dynamic-css">*/
/*             .header-wrapper, #currency-switcher #selected-currency, #currency-switcher-list li{*/
/*             background-color:#252A2B;*/
/*             }*/
/*             #logo h2 a{*/
/*             color:#ffffff;*/
/*             }*/
/*             #logo h2 a:hover, #logo h2 a:focus, #logo h2 a:active{*/
/*             color:#4dc7ec;*/
/*             }*/
/*             .tag-line span{*/
/*             color:#8b9293;*/
/*             }*/
/*             .tag-line span{*/
/*             background-color:#343a3b;*/
/*             }*/
/*             .page-head h1.page-title span{*/
/*             color:#394041;*/
/*             }*/
/*             .page-head h1.page-title span{*/
/*             background-color:#f5f4f3;*/
/*             }*/
/*             .page-head p{*/
/*             color:#ffffff;*/
/*             }*/
/*             .page-head p{*/
/*             background-color:#37B3D9;*/
/*             }*/
/*             .header-wrapper, #contact-email, #contact-email a, .user-nav a, .social_networks li a, #currency-switcher #selected-currency, #currency-switcher-list li{*/
/*             color:#929A9B;*/
/*             }*/
/*             #contact-email a:hover, .user-nav a:hover{*/
/*             color:#b0b8b9;*/
/*             }*/
/*             #header-top, .social_networks li a, .user-nav a, .header-wrapper .social_networks, #currency-switcher #selected-currency, #currency-switcher-list li{*/
/*             border-color:#343A3B;*/
/*             }*/
/*             .main-menu ul li a{*/
/*             color:#afb4b5;*/
/*             }*/
/*             .main-menu ul li.current-menu-ancestor > a, .main-menu ul li.current-menu-parent > a, .main-menu ul li.current-menu-item > a, .main-menu ul li.current_page_item > a, .main-menu ul li:hover > a, .main-menu ul li ul, .main-menu ul li ul li ul{*/
/*             background-color:#ec894d;*/
/*             }*/
/*             .main-menu ul li.current-menu-ancestor > a, .main-menu ul li.current-menu-parent > a, .main-menu ul li.current-menu-item > a, .main-menu ul li.current_page_item > a, .main-menu ul li:hover > a, .main-menu ul li ul, .main-menu ul li ul li a, .main-menu ul li ul li ul, .main-menu ul li ul li ul li a{*/
/*             color:#ffffff;*/
/*             }*/
/*             .main-menu ul li ul li:hover > a, .main-menu ul li ul li ul li:hover > a{*/
/*             background-color:#dc7d44;*/
/*             }*/
/*             .slide-description h3, .slide-description h3 a{*/
/*             color:#394041;*/
/*             }*/
/*             .slide-description h3 a:hover, .slide-description h3 a:focus, .slide-description h3 a:active{*/
/*             color:#df5400;*/
/*             }*/
/*             .slide-description p{*/
/*             color:#8b9293;*/
/*             }*/
/*             .slide-description span{*/
/*             color:#df5400;*/
/*             }*/
/*             .slide-description .know-more{*/
/*             color:#ffffff;*/
/*             }*/
/*             .slide-description .know-more{*/
/*             background-color:#37b3d9;*/
/*             }*/
/*             .slide-description .know-more:hover{*/
/*             background-color:#2aa6cc;*/
/*             }*/
/*             .property-item{*/
/*             background-color:#ffffff;*/
/*             }*/
/*             .property-item, .property-item .property-meta, .property-item .property-meta span{*/
/*             border-color:#dedede;*/
/*             }*/
/*             .property-item h4, .property-item h4 a, .es-carousel-wrapper ul li h4 a{*/
/*             color:#394041;*/
/*             }*/
/*             .property-item h4 a:hover, .property-item h4 a:focus, .property-item h4 a:active, .es-carousel-wrapper ul li h4 a:hover, .es-carousel-wrapper ul li h4 a:focus, .es-carousel-wrapper ul li h4 a:active{*/
/*             color:#df5400;*/
/*             }*/
/*             .property-item .price, .es-carousel-wrapper ul li .price, .property-item .price small{*/
/*             color:#ffffff;*/
/*             }*/
/*             .property-item .price, .es-carousel-wrapper ul li .price{*/
/*             background-color:#4dc7ec;*/
/*             }*/
/*             .property-item figure figcaption{*/
/*             color:#ffffff;*/
/*             }*/
/*             .property-item figure figcaption{*/
/*             background-color:#ec894d;*/
/*             }*/
/*             .property-item p, .es-carousel-wrapper ul li p{*/
/*             color:#8b9293;*/
/*             }*/
/*             .more-details, .es-carousel-wrapper ul li p a{*/
/*             color:#394041;*/
/*             }*/
/*             .more-details:hover, .more-details:focus, .more-details:active, .es-carousel-wrapper ul li p a:hover, .es-carousel-wrapper ul li p a:focus, .es-carousel-wrapper ul li p a:active{*/
/*             color:#df5400;*/
/*             }*/
/*             .property-item .property-meta span{*/
/*             color:#394041;*/
/*             }*/
/*             .property-item .property-meta{*/
/*             background-color:#f5f5f5;*/
/*             }*/
/*             #footer .widget .title{*/
/*             color:#394041;*/
/*             }*/
/*             #footer .widget .textwidget, #footer .widget, #footer-bottom p{*/
/*             color:#8b9293;*/
/*             }*/
/*             #footer .widget ul li a, #footer .widget a, #footer-bottom a{*/
/*             color:#75797A;*/
/*             }*/
/*             #footer .widget ul li a:hover, #footer .widget ul li a:focus, #footer.widget ul li a:active, #footer .widget a:hover, #footer .widget a:focus, #footer .widget a:active, #footer-bottom a:hover, #footer-bottom a:focus, #footer-bottom a:active{*/
/*             color:#dc7d44;*/
/*             }*/
/*             #footer-bottom{*/
/*             border-color:#dedede;*/
/*             }*/
/*             .real-btn{*/
/*             color:#ffffff;*/
/*             }*/
/*             .real-btn{*/
/*             background-color:#ec894d;*/
/*             }*/
/*             .real-btn:hover, .real-btn.current{*/
/*             color:#ffffff;*/
/*             }*/
/*             .real-btn:hover, .real-btn.current{*/
/*             background-color:#e3712c;*/
/*             }*/
/*             @media (min-width: 980px) {*/
/*             .contact-number, .contact-number .outer-strip{*/
/*             background-color:#4dc7ec;*/
/*             }*/
/*             .contact-number{*/
/*             color:#e7eff7;*/
/*             }*/
/*             .contact-number .fa-phone{*/
/*             background-color:#37b3d9;*/
/*             }*/
/*             }*/
/*         </style>*/
/*         <script type="text/javascript">*/
/*             var RecaptchaOptions = {*/
/*             	theme : 'custom', custom_theme_widget : 'recaptcha_widget'*/
/*             };*/
/*         </script>*/
/*         <!--[if lt IE 9]>*/
/*         <script src="http://html5shim.googlecode.com/svn/trunk/html5.js")}}"></script>*/
/*         <![endif]-->*/
/*         <meta name="generator" content="Powered by Slider Revolution 5.0.4.1 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface.">*/
/*         <!-- BEGIN GADWP v4.9.3.1 Universal Tracking - https://deconf.com/google-analytics-dashboard-wordpress/ -->*/
/*         <script>*/
/*             (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){*/
/*             (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),*/
/*             m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)*/
/*             })(window,document,'script','//www.google-analytics.com/analytics.js','ga');*/
/*             ga('create', 'UA-31893023-8', 'auto');*/
/*             ga('send', 'pageview');*/
/*         </script>*/
/*         <!-- END GADWP Universal Tracking -->*/
/*         <script type="text/javascript">*/
/*             if (typeof localdsidx == "undefined" || !localdsidx) { var localdsidx = {}; };*/
/*             localdsidx.pluginUrl = "/wp-content/plugins/dsidxpress/";*/
/*             localdsidx.homeUrl = "http://realhomes.inspirythemes.biz";*/
/*         </script>				<script type="text/javascript">*/
/*             if (typeof localdsidx == "undefined" || !localdsidx) { var localdsidx = {}; };*/
/*             localdsidx.pluginUrl = "/wp-content/plugins/dsidxpress/";*/
/*             localdsidx.homeUrl = "http://realhomes.inspirythemes.biz";*/
/*         </script>*/
/* {% endblock %}*/
