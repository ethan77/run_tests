<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_276223e69819bd2376a3cef58c2fcb4368c4ecaebae4471c11df87cc915a487d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7d41d9a885a83097ee4e7dac4ea9bdc79c87db4134c4cd6fd315bd35d1c84dd1 = $this->env->getExtension("native_profiler");
        $__internal_7d41d9a885a83097ee4e7dac4ea9bdc79c87db4134c4cd6fd315bd35d1c84dd1->enter($__internal_7d41d9a885a83097ee4e7dac4ea9bdc79c87db4134c4cd6fd315bd35d1c84dd1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7d41d9a885a83097ee4e7dac4ea9bdc79c87db4134c4cd6fd315bd35d1c84dd1->leave($__internal_7d41d9a885a83097ee4e7dac4ea9bdc79c87db4134c4cd6fd315bd35d1c84dd1_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_f30f06020d1427450791573c94ea3a062e59a9d104059fe178d9701f2c23b24d = $this->env->getExtension("native_profiler");
        $__internal_f30f06020d1427450791573c94ea3a062e59a9d104059fe178d9701f2c23b24d->enter($__internal_f30f06020d1427450791573c94ea3a062e59a9d104059fe178d9701f2c23b24d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_f30f06020d1427450791573c94ea3a062e59a9d104059fe178d9701f2c23b24d->leave($__internal_f30f06020d1427450791573c94ea3a062e59a9d104059fe178d9701f2c23b24d_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_282086775baf001820319799baf28cb24298a6e912718a70c6982d3900126645 = $this->env->getExtension("native_profiler");
        $__internal_282086775baf001820319799baf28cb24298a6e912718a70c6982d3900126645->enter($__internal_282086775baf001820319799baf28cb24298a6e912718a70c6982d3900126645_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_282086775baf001820319799baf28cb24298a6e912718a70c6982d3900126645->leave($__internal_282086775baf001820319799baf28cb24298a6e912718a70c6982d3900126645_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_afb8f1d7eb8f53f21e664ca216d4f457bbd4c93ee5d741f744c88e8c37c8ad86 = $this->env->getExtension("native_profiler");
        $__internal_afb8f1d7eb8f53f21e664ca216d4f457bbd4c93ee5d741f744c88e8c37c8ad86->enter($__internal_afb8f1d7eb8f53f21e664ca216d4f457bbd4c93ee5d741f744c88e8c37c8ad86_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_afb8f1d7eb8f53f21e664ca216d4f457bbd4c93ee5d741f744c88e8c37c8ad86->leave($__internal_afb8f1d7eb8f53f21e664ca216d4f457bbd4c93ee5d741f744c88e8c37c8ad86_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
