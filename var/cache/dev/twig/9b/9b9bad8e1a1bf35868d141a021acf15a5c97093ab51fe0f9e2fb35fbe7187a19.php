<?php

/* header_wrap.html.twig */
class __TwigTemplate_43b07972e0309b08ef56a8ce5490711beccdf4d0fc01db74227cfbe5cf81e129 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dbb420079e8e0eb1513de51cd0cd7d0e2fe2f9d34612d8b47cf7f988dfdffaf9 = $this->env->getExtension("native_profiler");
        $__internal_dbb420079e8e0eb1513de51cd0cd7d0e2fe2f9d34612d8b47cf7f988dfdffaf9->enter($__internal_dbb420079e8e0eb1513de51cd0cd7d0e2fe2f9d34612d8b47cf7f988dfdffaf9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "header_wrap.html.twig"));

        // line 1
        echo "<div class=\"header-wrapper\">
            <div class=\"container\">
                <!-- Start Header Container -->
                <header id=\"header\" class=\"clearfix\">
                    <div id=\"header-top\" class=\"clearfix\">
                        <h2 id=\"contact-email\">
                            <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Layer_1\" x=\"0px\" y=\"0px\" width=\"16px\" height=\"16px\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
                                <path class=\"path\" d=\"M8.174 15.926l-6.799 5.438c-0.431 0.346-0.501 0.975-0.156 1.406s0.974 0.5 1.4 0.156l7.211-5.769L8.174 15.926z\"></path>
                                <path class=\"path\" d=\"M15.838 15.936l-1.685 1.214l7.222 5.777c0.433 0.3 1.1 0.3 1.406-0.156c0.345-0.432 0.274-1.061-0.157-1.406 L15.838 15.936z\"></path>
                                <polygon class=\"path\" points=\"1,10.2 1.6,10.9 12,2.6 22,10.6 22,22 2,22 2,10.2 1,10.2 1.6,10.9 1,10.2 0,10.2 0,24 24,24 24,9.7 12,0 0,9.7 0,10.2 1,10.2 1,10.2\"></polygon>
                                <polygon class=\"path\" points=\"23.6,11.7 12.6,19.7 11.4,19.7 0.4,11.7 0.4,11.7 0.4,11.7 1.6,10.1 12,17.6 22.4,10.1\"></polygon>
                            </svg>
                            <span id=\"anda\">Email us at :</span>
                            <a href=\"mailto:sales@yourwebsite.com\">sales@yourwebsite.com</a>
                        </h2>
                        <!-- Social Navigation -->
                        <ul class=\"social_networks clearfix\">
                            <li class=\"facebook\">
                                <a target=\"_blank\" href=\"https://www.facebook.com/InspiryThemes\"><i class=\"fa fa-facebook fa-lg\"></i></a>
                            </li>
                            <li class=\"twitter\">
                                <a target=\"_blank\" href=\"https://twitter.com/InspiryThemes\"><i class=\"fa fa-twitter fa-lg\"></i></a>
                            </li>
                            <li class=\"linkedin\">
                                <a target=\"_blank\" href=\"http://realhomes.inspirythemes.biz/listing/#\"><i class=\"fa fa-linkedin fa-lg\"></i></a>
                            </li>
                            <li class=\"gplus\">
                                <a target=\"_blank\" href=\"http://realhomes.inspirythemes.biz/listing/#\"><i class=\"fa fa-google-plus fa-lg\"></i></a>
                            </li>
                            <li class=\"rss\">
                                <a target=\"_blank\" href=\"http://realhomes.inspirythemes.biz/feed/?post_type=property\"> <i class=\"fa fa-rss fa-lg\"></i></a>
                            </li>
                        </ul>
                        <!-- User Navigation -->
                        <div class=\"user-nav clearfix\">
                            <a href=\"http://realhomes.inspirythemes.biz/favorites/\">
                            <i class=\"fa fa-star\"></i>Favorites\t\t\t</a>
                            <a class=\"last\" href=\"#\" data-toggle=\"modal\"><i class=\"fa fa-sign-in\"></i>Login / Register</a>\t
                        </div>
                    </div>
                    <!-- Logo -->
                    <div id=\"logo\">
                        <a title=\"Real Homes\" href=\"http://realhomes.inspirythemes.biz/\">
                        <img src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/logo.png"), "html", null, true);
        echo "\" alt=\"Real Homes\">
                        </a>
                        <h2 class=\"logo-heading only-for-print\">
                            <a href=\"http://realhomes.inspirythemes.biz/\" title=\"Real Homes\">
                            Real Homes                                </a>
                        </h2>
                        <div class=\"tag-line\"><span>No. 1 Real Estate Theme</span></div>
                    </div>
                        <!-- Start Main Menu-->
                        ";
        // line 53
        $this->loadTemplate("header_wrap.html.twig", "header_wrap.html.twig", 53, "2764018")->display($context);
        // line 54
        echo "    
                                        <!-- End Main Menu -->
                </header>
            </div>
            <!-- End Header Container -->
        </div>";
        
        $__internal_dbb420079e8e0eb1513de51cd0cd7d0e2fe2f9d34612d8b47cf7f988dfdffaf9->leave($__internal_dbb420079e8e0eb1513de51cd0cd7d0e2fe2f9d34612d8b47cf7f988dfdffaf9_prof);

    }

    public function getTemplateName()
    {
        return "header_wrap.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 54,  79 => 53,  67 => 44,  22 => 1,);
    }
}


/* header_wrap.html.twig */
class __TwigTemplate_43b07972e0309b08ef56a8ce5490711beccdf4d0fc01db74227cfbe5cf81e129_2764018 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 53
        $this->parent = $this->loadTemplate("main_menu.html.twig", "header_wrap.html.twig", 53);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "main_menu.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ac05195ec0ccbe9464cad02a775864bec5e240909fadc16c9dc954e90870613f = $this->env->getExtension("native_profiler");
        $__internal_ac05195ec0ccbe9464cad02a775864bec5e240909fadc16c9dc954e90870613f->enter($__internal_ac05195ec0ccbe9464cad02a775864bec5e240909fadc16c9dc954e90870613f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "header_wrap.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ac05195ec0ccbe9464cad02a775864bec5e240909fadc16c9dc954e90870613f->leave($__internal_ac05195ec0ccbe9464cad02a775864bec5e240909fadc16c9dc954e90870613f_prof);

    }

    public function getTemplateName()
    {
        return "header_wrap.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 53,  81 => 54,  79 => 53,  67 => 44,  22 => 1,);
    }
}
/* <div class="header-wrapper">*/
/*             <div class="container">*/
/*                 <!-- Start Header Container -->*/
/*                 <header id="header" class="clearfix">*/
/*                     <div id="header-top" class="clearfix">*/
/*                         <h2 id="contact-email">*/
/*                             <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">*/
/*                                 <path class="path" d="M8.174 15.926l-6.799 5.438c-0.431 0.346-0.501 0.975-0.156 1.406s0.974 0.5 1.4 0.156l7.211-5.769L8.174 15.926z"></path>*/
/*                                 <path class="path" d="M15.838 15.936l-1.685 1.214l7.222 5.777c0.433 0.3 1.1 0.3 1.406-0.156c0.345-0.432 0.274-1.061-0.157-1.406 L15.838 15.936z"></path>*/
/*                                 <polygon class="path" points="1,10.2 1.6,10.9 12,2.6 22,10.6 22,22 2,22 2,10.2 1,10.2 1.6,10.9 1,10.2 0,10.2 0,24 24,24 24,9.7 12,0 0,9.7 0,10.2 1,10.2 1,10.2"></polygon>*/
/*                                 <polygon class="path" points="23.6,11.7 12.6,19.7 11.4,19.7 0.4,11.7 0.4,11.7 0.4,11.7 1.6,10.1 12,17.6 22.4,10.1"></polygon>*/
/*                             </svg>*/
/*                             <span id="anda">Email us at :</span>*/
/*                             <a href="mailto:sales@yourwebsite.com">sales@yourwebsite.com</a>*/
/*                         </h2>*/
/*                         <!-- Social Navigation -->*/
/*                         <ul class="social_networks clearfix">*/
/*                             <li class="facebook">*/
/*                                 <a target="_blank" href="https://www.facebook.com/InspiryThemes"><i class="fa fa-facebook fa-lg"></i></a>*/
/*                             </li>*/
/*                             <li class="twitter">*/
/*                                 <a target="_blank" href="https://twitter.com/InspiryThemes"><i class="fa fa-twitter fa-lg"></i></a>*/
/*                             </li>*/
/*                             <li class="linkedin">*/
/*                                 <a target="_blank" href="http://realhomes.inspirythemes.biz/listing/#"><i class="fa fa-linkedin fa-lg"></i></a>*/
/*                             </li>*/
/*                             <li class="gplus">*/
/*                                 <a target="_blank" href="http://realhomes.inspirythemes.biz/listing/#"><i class="fa fa-google-plus fa-lg"></i></a>*/
/*                             </li>*/
/*                             <li class="rss">*/
/*                                 <a target="_blank" href="http://realhomes.inspirythemes.biz/feed/?post_type=property"> <i class="fa fa-rss fa-lg"></i></a>*/
/*                             </li>*/
/*                         </ul>*/
/*                         <!-- User Navigation -->*/
/*                         <div class="user-nav clearfix">*/
/*                             <a href="http://realhomes.inspirythemes.biz/favorites/">*/
/*                             <i class="fa fa-star"></i>Favorites			</a>*/
/*                             <a class="last" href="#" data-toggle="modal"><i class="fa fa-sign-in"></i>Login / Register</a>	*/
/*                         </div>*/
/*                     </div>*/
/*                     <!-- Logo -->*/
/*                     <div id="logo">*/
/*                         <a title="Real Homes" href="http://realhomes.inspirythemes.biz/">*/
/*                         <img src="{{asset("bundles/front/logo.png")}}" alt="Real Homes">*/
/*                         </a>*/
/*                         <h2 class="logo-heading only-for-print">*/
/*                             <a href="http://realhomes.inspirythemes.biz/" title="Real Homes">*/
/*                             Real Homes                                </a>*/
/*                         </h2>*/
/*                         <div class="tag-line"><span>No. 1 Real Estate Theme</span></div>*/
/*                     </div>*/
/*                         <!-- Start Main Menu-->*/
/*                         {% embed "main_menu.html.twig" %}*/
/*                         {% endembed %}    */
/*                                         <!-- End Main Menu -->*/
/*                 </header>*/
/*             </div>*/
/*             <!-- End Header Container -->*/
/*         </div>*/
