<?php

/* baza.html.twig */
class __TwigTemplate_c3bcc1d978c3ebff1dc8610701b14e7f0192d933ea798884a8bcfc4bcf1205e0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
            'asset' => array($this, 'block_asset'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_39d678885ed1821c1d6e166d6e003776fa30ff6348d48ddd321a2c93181cea76 = $this->env->getExtension("native_profiler");
        $__internal_39d678885ed1821c1d6e166d6e003776fa30ff6348d48ddd321a2c93181cea76->enter($__internal_39d678885ed1821c1d6e166d6e003776fa30ff6348d48ddd321a2c93181cea76_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "baza.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        ";
        // line 4
        $this->displayBlock('head', $context, $blocks);
        // line 5
        echo "    </head>
    <body>
        ";
        // line 7
        $this->displayBlock('body', $context, $blocks);
        // line 8
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 9
        echo "        <div class=\"yoz\" style=\"color:red\">";
        $this->displayBlock('asset', $context, $blocks);
        // line 11
        echo "        </div>
    </body>
</html>
";
        
        $__internal_39d678885ed1821c1d6e166d6e003776fa30ff6348d48ddd321a2c93181cea76->leave($__internal_39d678885ed1821c1d6e166d6e003776fa30ff6348d48ddd321a2c93181cea76_prof);

    }

    // line 4
    public function block_head($context, array $blocks = array())
    {
        $__internal_54a2f06bbe5d3d2c8c010dc12ba39659a1f863d6da43270938af691d27289528 = $this->env->getExtension("native_profiler");
        $__internal_54a2f06bbe5d3d2c8c010dc12ba39659a1f863d6da43270938af691d27289528->enter($__internal_54a2f06bbe5d3d2c8c010dc12ba39659a1f863d6da43270938af691d27289528_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_54a2f06bbe5d3d2c8c010dc12ba39659a1f863d6da43270938af691d27289528->leave($__internal_54a2f06bbe5d3d2c8c010dc12ba39659a1f863d6da43270938af691d27289528_prof);

    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        $__internal_7e3dd2003613ef35ed2a896a588d420ccecfb2669a431fa544dbe56dcc177e12 = $this->env->getExtension("native_profiler");
        $__internal_7e3dd2003613ef35ed2a896a588d420ccecfb2669a431fa544dbe56dcc177e12->enter($__internal_7e3dd2003613ef35ed2a896a588d420ccecfb2669a431fa544dbe56dcc177e12_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_7e3dd2003613ef35ed2a896a588d420ccecfb2669a431fa544dbe56dcc177e12->leave($__internal_7e3dd2003613ef35ed2a896a588d420ccecfb2669a431fa544dbe56dcc177e12_prof);

    }

    // line 8
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_c936b27f609ea6029fcf22d56a9f999b84aa07a6d06dc6bf3e43202f06e449f4 = $this->env->getExtension("native_profiler");
        $__internal_c936b27f609ea6029fcf22d56a9f999b84aa07a6d06dc6bf3e43202f06e449f4->enter($__internal_c936b27f609ea6029fcf22d56a9f999b84aa07a6d06dc6bf3e43202f06e449f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_c936b27f609ea6029fcf22d56a9f999b84aa07a6d06dc6bf3e43202f06e449f4->leave($__internal_c936b27f609ea6029fcf22d56a9f999b84aa07a6d06dc6bf3e43202f06e449f4_prof);

    }

    // line 9
    public function block_asset($context, array $blocks = array())
    {
        $__internal_4c0124b8107ace86be585a2179c550a98aa2bc80564dfccc14f72f453056906b = $this->env->getExtension("native_profiler");
        $__internal_4c0124b8107ace86be585a2179c550a98aa2bc80564dfccc14f72f453056906b->enter($__internal_4c0124b8107ace86be585a2179c550a98aa2bc80564dfccc14f72f453056906b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "asset"));

        // line 10
        echo "            ";
        
        $__internal_4c0124b8107ace86be585a2179c550a98aa2bc80564dfccc14f72f453056906b->leave($__internal_4c0124b8107ace86be585a2179c550a98aa2bc80564dfccc14f72f453056906b_prof);

    }

    public function getTemplateName()
    {
        return "baza.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  94 => 10,  88 => 9,  77 => 8,  66 => 7,  55 => 4,  45 => 11,  42 => 9,  39 => 8,  37 => 7,  33 => 5,  31 => 4,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         {% block head %}{% endblock %}*/
/*     </head>*/
/*     <body>*/
/*         {% block body %}{% endblock %}*/
/*         {% block javascripts %}{% endblock %}*/
/*         <div class="yoz" style="color:red">{% block asset %}*/
/*             {% endblock asset%}*/
/*         </div>*/
/*     </body>*/
/* </html>*/
/* */
