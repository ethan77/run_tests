<?php

/* executari/details.html.twig */
class __TwigTemplate_4d1a64c0148b014084a352d7c668720d2d6ccaa48948a3b97ca59073c53f390f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("head.html.twig", "executari/details.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "head.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7b8e6648d22a24940221a381b50fa04f35a5d1b1c2ce0e87f0e2ea42664ec0a9 = $this->env->getExtension("native_profiler");
        $__internal_7b8e6648d22a24940221a381b50fa04f35a5d1b1c2ce0e87f0e2ea42664ec0a9->enter($__internal_7b8e6648d22a24940221a381b50fa04f35a5d1b1c2ce0e87f0e2ea42664ec0a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "executari/details.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7b8e6648d22a24940221a381b50fa04f35a5d1b1c2ce0e87f0e2ea42664ec0a9->leave($__internal_7b8e6648d22a24940221a381b50fa04f35a5d1b1c2ce0e87f0e2ea42664ec0a9_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_0d2d6802322f3e28c8b9ddda1024dfc1e31ac1ab244d5f3d0bb7faf0ccaa4fcb = $this->env->getExtension("native_profiler");
        $__internal_0d2d6802322f3e28c8b9ddda1024dfc1e31ac1ab244d5f3d0bb7faf0ccaa4fcb->enter($__internal_0d2d6802322f3e28c8b9ddda1024dfc1e31ac1ab244d5f3d0bb7faf0ccaa4fcb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <body class=\"single single-property postid-79\">

        <!-- Start Header -->
        <div class=\"header-wrapper\">

            <div class=\"container\"><!-- Start Header Container -->

                <header id=\"header\" class=\"clearfix\">

                    <div id=\"header-top\" class=\"clearfix\">
                                                    <h2 id=\"contact-email\">
                                <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Layer_1\" x=\"0px\" y=\"0px\" width=\"16px\" height=\"16px\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path class=\"path\" d=\"M8.174 15.926l-6.799 5.438c-0.431 0.346-0.501 0.975-0.156 1.406s0.974 0.5 1.4 0.156l7.211-5.769L8.174 15.926z\"/>
<path class=\"path\" d=\"M15.838 15.936l-1.685 1.214l7.222 5.777c0.433 0.3 1.1 0.3 1.406-0.156c0.345-0.432 0.274-1.061-0.157-1.406 L15.838 15.936z\"/>
<polygon class=\"path\" points=\"1,10.2 1.6,10.9 12,2.6 22,10.6 22,22 2,22 2,10.2 1,10.2 1.6,10.9 1,10.2 0,10.2 0,24 24,24 24,9.7 12,0 0,9.7 0,10.2 1,10.2 1,10.2\"/>
<polygon class=\"path\" points=\"23.6,11.7 12.6,19.7 11.4,19.7 0.4,11.7 0.4,11.7 0.4,11.7 1.6,10.1 12,17.6 22.4,10.1\"/>
</svg>
Email us at :
                                <a href=\"mailto:s&#97;&#108;e&#115;&#64;&#121;&#111;&#117;rwebs&#105;te.c&#111;m\">&#115;&#97;&#108;&#101;s&#64;you&#114;&#119;&#101;&#98;s&#105;t&#101;.&#99;&#111;m</a>
                            </h2>
                            
                        <!-- Social Navigation -->
                            <ul class=\"social_networks clearfix\">
                            <li class=\"facebook\">
                    <a target=\"_blank\" href=\"https://www.facebook.com/InspiryThemes\"><i class=\"fa fa-facebook fa-lg\"></i></a>
                </li>
                                <li class=\"twitter\">
                    <a target=\"_blank\" href=\"https://twitter.com/InspiryThemes\"><i class=\"fa fa-twitter fa-lg\"></i></a>
                </li>
                                <li class=\"linkedin\">
                    <a target=\"_blank\" href=\"#\"><i class=\"fa fa-linkedin fa-lg\"></i></a>
                </li>
                                <li class=\"gplus\">
                    <a target=\"_blank\" href=\"#\"><i class=\"fa fa-google-plus fa-lg\"></i></a>
                </li>
                                <li class=\"rss\">
                    <a target=\"_blank\" href=\"http://realhomes.inspirythemes.biz/feed/?post_type=property\"> <i class=\"fa fa-rss fa-lg\"></i></a>
                </li>
                    </ul>
    
                        <!-- User Navigation -->
                        \t<div class=\"user-nav clearfix\">
\t\t\t\t\t<a href=\"http://realhomes.inspirythemes.biz/favorites/\">
\t\t\t\t<i class=\"fa fa-star\"></i>Favorites\t\t\t</a>
\t\t\t<a class=\"last\" href=\"#login-modal\" data-toggle=\"modal\"><i class=\"fa fa-sign-in\"></i>Login / Register</a>\t</div>
\t
                    </div>

                    <!-- Logo -->
                    <div id=\"logo\">

                                                    <a title=\"Real Homes\" href=\"http://realhomes.inspirythemes.biz\">
                                <img src=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2015/08/logo.png\" alt=\"Real Homes\">
                            </a>
                            <h2 class=\"logo-heading only-for-print\">
                                <a href=\"http://realhomes.inspirythemes.biz\"  title=\"Real Homes\">
                                    Real Homes                                </a>
                            </h2>
                            <div class=\"tag-line\"><span>No. 1 Real Estate Theme</span></div>                    </div>


                    <div class=\"menu-and-contact-wrap\">
                        <h2  class=\"contact-number \"><i class=\"fa fa-phone\"></i><span class=\"desktop-version\">1-800-555-1234</span><a class=\"mobile-version\" href=\"tel://1-800-555-1234\" title=\"Make a Call\">1-800-555-1234</a><span class=\"outer-strip\"></span></h2>
                        <!-- Start Main Menu-->
                        <nav class=\"main-menu\">
                            <div class=\"menu-main-menu-container\"><ul id=\"menu-main-menu\" class=\"clearfix\"><li id=\"menu-item-3646\" class=\"menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3646\"><a href=\"http://realhomes.inspirythemes.biz/\">Home</a>
<ul class=\"sub-menu\">
\t<li id=\"menu-item-3636\" class=\"menu-item menu-item-type-custom menu-item-object-custom menu-item-3636\"><a href=\"http://realhomes.inspirythemes.biz/?module=revolution-slider\">Home with Revolution Slider</a></li>
\t<li id=\"menu-item-3630\" class=\"menu-item menu-item-type-custom menu-item-object-custom menu-item-3630\"><a href=\"http://realhomes.inspirythemes.biz/?module=properties-map\">Home with Google Map</a></li>
\t<li id=\"menu-item-3645\" class=\"menu-item menu-item-type-custom menu-item-object-custom menu-item-3645\"><a href=\"http://realhomes.inspirythemes.biz/?news-on-home=true\">Home with News Posts</a></li>
\t<li id=\"menu-item-3629\" class=\"menu-item menu-item-type-custom menu-item-object-custom menu-item-3629\"><a href=\"http://realhomes.inspirythemes.biz/?module=slides-slider\">Home with Custom Slider</a></li>
\t<li id=\"menu-item-3631\" class=\"menu-item menu-item-type-custom menu-item-object-custom menu-item-3631\"><a href=\"http://realhomes.inspirythemes.biz/?module=properties-slider\">Home with Properties Slider</a></li>
\t<li id=\"menu-item-3632\" class=\"menu-item menu-item-type-custom menu-item-object-custom menu-item-3632\"><a href=\"http://realhomes.inspirythemes.biz/?module=banner\">Home with Simple Banner</a></li>
</ul>
</li>
<li id=\"menu-item-3654\" class=\"menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3654\"><a href=\"http://realhomes.inspirythemes.biz/listing/\">Listing</a>
<ul class=\"sub-menu\">
\t<li id=\"menu-item-3655\" class=\"menu-item menu-item-type-post_type menu-item-object-page menu-item-3655\"><a href=\"http://realhomes.inspirythemes.biz/listing/\">Simple Listing</a></li>
\t<li id=\"menu-item-3633\" class=\"menu-item menu-item-type-custom menu-item-object-custom menu-item-3633\"><a href=\"http://realhomes.inspirythemes.biz/listing/?module=properties-map\">Simple Listing with Google Map</a></li>
\t<li id=\"menu-item-3653\" class=\"menu-item menu-item-type-post_type menu-item-object-page menu-item-3653\"><a href=\"http://realhomes.inspirythemes.biz/grid-listing/\">Grid Listing</a></li>
\t<li id=\"menu-item-3634\" class=\"menu-item menu-item-type-custom menu-item-object-custom menu-item-3634\"><a href=\"http://realhomes.inspirythemes.biz/grid-listing/?module=properties-map\">Grid Listing with Google Map</a></li>
</ul>
</li>
<li id=\"menu-item-3641\" class=\"menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3641\"><a href=\"http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/\">Property</a>
<ul class=\"sub-menu\">
\t<li id=\"menu-item-3642\" class=\"menu-item menu-item-type-custom menu-item-object-custom menu-item-3642\"><a href=\"http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/\">Default &#8211; Variation</a></li>
\t<li id=\"menu-item-3643\" class=\"menu-item menu-item-type-custom menu-item-object-custom menu-item-3643\"><a href=\"http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/?variation=agent-in-sidebar\">Agent in Sidebar &#8211; Variation</a></li>
\t<li id=\"menu-item-3644\" class=\"menu-item menu-item-type-custom menu-item-object-custom menu-item-3644\"><a href=\"http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/?slider-type=thumb-on-bottom\">Gallery &#8211; Variation</a></li>
</ul>
</li>
<li id=\"menu-item-3647\" class=\"menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-3647\"><a href=\"http://realhomes.inspirythemes.biz/news/\">News</a></li>
<li id=\"menu-item-3648\" class=\"menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3648\"><a href=\"http://realhomes.inspirythemes.biz/3-columns-gallery/\">Gallery</a>
<ul class=\"sub-menu\">
\t<li id=\"menu-item-3651\" class=\"menu-item menu-item-type-post_type menu-item-object-page menu-item-3651\"><a href=\"http://realhomes.inspirythemes.biz/2-columns-gallery/\">2 Columns Gallery</a></li>
\t<li id=\"menu-item-3650\" class=\"menu-item menu-item-type-post_type menu-item-object-page menu-item-3650\"><a href=\"http://realhomes.inspirythemes.biz/3-columns-gallery/\">3 Columns Gallery</a></li>
\t<li id=\"menu-item-3649\" class=\"menu-item menu-item-type-post_type menu-item-object-page menu-item-3649\"><a href=\"http://realhomes.inspirythemes.biz/4-columns-gallery/\">4 Columns Gallery</a></li>
</ul>
</li>
<li id=\"menu-item-760\" class=\"menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-760\"><a href=\"#\">Pages</a>
<ul class=\"sub-menu\">
\t<li id=\"menu-item-3608\" class=\"menu-item menu-item-type-post_type menu-item-object-page menu-item-3608\"><a href=\"http://realhomes.inspirythemes.biz/agents/\">Agents</a></li>
\t<li id=\"menu-item-3580\" class=\"menu-item menu-item-type-taxonomy menu-item-object-property-status menu-item-3580\"><a href=\"http://realhomes.inspirythemes.biz/property-status/for-rent/\">For Rent</a></li>
\t<li id=\"menu-item-3579\" class=\"menu-item menu-item-type-taxonomy menu-item-object-property-status menu-item-3579\"><a href=\"http://realhomes.inspirythemes.biz/property-status/for-sale/\">For Sale</a></li>
\t<li id=\"menu-item-761\" class=\"menu-item menu-item-type-taxonomy menu-item-object-property-city menu-item-761\"><a href=\"http://realhomes.inspirythemes.biz/property-city/miami/\">Miami City</a></li>
\t<li id=\"menu-item-3616\" class=\"menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3616\"><a href=\"http://realhomes.inspirythemes.biz/faqs-filterable/\">FAQs</a>
\t<ul class=\"sub-menu\">
\t\t<li id=\"menu-item-3615\" class=\"menu-item menu-item-type-post_type menu-item-object-page menu-item-3615\"><a href=\"http://realhomes.inspirythemes.biz/faqs-filterable/\">FAQs &#8211; Filterable</a></li>
\t\t<li id=\"menu-item-3617\" class=\"menu-item menu-item-type-post_type menu-item-object-page menu-item-3617\"><a href=\"http://realhomes.inspirythemes.biz/faqs-toggle-style/\">FAQs &#8211; Toggle Style</a></li>
\t\t<li id=\"menu-item-3618\" class=\"menu-item menu-item-type-post_type menu-item-object-page menu-item-3618\"><a href=\"http://realhomes.inspirythemes.biz/faqs-simple-list/\">FAQs &#8211; Simple List</a></li>
\t</ul>
</li>
\t<li id=\"menu-item-3619\" class=\"menu-item menu-item-type-post_type menu-item-object-page menu-item-3619\"><a href=\"http://realhomes.inspirythemes.biz/testimonials/\">Testimonials</a></li>
\t<li id=\"menu-item-3613\" class=\"menu-item menu-item-type-post_type menu-item-object-page menu-item-3613\"><a href=\"http://realhomes.inspirythemes.biz/typography/\">Typography</a></li>
\t<li id=\"menu-item-3614\" class=\"menu-item menu-item-type-post_type menu-item-object-page menu-item-3614\"><a href=\"http://realhomes.inspirythemes.biz/columns/\">Columns</a></li>
\t<li id=\"menu-item-3587\" class=\"menu-item menu-item-type-custom menu-item-object-custom menu-item-3587\"><a href=\"http://realhomes.inspirythemes.biz/idx/\">IDX using dsIDXpress Plugin</a></li>
</ul>
</li>
<li id=\"menu-item-3578\" class=\"menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3578\"><a href=\"#\">Types</a>
<ul class=\"sub-menu\">
\t<li id=\"menu-item-763\" class=\"menu-item menu-item-type-taxonomy menu-item-object-property-type menu-item-763\"><a href=\"http://realhomes.inspirythemes.biz/property-type/single-family-home/\">Single Family Home</a></li>
\t<li id=\"menu-item-3590\" class=\"menu-item menu-item-type-taxonomy menu-item-object-property-type menu-item-3590\"><a href=\"http://realhomes.inspirythemes.biz/property-type/apartment-building/\">Apartment Building</a></li>
\t<li id=\"menu-item-3589\" class=\"menu-item menu-item-type-taxonomy menu-item-object-property-type menu-item-3589\"><a href=\"http://realhomes.inspirythemes.biz/property-type/apartment/\">Apartment</a></li>
\t<li id=\"menu-item-3591\" class=\"menu-item menu-item-type-taxonomy menu-item-object-property-type menu-item-3591\"><a href=\"http://realhomes.inspirythemes.biz/property-type/office/\">Office</a></li>
\t<li id=\"menu-item-3592\" class=\"menu-item menu-item-type-taxonomy menu-item-object-property-type menu-item-3592\"><a href=\"http://realhomes.inspirythemes.biz/property-type/shop/\">Shop</a></li>
\t<li id=\"menu-item-762\" class=\"menu-item menu-item-type-taxonomy menu-item-object-property-type menu-item-762\"><a href=\"http://realhomes.inspirythemes.biz/property-type/villa/\">Villa</a></li>
\t<li id=\"menu-item-764\" class=\"menu-item menu-item-type-taxonomy menu-item-object-property-type menu-item-764\"><a href=\"http://realhomes.inspirythemes.biz/property-type/condominium/\">Condominium</a></li>
</ul>
</li>
<li id=\"menu-item-3656\" class=\"menu-item menu-item-type-post_type menu-item-object-page menu-item-3656\"><a href=\"http://realhomes.inspirythemes.biz/contact-us/\">Contact us</a></li>
</ul></div>                        </nav>
                        <!-- End Main Menu -->
                    </div>

                </header>

            </div> <!-- End Header Container -->

        </div><!-- End Header -->

    <div class=\"page-head\" style=\"background-repeat: no-repeat;background-position: center top;background-image: url('http://realhomes.inspirythemes.biz/wp-content/uploads/2015/08/banner.jpg'); background-size: cover;\">
                    <div class=\"container\">
                <div class=\"wrap clearfix\">
                    <h1 class=\"page-title\"><span>Villa in Hialeah, Dade County</span></h1>
                            <div class=\"page-breadcrumbs\">
        <nav class=\"property-breadcrumbs\">
            <ul>
            <li><a href=\"http://realhomes.inspirythemes.biz/\">Home</a><i class=\"breadcrumbs-separator fa fa-angle-right\"></i></li><li><a href=\"http://realhomes.inspirythemes.biz/property-city/miami/\">Miami</a><i class=\"breadcrumbs-separator fa fa-angle-right\"></i></li><li><a href=\"http://realhomes.inspirythemes.biz/property-city/hialeah/\">Hialeah</a></li>            </ul>
        </nav>
        </div>
                        </div>
            </div>
            </div><!-- End Page Head -->

    <!-- Content -->
    <div class=\"container contents detail\">
        <div class=\"row\">
            <div class=\"span9 main-wrap\">

                <!-- Main Content -->
                <div class=\"main\">

                    <div id=\"overview\">
                            <div id=\"property-detail-flexslider\" class=\"clearfix\">
        <div class=\"flexslider\">
            <ul class=\"slides\">
                <li data-thumb=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/kitchen-82x60.jpg\"><a href=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/kitchen.jpg\" class=\"swipebox\" rel=\"gallery_real_homes\"><img src=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/kitchen-770x386.jpg\" alt=\"kitchen\" /></a></li><li data-thumb=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/guest-room-82x60.jpg\"><a href=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/guest-room.jpg\" class=\"swipebox\" rel=\"gallery_real_homes\"><img src=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/guest-room-770x386.jpg\" alt=\"guest-room\" /></a></li><li data-thumb=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/drawing-room-82x60.jpg\"><a href=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/drawing-room.jpg\" class=\"swipebox\" rel=\"gallery_real_homes\"><img src=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/drawing-room-770x386.jpg\" alt=\"drawing-room\" /></a></li><li data-thumb=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/interior-one-82x60.jpg\"><a href=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/interior-one.jpg\" class=\"swipebox\" rel=\"gallery_real_homes\"><img src=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/interior-one-770x386.jpg\" alt=\"interior one\" /></a></li><li data-thumb=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/living-room-02-82x60.jpg\"><a href=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/living-room-02.jpg\" class=\"swipebox\" rel=\"gallery_real_homes\"><img src=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/living-room-02-770x386.jpg\" alt=\"living room\" /></a></li><li data-thumb=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/interior-two-82x60.jpg\"><a href=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/interior-two.jpg\" class=\"swipebox\" rel=\"gallery_real_homes\"><img src=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/interior-two-770x386.jpg\" alt=\"interior two\" /></a></li><li data-thumb=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/kids-room-82x60.jpg\"><a href=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/kids-room.jpg\" class=\"swipebox\" rel=\"gallery_real_homes\"><img src=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/kids-room-770x386.jpg\" alt=\"kids room\" /></a></li><li data-thumb=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/property-08-82x60.jpg\"><a href=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/property-08.jpg\" class=\"swipebox\" rel=\"gallery_real_homes\"><img src=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/property-08-770x386.jpg\" alt=\"property 08\" /></a></li>            </ul>
        </div>
    </div>
            <div id=\"property-featured-image\" class=\"clearfix only-for-print\">
            <img src=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/property-08.jpg\" alt=\"Villa in Hialeah, Dade County\" />        </div>
        <article class=\"property-item clearfix\">
    <div class=\"wrap clearfix\">
        <h4 class=\"title\">
            Property ID : RH1002        </h4>
        <h5 class=\"price\">
            <span class=\"status-label\">
                For Rent            </span>
            <span>
                \$7,500 Per Month<small> - Single Family Home</small>            </span>
        </h5>
    </div>

    <div class=\"property-meta clearfix\">
        <span><svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Layer_1\" x=\"0px\" y=\"0px\" width=\"16px\" height=\"16px\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path class=\"path\" d=\"M14 7.001H2.999C1.342 7 0 8.3 0 10v11c0 1.7 1.3 3 3 3H14c1.656 0 3-1.342 3-3V10 C17 8.3 15.7 7 14 7.001z M14.998 21c0 0.551-0.447 1-0.998 1.002H2.999C2.448 22 2 21.6 2 21V10 c0.001-0.551 0.449-0.999 1-0.999H14c0.551 0 1 0.4 1 0.999V21z\"/>
<path class=\"path\" d=\"M14.266 0.293c-0.395-0.391-1.034-0.391-1.429 0c-0.395 0.39-0.395 1 0 1.415L13.132 2H3.869l0.295-0.292 c0.395-0.391 0.395-1.025 0-1.415c-0.394-0.391-1.034-0.391-1.428 0L0 3l2.736 2.707c0.394 0.4 1 0.4 1.4 0 c0.395-0.391 0.395-1.023 0-1.414L3.869 4.001h9.263l-0.295 0.292c-0.395 0.392-0.395 1 0 1.414s1.034 0.4 1.4 0L17 3 L14.266 0.293z\"/>
<path class=\"path\" d=\"M18.293 9.734c-0.391 0.395-0.391 1 0 1.429s1.023 0.4 1.4 0L20 10.868v9.263l-0.292-0.295 c-0.392-0.395-1.024-0.395-1.415 0s-0.391 1 0 1.428L21 24l2.707-2.736c0.391-0.394 0.391-1.033 0-1.428s-1.023-0.395-1.414 0 l-0.292 0.295v-9.263l0.292 0.295c0.392 0.4 1 0.4 1.4 0s0.391-1.034 0-1.429L21 7L18.293 9.734z\"/>
</svg>
4800&nbsp;sq ft</span><span><svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Layer_1\" x=\"0px\" y=\"0px\" width=\"16px\" height=\"16px\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<circle class=\"circle\" cx=\"5\" cy=\"8.3\" r=\"2.2\"/>
<path class=\"path\" d=\"M0 22.999C0 23.6 0.4 24 1 24S2 23.6 2 22.999V18H2h20h0.001v4.999c0 0.6 0.4 1 1 1 C23.552 24 24 23.6 24 22.999V10C24 9.4 23.6 9 23 9C22.447 9 22 9.4 22 10v1H22h-0.999V10.5 C20.999 8 20 6 17.5 6H11C9.769 6.1 8.2 6.3 8 8v3H2H2V9C2 8.4 1.6 8 1 8S0 8.4 0 9V22.999z M10.021 8.2 C10.19 8.1 10.6 8 11 8h5.5c1.382 0 2.496-0.214 2.5 2.501v0.499h-9L10.021 8.174z M22 16H2v-2.999h20V16z\"/>
</svg>
4&nbsp;Bedrooms</span><span><svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Layer_1\" x=\"0px\" y=\"0px\" width=\"16px\" height=\"16px\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path class=\"path\" d=\"M23.001 12h-1.513C21.805 11.6 22 11.1 22 10.5C22 9.1 20.9 8 19.5 8S17 9.1 17 10.5 c0 0.6 0.2 1.1 0.5 1.5H2.999c0-0.001 0-0.002 0-0.002V2.983V2.98c0.084-0.169-0.083-0.979 1-0.981h0.006 C4.008 2 4.3 2 4.5 2.104L4.292 2.292c-0.39 0.392-0.39 1 0 1.415c0.391 0.4 1 0.4 1.4 0l2-1.999 c0.39-0.391 0.39-1.025 0-1.415c-0.391-0.391-1.023-0.391-1.415 0L5.866 0.72C5.775 0.6 5.7 0.5 5.5 0.4 C4.776 0 4.1 0 4 0H3.984v0.001C1.195 0 1 2.7 1 2.98v0.019v0.032v8.967c0 0 0 0 0 0.002H0.999 C0.447 12 0 12.4 0 12.999S0.447 14 1 14H1v2.001c0.001 2.6 1.7 4.8 4 5.649V23c0 0.6 0.4 1 1 1s1-0.447 1-1v-1h10v1 c0 0.6 0.4 1 1 1s1-0.447 1-1v-1.102c2.745-0.533 3.996-3.222 4-5.897V14h0.001C23.554 14 24 13.6 24 13 S23.554 12 23 12z M21.001 16.001c-0.091 2.539-0.927 3.97-3.001 3.997H7c-2.208-0.004-3.996-1.79-4-3.997V14h15.173 c-0.379 0.484-0.813 0.934-1.174 1.003c-0.54 0.104-0.999 0.446-0.999 1c0 0.6 0.4 1 1 1 c2.159-0.188 3.188-2.006 3.639-2.999h0.363V16.001z\"/>
<rect class=\"rect\" x=\"6.6\" y=\"4.1\" transform=\"matrix(-0.7071 0.7071 -0.7071 -0.7071 15.6319 3.2336)\" width=\"1\" height=\"1.4\"/>
<rect class=\"rect\" x=\"9.4\" y=\"2.4\" transform=\"matrix(0.7066 0.7076 -0.7076 0.7066 4.9969 -6.342)\" width=\"1.4\" height=\"1\"/>
<rect class=\"rect\" x=\"9.4\" y=\"6.4\" transform=\"matrix(0.7071 0.7071 -0.7071 0.7071 7.8179 -5.167)\" width=\"1.4\" height=\"1\"/>
<rect class=\"rect\" x=\"12.4\" y=\"4.4\" transform=\"matrix(0.7069 0.7073 -0.7073 0.7069 7.2858 -7.8754)\" width=\"1.4\" height=\"1\"/>
<rect class=\"rect\" x=\"13.4\" y=\"7.4\" transform=\"matrix(-0.7064 -0.7078 0.7078 -0.7064 18.5823 23.4137)\" width=\"1.4\" height=\"1\"/>
</svg>
3&nbsp;Bathrooms</span><span><svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Layer_1\" x=\"0px\" y=\"0px\" width=\"16px\" height=\"16px\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path class=\"path\" d=\"M23.958 0.885c-0.175-0.64-0.835-1.016-1.475-0.842l-11 3.001c-0.64 0.173-1.016 0.833-0.842 1.5 c0.175 0.6 0.8 1 1.5 0.842L16 4.299V6.2h-0.001H13c-2.867 0-4.892 1.792-5.664 2.891L5.93 11.2H5.024 c-0.588-0.029-2.517-0.02-3.851 1.221C0.405 13.1 0 14.1 0 15.201V18.2v2H2h2.02C4.126 22.3 5.9 24 8 24 c2.136 0 3.873-1.688 3.979-3.801H16V24h2V3.754l5.116-1.396C23.756 2.2 24.1 1.5 24 0.885z M8 22 c-1.104 0-2-0.896-2-2.001s0.896-2 2-2S10 18.9 10 20S9.105 22 8 22.001z M11.553 18.2C10.891 16.9 9.6 16 8 16 c-1.556 0-2.892 0.901-3.553 2.201H2v-2.999c0-0.599 0.218-1.019 0.537-1.315C3.398 13.1 5 13.2 5 13.2h2L9 10.2 c0 0 1.407-1.999 4-1.999h2.999H16v10H11.553z\"/>
</svg>
2&nbsp;Garages</span>    <span class=\"add-to-fav\">
                    <form action=\"http://realhomes.inspirythemes.biz/wp-admin/admin-ajax.php\" method=\"post\" id=\"add-to-favorite-form\">
                <input type=\"hidden\" name=\"property_id\" value=\"79\" />
                <input type=\"hidden\" name=\"action\" value=\"add_to_favorite\" />
            </form>
            <div id=\"fav_output\">
\t            <i class=\"fa fa-star-o dim\"></i>&nbsp;
\t            <span id=\"fav_target\" class=\"dim\"></span>
            </div>
            <a id=\"add-to-favorite\" href=\"#add-to-favorite\">
\t            <i class=\"fa fa-star-o\"></i>&nbsp;Add to Favorites            </a>
                </span>
            <!-- Print link -->
        <span class=\"printer-icon\"><a href=\"javascript:window.print()\"><i class=\"fa fa-print\"></i>Print</a></span>
    </div>

    <div class=\"content clearfix\">
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
<p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>
<h4 class=\"additional-title\">Additional Details</h4><ul class=\"additional-details clearfix\">        <li>
            <strong>AC:</strong>
            <span>Central</span>
        </li>
                <li>
            <strong>ACRES:</strong>
            <span>0.14</span>
        </li>
                <li>
            <strong>ACRES SOURCE:</strong>
            <span>Assessor</span>
        </li>
                <li>
            <strong>AP #:</strong>
            <span>2639-017-020</span>
        </li>
                <li>
            <strong>APPLIANCES:</strong>
            <span>Cooktop - Gas, Dishwasher, Freestanding Gas Range, Garbage Disposal</span>
        </li>
                <li>
            <strong>BATHROOM DESCRIPTION:</strong>
            <span>Remodeled, Skylight(s), Tile Walls</span>
        </li>
                <li>
            <strong>CROSS STREETS:</strong>
            <span>NORDOFF/ WAKEFIELD</span>
        </li>
                <li>
            <strong>DISABILITY ACCESS:</strong>
            <span>Main Level Entry</span>
        </li>
                <li>
            <strong>DOORS &amp; WINDOWS:</strong>
            <span>Double Pane Windows, Skylights</span>
        </li>
                <li>
            <strong>FIREPLACE DESCRIPTION:</strong>
            <span>Brick</span>
        </li>
                <li>
            <strong>FIREPLACE FUEL:</strong>
            <span>Uses Both Gas &amp; Wood</span>
        </li>
                <li>
            <strong>FIREPLACE LOCATION:</strong>
            <span>Living Room</span>
        </li>
                <li>
            <strong>FLOORS:</strong>
            <span>Carpet - Partial, Ceramic Tile</span>
        </li>
                <li>
            <strong>PLUMBING:</strong>
            <span>Full Copper Plumbing</span>
        </li>
        </ul>        <div class=\"common-note\">
            <h4 class=\"common-note-heading\">Common Note</h4><p>A common note across all properties to describe some policies or terms that are applicable on all the properties. This common note can be disabled from theme options.</p>        </div>
            </div>


            <div class=\"features\">
            <h4 class=\"title\">Features</h4>            <ul class=\"arrow-bullet-list clearfix\">
            <li><a href=\"http://realhomes.inspirythemes.biz/property-feature/2-stories/\">2 Stories</a></li><li><a href=\"http://realhomes.inspirythemes.biz/property-feature/central-heating/\">Central Heating</a></li><li><a href=\"http://realhomes.inspirythemes.biz/property-feature/dual-sinks/\">Dual Sinks</a></li><li><a href=\"http://realhomes.inspirythemes.biz/property-feature/electric-range/\">Electric Range</a></li><li><a href=\"http://realhomes.inspirythemes.biz/property-feature/fire-place/\">Fire Place</a></li><li><a href=\"http://realhomes.inspirythemes.biz/property-feature/laundry-room/\">Laundry Room</a></li><li><a href=\"http://realhomes.inspirythemes.biz/property-feature/lawn/\">Lawn</a></li><li><a href=\"http://realhomes.inspirythemes.biz/property-feature/marble-floors/\">Marble Floors</a></li><li><a href=\"http://realhomes.inspirythemes.biz/property-feature/swimming-pool/\">Swimming Pool</a></li>            </ul>
        </div>
        </article>        <div class=\"floor-plans\">

\t\t\t<h3 class=\"floor-plans-label\">Floor Plans</h3>

\t\t\t<div class=\"floor-plans-accordions\">
                \t\t\t\t\t<div class=\"floor-plan\">
\t\t\t\t\t\t<div class=\"floor-plan-title clearfix\">
\t\t\t\t\t\t\t<i class=\"fa fa-plus\"></i>
\t\t\t\t\t\t\t<h3>Ground Floor</h3>
\t\t\t\t\t\t\t<div class=\"floor-plan-meta\">
\t\t\t\t\t\t\t\t<div>2500 sq ft</div><div>1 Bedroom</div><div>1 Bathroom</div><div class=\"floor-price\"><span class=\"floor-price-value\">\$4,000</span> Per Month</div>\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"floor-plan-content\">

\t\t                    \t                            <div class=\"floor-plan-desc\">
\t\t                            <p>This is a sample floor plan to demonstrate how you can display your floor plans using this theme. </p>
<p>Curabitur blandit tempus porttitor. Sed posuere consectetur est at lobortis. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Etiam porta sem malesuada magna mollis euismod. Nulla vitae elit libero, a pharetra augue. Vestibulum id ligula porta felis euismod semper.</p>
\t                            </div>
\t\t                    
\t\t                    \t                        <div class=\"floor-plan-map\">
\t                            <a href=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2015/08/inspiry-0-ground-floor.jpg\" class=\"swipebox\" rel=\"gallery_floor_plans\">
\t                                <img src=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2015/08/inspiry-0-ground-floor.jpg\" >
\t                            </a>
\t                        </div>
\t\t                    \t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t                \t\t\t\t\t<div class=\"floor-plan\">
\t\t\t\t\t\t<div class=\"floor-plan-title clearfix\">
\t\t\t\t\t\t\t<i class=\"fa fa-plus\"></i>
\t\t\t\t\t\t\t<h3>First Floor</h3>
\t\t\t\t\t\t\t<div class=\"floor-plan-meta\">
\t\t\t\t\t\t\t\t<div>2300 sq ft</div><div>3 Bedrooms</div><div>2 Bathrooms</div><div class=\"floor-price\"><span class=\"floor-price-value\">\$3,500</span> Per Month</div>\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"floor-plan-content\">

\t\t                    \t                            <div class=\"floor-plan-desc\">
\t\t                            <p>This is a sample floor plan to demonstrate how you can display your floor plans using this theme.</p>
<p>Curabitur blandit tempus porttitor. Sed posuere consectetur est at lobortis. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Etiam porta sem malesuada magna mollis euismod. Nulla vitae elit libero, a pharetra augue. Vestibulum id ligula porta felis euismod semper.</p>
\t                            </div>
\t\t                    
\t\t                    \t                        <div class=\"floor-plan-map\">
\t                            <a href=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2015/08/inspiry-0-first-floor.jpg\" class=\"swipebox\" rel=\"gallery_floor_plans\">
\t                                <img src=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2015/08/inspiry-0-first-floor.jpg\" >
\t                            </a>
\t                        </div>
\t\t                    \t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t                \t\t\t</div>
        </div>
               
            <div class=\"attachments-wrap clearfix\">
            <span class=\"attachments-label\">Property Attachments</span>            <div class=\"attachments-inner clearfix\">
                <ul class=\"attachments-list clearfix\"><li class=\"jpg\"><a target=\"_blank\" href=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2015/08/inspiry-0-ground-floor.jpg\"><i class=\"fa fa-file-image-o\"></i>Ground Floor</a></li><li class=\"jpg\"><a target=\"_blank\" href=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2015/08/inspiry-0-first-floor.jpg\"><i class=\"fa fa-file-image-o\"></i>First Floor</a></li><li class=\"pdf\"><a target=\"_blank\" href=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2014/06/demo.pdf\"><i class=\"fa fa-file-pdf-o\"></i>Demo PDF</a></li></ul>            </div>
        </div>
        \t<div class=\"agent-detail clearfix\">

\t\t<div class=\"left-box\">
\t\t\t<h3>Agent Julia Robert</h3>\t\t\t\t\t<figure>
\t\t\t\t\t\t<a href=\"http://realhomes.inspirythemes.biz/agent/julia-robert/\">
\t\t\t\t\t\t\t<img width=\"210\" height=\"210\" src=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/agent.jpg\" class=\"attachment-agent-image size-agent-image wp-post-image\" alt=\"Julia Robert\" srcset=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/agent.jpg 210w, http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/agent-150x150.jpg 150w\" sizes=\"(max-width: 210px) 100vw, 210px\" />\t\t\t\t\t\t</a>
\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t<ul class=\"contacts-list\">
\t\t\t\t\t\t\t\t\t<li class=\"office\">
\t\t\t\t\t\t<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Layer_1\" x=\"0px\" y=\"0px\" width=\"16px\" height=\"16px\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path class=\"path\" d=\"M5.597 1.999C6.4 2 8 2 8.1 5.51C8.017 6.1 7.8 6.3 7.5 6.658C7.12 7.1 6.4 7.9 6.4 9.2 c0 1.5 1 3.1 3.1 5.198c1.143 1.2 3.3 3.1 4.8 3.123c1.222 0 1.856-0.836 2.128-1.192 c0.18-0.236 0.284-0.373 0.862-0.468h0.055c3.91 0 4.6 0.8 4.6 2.476c0 0.358-0.151 1.891-0.893 2.8 c-0.297 0.382-1.161 0.789-2.695 0.789c-3.012 0-7.658-1.598-11.249-5.162C2.085 11.7 2 4.7 2 4.6 C2.021 2.2 4.8 2 5.6 2 M5.598-0.001c-0.385 0-5.578 0.084-5.578 4.624c0 0-0.041 7.8 5.8 13.6 c3.825 3.8 8.9 5.7 12.7 5.747c1.914 0 3.46-0.515 4.273-1.56c1.314-1.688 1.314-4.07 1.314-4.07 c0-4.618-4.734-4.47-6.801-4.476c-2.345 0.324-2.198 1.66-2.843 1.66c-0.187 0-1.32-0.393-3.409-2.523 c-2.106-2.085-2.527-3.234-2.527-3.797c0-1.265 1.396-1.328 1.662-3.602c-0.004-1.028-0.011-5.563-4.482-5.632 C5.621-0.001 5.613-0.001 5.598-0.001L5.598-0.001z\"/>
</svg>
Office : 041-456-3692\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li class=\"mobile\">
\t\t\t\t\t\t<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Layer_1\" x=\"0px\" y=\"0px\" width=\"16px\" height=\"16px\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path class=\"path\" fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M18 24c1.657 0 2.999-1.341 3-2.999V2.999C20.999 1.3 19.7 0 18 0H6.001 C4.343 0 3 1.3 3 2.999v18.002C3.001 22.7 4.3 24 6 24H18z M6.001 22.001c-0.553 0-1-0.448-1.001-1V2.999 C5.001 2.4 5.4 2 6 2h9C15.552 2 16 2.4 16 2.999v18.002c-0.002 0.552-0.448 1-0.999 1H6.001z M19 21 c-0.001 0.552-0.447 1-1 1h-0.184c0.111-0.314 0.184-0.647 0.184-1V2.999C18 2.6 17.9 2.3 17.8 2H18 c0.553 0 1 0.4 1 0.999V21.001z\"/>
<circle class=\"circle\" fill-rule=\"evenodd\" clip-rule=\"evenodd\" cx=\"10.5\" cy=\"19.5\" r=\"1.5\"/>
<path class=\"path\" fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M12 2.999H8.999C8.447 3 8 3.4 8 4c0 0.6 0.4 1 1 1H12 c0.552 0 1-0.448 1-1C13 3.4 12.6 3 12 2.999z\"/>
</svg>
Mobile : 0200-123-4567\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li class=\"fax\">
\t\t\t\t\t\t<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Layer_1\" x=\"0px\" y=\"0px\" width=\"16px\" height=\"16px\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<g>
\t<path class=\"path\" d=\"M14.415 0H6v10h1h11V3.585L14.415 0z M16 8H8V2h5.586L16 4.413V8z\"/>
\t<path class=\"path\" d=\"M15.001 14.001H8.999C7.343 14 6 15.3 6 17v7h1c0.469 0 0.9 0 2 0h6.002c1.104 0 1.6 0 2 0h1v-7 C18 15.3 16.7 14 15 14.001z M16 22c-0.264 0-0.586 0-0.999 0H8.999c-0.417 0-0.737 0-1 0v-5 c0.001-0.552 0.449-0.998 1-1h6.002c0.55 0 1 0.4 1 1V22z\"/>
</g>
<g>
\t<path class=\"path\" d=\"M7 18H3.001c-0.917-0.029-0.844-0.142-0.855-0.146C2.067 17.8 2 17.1 2 16v-6c0.029-1.481 0.922-1.894 3.001-2H7V6 H5.001C2.66 6 0 7.1 0 10v6c0.02 1.111-0.047 1.9 0.4 2.803c0.499 0.9 1.6 1.2 2.6 1.197H7V18z\"/>
\t<path class=\"path\" d=\"M19.001 6H17v2h1.999c2.078 0.1 3 0.5 3 2v6c0.02 1.098-0.067 1.78-0.146 1.9 c-0.011 0 0.1 0.116-0.855 0.146H17v2h3.999c1.046 0 2.117-0.255 2.616-1.197C24.048 17.9 24 17.1 24 16v-6 C23.995 7.1 21.3 6 19 6z\"/>
</g>
<polygon class=\"path\" fill-rule=\"evenodd\" clip-rule=\"evenodd\" points=\"18,4.4 13.6,4.4 13.6,0 13.6,0 18,4.4 18,4.4\"/>
<path class=\"path\" d=\"M20.499 17c-0.275 0-0.5-0.224-0.5-0.5v-0.999c0-0.276 0.225-0.5 0.5-0.5c0.277 0 0.5 0.2 0.5 0.5V16.5 C21.001 16.8 20.8 17 20.5 17L20.499 17z M4 11C3.292 11 3 10.7 3 10C3 9.2 3.3 9 4 8.999c0.77 0 1 0.2 1 1 C5 10.7 4.8 11 4 11L4 11z\"/>
</svg>
Fax : 041-789-4561\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t<p>Lorem agent info ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna&hellip;<br/><a class=\"real-btn\" href=\"http://realhomes.inspirythemes.biz/agent/julia-robert/\">Know More</a></p>
\t\t</div>

\t\t\t\t\t<div class=\"contact-form\">
\t\t\t\t<h3>Contact</h3>
\t\t\t\t<form id=\"agent-form-id110\" class=\"agent-form contact-form-small\" method=\"post\" action=\"http://realhomes.inspirythemes.biz/wp-admin/admin-ajax.php\">
\t\t\t\t\t<input type=\"text\" name=\"name\" placeholder=\"Name\" class=\"required\" title=\"* Please provide your name\">
\t\t\t\t\t<input type=\"text\" name=\"email\" placeholder=\"Email\" class=\"email required\" title=\"* Please provide valid email address\">
\t\t\t\t\t<textarea name=\"message\" class=\"required\" placeholder=\"Message\" title=\"* Please provide your message\"></textarea>
\t\t\t\t\t    <div id=\"recaptcha_widget\" style=\"display:none\" class=\"recaptcha_widget\">
        <div id=\"recaptcha_image\"></div>
        <div class=\"recaptcha_only_if_incorrect_sol\" style=\"color:red\">Incorrect. Please try again.</div>

        <div class=\"recaptcha_input\">
            <label class=\"recaptcha_only_if_image\" for=\"recaptcha_response_field\">Enter the words above:</label>
            <label class=\"recaptcha_only_if_audio\" for=\"recaptcha_response_field\">Enter the numbers you hear:</label>

            <input type=\"text\" id=\"recaptcha_response_field\" name=\"recaptcha_response_field\">
        </div>

        <ul class=\"recaptcha_options\">
            <li>
                <a href=\"javascript:Recaptcha.reload()\">
                    <i class=\"icon-refresh\"></i>
                    <span class=\"captcha_hide\">Get another CAPTCHA</span>
                </a>
            </li>
            <li class=\"recaptcha_only_if_image\">
                <a href=\"javascript:Recaptcha.switch_type('audio')\">
                    <i class=\"icon-volume-up\"></i><span class=\"captcha_hide\"> Get an audio CAPTCHA</span>
                </a>
            </li>
            <li class=\"recaptcha_only_if_audio\">
                <a href=\"javascript:Recaptcha.switch_type('image')\">
                    <i class=\"icon-picture\"></i><span class=\"captcha_hide\"> Get an image CAPTCHA</span>
                </a>
            </li>
            <li>
                <a href=\"javascript:Recaptcha.showhelp()\">
                    <i class=\"icon-question-sign\"></i><span class=\"captcha_hide\"> Help</span>
                </a>
            </li>
        </ul>
    </div>

    <script type=\"text/javascript\" src=\"//www.google.com/recaptcha/api/challenge?k=6Ldf7fQSAAAAAD52MkF0ZMwi09JoycTSGBLe2RW5\"></script>
    <noscript>
        <iframe src=\"//www.google.com/recaptcha/api/noscript?k=6Ldf7fQSAAAAAD52MkF0ZMwi09JoycTSGBLe2RW5\" height=\"300\" width=\"220\" frameborder=\"0\"></iframe><br>
        <textarea name=\"recaptcha_challenge_field\"></textarea>
        <input type=\"hidden\" name=\"recaptcha_response_field\" value=\"manual_challenge\">
    </noscript>
    \t\t\t\t\t<input type=\"hidden\" name=\"nonce\" value=\"7c7cc1e8b9\"/>
\t\t\t\t\t<input type=\"hidden\" name=\"target\" value=\"r&#111;&#98;&#111;t&#64;in&#115;p&#105;ry&#116;h&#101;m&#101;&#115;.&#98;&#105;&#122;\">
\t\t\t\t\t<input type=\"hidden\" name=\"action\" value=\"send_message_to_agent\"/>
\t\t\t\t\t<input type=\"hidden\" name=\"property_title\" value=\"Villa in Hialeah, Dade County\"/>
\t\t\t\t\t<input type=\"hidden\" name=\"property_permalink\" value=\"http://realhomes.inspirythemes.biz/property/villa-in-hialeah-dade-county/\"/>
\t\t\t\t\t<input type=\"submit\" value=\"Send Message\" name=\"submit\" class=\"submit-button real-btn\">
\t\t\t\t\t<img src=\"http://realhomes.inspirythemes.biz/wp-content/themes/realhomes/images/ajax-loader.gif\" class=\"ajax-loader\" alt=\"Loading...\">
\t\t\t\t\t<div class=\"clearfix form-separator\"></div>
\t\t\t\t\t<div class=\"error-container\"></div>
\t\t\t\t\t<div class=\"message-container\"></div>
\t\t\t\t</form>
\t\t\t</div>
\t\t\t<script type=\"text/javascript\">
\t\t\t\t(function(\$){
\t\t\t\t\t\"use strict\";

\t\t\t\t\tif ( jQuery().validate && jQuery().ajaxSubmit ) {

\t\t\t\t\t\tvar agentForm = \$('#agent-form-id110');
\t\t\t\t\t\tvar submitButton = agentForm.find( '.submit-button' ),
\t\t\t\t\t\t\tajaxLoader = agentForm.find( '.ajax-loader' ),
\t\t\t\t\t\t\tmessageContainer = agentForm.find( '.message-container' ),
\t\t\t\t\t\t\terrorContainer = agentForm.find( \".error-container\" );

\t\t\t\t\t\t// Property detail page form
\t\t\t\t\t\tagentForm.validate( {
\t\t\t\t\t\t\terrorLabelContainer: errorContainer,
\t\t\t\t\t\t\tsubmitHandler : function( form ) {
\t\t\t\t\t\t\t\t\$(form).ajaxSubmit( {
\t\t\t\t\t\t\t\t\tbeforeSubmit: function(){
\t\t\t\t\t\t\t\t\t\tsubmitButton.attr('disabled','disabled');
\t\t\t\t\t\t\t\t\t\tajaxLoader.fadeIn('fast');
\t\t\t\t\t\t\t\t\t\tmessageContainer.fadeOut('fast');
\t\t\t\t\t\t\t\t\t\terrorContainer.fadeOut('fast');
\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\tsuccess: function( ajax_response, statusText, xhr, \$form) {
\t\t\t\t\t\t\t\t\t\tvar response = \$.parseJSON ( ajax_response );
\t\t\t\t\t\t\t\t\t\tajaxLoader.fadeOut('fast');
\t\t\t\t\t\t\t\t\t\tsubmitButton.removeAttr('disabled');
\t\t\t\t\t\t\t\t\t\tif( response.success ) {
\t\t\t\t\t\t\t\t\t\t\t\$form.resetForm();
\t\t\t\t\t\t\t\t\t\t\tmessageContainer.html( response.message ).fadeIn('fast');
\t\t\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\t\t\terrorContainer.html( response.message ).fadeIn('fast');
\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t} );
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} );

\t\t\t\t\t}

\t\t\t\t})(jQuery);
\t\t\t</script>
\t\t\t\t</div>
\t                    </div>

                </div><!-- End Main Content -->

                            <section class=\"listing-layout property-grid\">
                <div class=\"list-container clearfix\">
                    <h3>Similar Properties</h3><article class=\"property-item clearfix\">

    <figure>
        <a href=\"http://realhomes.inspirythemes.biz/property/apartment-building-having-35-apartments/15-apartments-of-type-b/\">
            <img width=\"246\" height=\"162\" src=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2014/06/apartment-b-246x162.jpg\" class=\"attachment-grid-view-image size-grid-view-image wp-post-image\" alt=\"Apartment interior\" srcset=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2014/06/apartment-b-246x162.jpg 246w, http://realhomes.inspirythemes.biz/wp-content/uploads/2014/06/apartment-b-150x100.jpg 150w\" sizes=\"(max-width: 246px) 100vw, 246px\" />        </a>

        <figcaption class=\"for-rent\">For Rent</figcaption>
    </figure>


    <h4><a href=\"http://realhomes.inspirythemes.biz/property/apartment-building-having-35-apartments/15-apartments-of-type-b/\">15 Apartments of Type B</a></h4>
    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed&hellip; <a class=\"more-details\" href=\"http://realhomes.inspirythemes.biz/property/apartment-building-having-35-apartments/15-apartments-of-type-b/\">More Details <i class=\"fa fa-caret-right\"></i></a></p>
    <span>\$2,200 Per Month</span></article><article class=\"property-item clearfix\">

    <figure>
        <a href=\"http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/\">
            <img width=\"246\" height=\"162\" src=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/property-10-246x162.jpg\" class=\"attachment-grid-view-image size-grid-view-image wp-post-image\" alt=\"Demo JPG\" />        </a>

        <figcaption class=\"for-rent\">For Rent</figcaption>
    </figure>


    <h4><a href=\"http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/\">15421 Southwest 39th Terrace</a></h4>
    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed&hellip; <a class=\"more-details\" href=\"http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/\">More Details <i class=\"fa fa-caret-right\"></i></a></p>
    <span>\$3,850 Per Month</span></article><article class=\"property-item clearfix\">

    <figure>
        <a href=\"http://realhomes.inspirythemes.biz/property/60-merrick-way-miami/\">
            <img width=\"246\" height=\"162\" src=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/property-07-246x162.jpg\" class=\"attachment-grid-view-image size-grid-view-image wp-post-image\" alt=\"property-07\" />        </a>

        <figcaption class=\"for-sale\">For Sale</figcaption>
    </figure>


    <h4><a href=\"http://realhomes.inspirythemes.biz/property/60-merrick-way-miami/\">60 Merrick Way, Miami</a></h4>
    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed&hellip; <a class=\"more-details\" href=\"http://realhomes.inspirythemes.biz/property/60-merrick-way-miami/\">More Details <i class=\"fa fa-caret-right\"></i></a></p>
    <span>\$440,000 </span></article>                </div>
            </section>
            
            </div> <!-- End span9 -->

            <div class=\"span3 sidebar-wrap\">

    <!-- Sidebar -->
    <aside class=\"sidebar\">
        <section class=\"widget advance-search\"><h4 class=\"title search-heading\">Find Your Home<i class=\"fa fa-search\"></i></h4>\t<div class=\"as-form-wrap\">
\t    <form class=\"advance-search-form clearfix\" action=\"http://realhomes.inspirythemes.biz/property-search/\" method=\"get\">
\t    <div class=\"option-bar large\">
\t<label for=\"keyword-txt\">
\t\tKeyword\t</label>
\t<input type=\"text\" name=\"keyword\" id=\"keyword-txt\"
\t       value=\"\"
\t       placeholder=\"Any\"/>
</div><div class=\"option-bar large\">
\t<label for=\"property-id-txt\">
\t\tProperty ID\t</label>
\t<input type=\"text\" name=\"property-id\" id=\"property-id-txt\"
\t       value=\"\"
\t       placeholder=\"Any\" />
</div>\t<div class=\"option-bar large\">
\t\t<label for=\"location\">
\t\t\tLocation\t\t</label>
        <span class=\"selectwrap\">
            <select name=\"location\" id=\"location\" class=\"search-select\"></select>
        </span>
\t</div>
\t<div class=\"option-bar large\">
\t<label for=\"select-status\">
\t\tProperty Status\t</label>
    <span class=\"selectwrap\">
        <select name=\"status\" id=\"select-status\" class=\"search-select\">
            <option value=\"any\" selected=\"selected\">Any</option><option value=\"for-rent\">For Rent</option><option value=\"for-sale\">For Sale</option>        </select>
    </span>
</div><div class=\"option-bar large\">
\t<label for=\"select-property-type\">
\t\tProperty Type\t</label>
    <span class=\"selectwrap\">
        <select name=\"type\" id=\"select-property-type\" class=\"search-select\">
\t        <option value=\"any\" selected=\"selected\">Any</option><option value=\"commercial\"> Commercial</option><option value=\"office\">-  Office</option><option value=\"shop\">-  Shop</option><option value=\"residential\"> Residential</option><option value=\"apartment\">-  Apartment</option><option value=\"apartment-building\">-  Apartment Building</option><option value=\"condominium\">-  Condominium</option><option value=\"single-family-home\">-  Single Family Home</option><option value=\"villa\">-  Villa</option>        </select>
    </span>
</div><div class=\"option-bar small\">
\t<label for=\"select-bedrooms\">
\t\tMin Beds\t</label>
    <span class=\"selectwrap\">
        <select name=\"bedrooms\" id=\"select-bedrooms\" class=\"search-select\">
            <option value=\"any\" selected=\"selected\">Any</option><option value=\"1\">1</option><option value=\"2\">2</option><option value=\"3\">3</option><option value=\"4\">4</option><option value=\"5\">5</option><option value=\"6\">6</option><option value=\"7\">7</option><option value=\"8\">8</option><option value=\"9\">9</option><option value=\"10\">10</option>        </select>
    </span>
</div><div class=\"option-bar small\">
\t<label for=\"select-bathrooms\">
\t\tMin Baths\t</label>
    <span class=\"selectwrap\">
        <select name=\"bathrooms\" id=\"select-bathrooms\" class=\"search-select\">
            <option value=\"any\" selected=\"selected\">Any</option><option value=\"1\">1</option><option value=\"2\">2</option><option value=\"3\">3</option><option value=\"4\">4</option><option value=\"5\">5</option><option value=\"6\">6</option><option value=\"7\">7</option><option value=\"8\">8</option><option value=\"9\">9</option><option value=\"10\">10</option>        </select>
    </span>
</div><div class=\"option-bar small price-for-others\">
\t<label for=\"select-min-price\">
\t\tMin Price\t</label>
    <span class=\"selectwrap\">
        <select name=\"min-price\" id=\"select-min-price\" class=\"search-select\">
            <option value=\"any\" selected=\"selected\">Any</option><option value=\"1000\">\$1,000</option><option value=\"5000\">\$5,000</option><option value=\"10000\">\$10,000</option><option value=\"50000\">\$50,000</option><option value=\"100000\">\$100,000</option><option value=\"200000\">\$200,000</option><option value=\"300000\">\$300,000</option><option value=\"400000\">\$400,000</option><option value=\"500000\">\$500,000</option><option value=\"600000\">\$600,000</option><option value=\"700000\">\$700,000</option><option value=\"800000\">\$800,000</option><option value=\"900000\">\$900,000</option><option value=\"1000000\">\$1,000,000</option><option value=\"1500000\">\$1,500,000</option><option value=\"2000000\">\$2,000,000</option><option value=\"2500000\">\$2,500,000</option><option value=\"5000000\">\$5,000,000</option>        </select>
    </span>
</div>

<div class=\"option-bar small price-for-others\">
\t<label for=\"select-max-price\">
\t\tMax Price\t</label>
    <span class=\"selectwrap\">
        <select name=\"max-price\" id=\"select-max-price\" class=\"search-select\">
            <option value=\"any\" selected=\"selected\">Any</option><option value=\"5000\">\$5,000</option><option value=\"10000\">\$10,000</option><option value=\"50000\">\$50,000</option><option value=\"100000\">\$100,000</option><option value=\"200000\">\$200,000</option><option value=\"300000\">\$300,000</option><option value=\"400000\">\$400,000</option><option value=\"500000\">\$500,000</option><option value=\"600000\">\$600,000</option><option value=\"700000\">\$700,000</option><option value=\"800000\">\$800,000</option><option value=\"900000\">\$900,000</option><option value=\"1000000\">\$1,000,000</option><option value=\"1500000\">\$1,500,000</option><option value=\"2000000\">\$2,000,000</option><option value=\"2500000\">\$2,500,000</option><option value=\"5000000\">\$5,000,000</option><option value=\"10000000\">\$10,000,000</option>        </select>
    </span>
</div>

<div class=\"option-bar small price-for-rent hide-fields\">
\t<label for=\"select-min-price-for-rent\">
\t\tMin Price\t</label>
    <span class=\"selectwrap\">
        <select name=\"min-price\" id=\"select-min-price-for-rent\" class=\"search-select\" disabled=\"disabled\">
            <option value=\"any\" selected=\"selected\">Any</option><option value=\"500\">\$500</option><option value=\"1000\">\$1,000</option><option value=\"2000\">\$2,000</option><option value=\"3000\">\$3,000</option><option value=\"4000\">\$4,000</option><option value=\"5000\">\$5,000</option><option value=\"7500\">\$7,500</option><option value=\"10000\">\$10,000</option><option value=\"15000\">\$15,000</option><option value=\"20000\">\$20,000</option><option value=\"25000\">\$25,000</option><option value=\"30000\">\$30,000</option><option value=\"40000\">\$40,000</option><option value=\"50000\">\$50,000</option><option value=\"75000\">\$75,000</option><option value=\"100000\">\$100,000</option>        </select>
    </span>
</div>

<div class=\"option-bar small price-for-rent hide-fields\">
\t<label for=\"select-max-price-for-rent\">
\t\tMax Price\t</label>
    <span class=\"selectwrap\">
        <select name=\"max-price\" id=\"select-max-price-for-rent\" class=\"search-select\" disabled=\"disabled\">
            <option value=\"any\" selected=\"selected\">Any</option><option value=\"1000\">\$1,000</option><option value=\"2000\">\$2,000</option><option value=\"3000\">\$3,000</option><option value=\"4000\">\$4,000</option><option value=\"5000\">\$5,000</option><option value=\"7500\">\$7,500</option><option value=\"10000\">\$10,000</option><option value=\"15000\">\$15,000</option><option value=\"20000\">\$20,000</option><option value=\"25000\">\$25,000</option><option value=\"30000\">\$30,000</option><option value=\"40000\">\$40,000</option><option value=\"50000\">\$50,000</option><option value=\"75000\">\$75,000</option><option value=\"100000\">\$100,000</option><option value=\"150000\">\$150,000</option>        </select>
    </span>
</div><div class=\"option-bar small\">
\t<label for=\"min-area\">
\t\tMin Area\t\t<span>(sq ft)</span>
\t</label>
\t<input type=\"text\" name=\"min-area\" id=\"min-area\" pattern=\"[0-9]+\"
\t       value=\"\"
\t       placeholder=\"Any\"
\t       title=\"Only provide digits!\" />
</div>

<div class=\"option-bar small\">
\t<label for=\"max-area\">
\t\tMax Area\t\t<span>(sq ft)</span>
\t</label>
\t<input type=\"text\" name=\"max-area\" id=\"max-area\" pattern=\"[0-9]+\"
\t       value=\"\"
\t       placeholder=\"Any\"
\t       title=\"Only provide digits!\" />
</div><div class=\"option-bar\">
\t\t<input type=\"submit\" value=\"Search\" class=\"real-btn btn\">
</div>\t<div class=\"clearfix\"></div>

\t<div class=\"more-option-trigger\">
\t\t<a href=\"#\">
\t\t\t<i class=\"fa fa-plus-square-o\"></i>
\t\t\tLooking for certain features\t\t</a>
\t</div>

\t<div class=\"more-options-wrapper clearfix collapsed\">
\t\t\t\t\t<div class=\"option-bar\">
\t\t\t\t<input type=\"checkbox\"
\t\t\t\t       id=\"feature-2-stories\"
\t\t\t\t       name=\"features[]\"
\t\t\t\t       value=\"2-stories\"
\t\t\t\t\t />
\t\t\t\t<label for=\"feature-2-stories\">2 Stories <small>(6)</small></label>
\t\t\t</div>
\t\t\t\t\t\t<div class=\"option-bar\">
\t\t\t\t<input type=\"checkbox\"
\t\t\t\t       id=\"feature-26-ceilings\"
\t\t\t\t       name=\"features[]\"
\t\t\t\t       value=\"26-ceilings\"
\t\t\t\t\t />
\t\t\t\t<label for=\"feature-26-ceilings\">26' Ceilings <small>(1)</small></label>
\t\t\t</div>
\t\t\t\t\t\t<div class=\"option-bar\">
\t\t\t\t<input type=\"checkbox\"
\t\t\t\t       id=\"feature-central-heating\"
\t\t\t\t       name=\"features[]\"
\t\t\t\t       value=\"central-heating\"
\t\t\t\t\t />
\t\t\t\t<label for=\"feature-central-heating\">Central Heating <small>(7)</small></label>
\t\t\t</div>
\t\t\t\t\t\t<div class=\"option-bar\">
\t\t\t\t<input type=\"checkbox\"
\t\t\t\t       id=\"feature-dual-sinks\"
\t\t\t\t       name=\"features[]\"
\t\t\t\t       value=\"dual-sinks\"
\t\t\t\t\t />
\t\t\t\t<label for=\"feature-dual-sinks\">Dual Sinks <small>(7)</small></label>
\t\t\t</div>
\t\t\t\t\t\t<div class=\"option-bar\">
\t\t\t\t<input type=\"checkbox\"
\t\t\t\t       id=\"feature-electric-range\"
\t\t\t\t       name=\"features[]\"
\t\t\t\t       value=\"electric-range\"
\t\t\t\t\t />
\t\t\t\t<label for=\"feature-electric-range\">Electric Range <small>(10)</small></label>
\t\t\t</div>
\t\t\t\t\t\t<div class=\"option-bar\">
\t\t\t\t<input type=\"checkbox\"
\t\t\t\t       id=\"feature-emergency-exit\"
\t\t\t\t       name=\"features[]\"
\t\t\t\t       value=\"emergency-exit\"
\t\t\t\t\t />
\t\t\t\t<label for=\"feature-emergency-exit\">Emergency Exit <small>(7)</small></label>
\t\t\t</div>
\t\t\t\t\t\t<div class=\"option-bar\">
\t\t\t\t<input type=\"checkbox\"
\t\t\t\t       id=\"feature-fire-alarm\"
\t\t\t\t       name=\"features[]\"
\t\t\t\t       value=\"fire-alarm\"
\t\t\t\t\t />
\t\t\t\t<label for=\"feature-fire-alarm\">Fire Alarm <small>(5)</small></label>
\t\t\t</div>
\t\t\t\t\t\t<div class=\"option-bar\">
\t\t\t\t<input type=\"checkbox\"
\t\t\t\t       id=\"feature-fire-place\"
\t\t\t\t       name=\"features[]\"
\t\t\t\t       value=\"fire-place\"
\t\t\t\t\t />
\t\t\t\t<label for=\"feature-fire-place\">Fire Place <small>(9)</small></label>
\t\t\t</div>
\t\t\t\t\t\t<div class=\"option-bar\">
\t\t\t\t<input type=\"checkbox\"
\t\t\t\t       id=\"feature-home-theater\"
\t\t\t\t       name=\"features[]\"
\t\t\t\t       value=\"home-theater\"
\t\t\t\t\t />
\t\t\t\t<label for=\"feature-home-theater\">Home Theater <small>(3)</small></label>
\t\t\t</div>
\t\t\t\t\t\t<div class=\"option-bar\">
\t\t\t\t<input type=\"checkbox\"
\t\t\t\t       id=\"feature-hurricane-shutters\"
\t\t\t\t       name=\"features[]\"
\t\t\t\t       value=\"hurricane-shutters\"
\t\t\t\t\t />
\t\t\t\t<label for=\"feature-hurricane-shutters\">Hurricane Shutters <small>(1)</small></label>
\t\t\t</div>
\t\t\t\t\t\t<div class=\"option-bar\">
\t\t\t\t<input type=\"checkbox\"
\t\t\t\t       id=\"feature-laundry-room\"
\t\t\t\t       name=\"features[]\"
\t\t\t\t       value=\"laundry-room\"
\t\t\t\t\t />
\t\t\t\t<label for=\"feature-laundry-room\">Laundry Room <small>(6)</small></label>
\t\t\t</div>
\t\t\t\t\t\t<div class=\"option-bar\">
\t\t\t\t<input type=\"checkbox\"
\t\t\t\t       id=\"feature-lawn\"
\t\t\t\t       name=\"features[]\"
\t\t\t\t       value=\"lawn\"
\t\t\t\t\t />
\t\t\t\t<label for=\"feature-lawn\">Lawn <small>(6)</small></label>
\t\t\t</div>
\t\t\t\t\t\t<div class=\"option-bar\">
\t\t\t\t<input type=\"checkbox\"
\t\t\t\t       id=\"feature-marble-floors\"
\t\t\t\t       name=\"features[]\"
\t\t\t\t       value=\"marble-floors\"
\t\t\t\t\t />
\t\t\t\t<label for=\"feature-marble-floors\">Marble Floors <small>(8)</small></label>
\t\t\t</div>
\t\t\t\t\t\t<div class=\"option-bar\">
\t\t\t\t<input type=\"checkbox\"
\t\t\t\t       id=\"feature-next-to-busway\"
\t\t\t\t       name=\"features[]\"
\t\t\t\t       value=\"next-to-busway\"
\t\t\t\t\t />
\t\t\t\t<label for=\"feature-next-to-busway\">NEXT To Busway <small>(2)</small></label>
\t\t\t</div>
\t\t\t\t\t\t<div class=\"option-bar\">
\t\t\t\t<input type=\"checkbox\"
\t\t\t\t       id=\"feature-swimming-pool\"
\t\t\t\t       name=\"features[]\"
\t\t\t\t       value=\"swimming-pool\"
\t\t\t\t\t />
\t\t\t\t<label for=\"feature-swimming-pool\">Swimming Pool <small>(6)</small></label>
\t\t\t</div>
\t\t\t\t\t\t<div class=\"option-bar\">
\t\t\t\t<input type=\"checkbox\"
\t\t\t\t       id=\"feature-wifi\"
\t\t\t\t       name=\"features[]\"
\t\t\t\t       value=\"wifi\"
\t\t\t\t\t />
\t\t\t\t<label for=\"feature-wifi\">Wifi <small>(6)</small></label>
\t\t\t</div>
\t\t\t\t</div>
\t\t    </form>
\t</div>
\t</section><section id=\"featured_properties_widget-11\" class=\"widget clearfix Featured_Properties_Widget\"><h3 class=\"title\">Featured Properties</h3>            <ul class=\"featured-properties\">
                                    <li>

                        <figure>
                            <a href=\"http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/\">
                            <img width=\"246\" height=\"162\" src=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/property-10-246x162.jpg\" class=\"attachment-grid-view-image size-grid-view-image wp-post-image\" alt=\"Demo JPG\" />                            </a>
                        </figure>

                        <h4><a href=\"http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/\">15421 Southwest 39th Terrace</a></h4>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing&hellip; <a href=\"http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/\">Read More</a></p>
                        <span class=\"price\">\$3,850 Per Month</span>                    </li>
                                        <li>

                        <figure>
                            <a href=\"http://realhomes.inspirythemes.biz/property/60-merrick-way-miami/\">
                            <img width=\"246\" height=\"162\" src=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/property-07-246x162.jpg\" class=\"attachment-grid-view-image size-grid-view-image wp-post-image\" alt=\"property-07\" />                            </a>
                        </figure>

                        <h4><a href=\"http://realhomes.inspirythemes.biz/property/60-merrick-way-miami/\">60 Merrick Way, Miami</a></h4>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing&hellip; <a href=\"http://realhomes.inspirythemes.biz/property/60-merrick-way-miami/\">Read More</a></p>
                        <span class=\"price\">\$440,000 </span>                    </li>
                                </ul>
            </section>    </aside><!-- End Sidebar -->

</div>
        </div><!-- End contents row -->
    </div><!-- End Content -->

    <div class=\"container page-carousel\">
        <div class=\"row\">
            <div class=\"span12\">
                <section class=\"brands-carousel  clearfix\">
                                        <h3><span>Partners</span></h3>
                            <ul class=\"brands-carousel-list clearfix\">
                                                                        <li>
                                            <a target=\"_blank\" href=\"http://graphicriver.net/\" title=\"graphicriver\">
                                                <img width=\"190\" height=\"55\" src=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-4.png\" class=\"attachment-partners-logo size-partners-logo wp-post-image\" alt=\"graphicriver\" title=\"graphicriver\" srcset=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-4.png 190w, http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-4-150x43.png 150w\" sizes=\"(max-width: 190px) 100vw, 190px\" />                                            </a>
                                        </li>
                                                                                <li>
                                            <a target=\"_blank\" href=\"http://photodune.net/\" title=\"photodune\">
                                                <img width=\"190\" height=\"55\" src=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-2.png\" class=\"attachment-partners-logo size-partners-logo wp-post-image\" alt=\"photodune\" title=\"photodune\" srcset=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-2.png 190w, http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-2-150x43.png 150w\" sizes=\"(max-width: 190px) 100vw, 190px\" />                                            </a>
                                        </li>
                                                                                <li>
                                            <a target=\"_blank\" href=\"http://themeforest.net/\" title=\"themeforest\">
                                                <img width=\"190\" height=\"55\" src=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-1.png\" class=\"attachment-partners-logo size-partners-logo wp-post-image\" alt=\"themeforest\" title=\"themeforest\" srcset=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-1.png 190w, http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-1-150x43.png 150w\" sizes=\"(max-width: 190px) 100vw, 190px\" />                                            </a>
                                        </li>
                                                                                <li>
                                            <a target=\"_blank\" href=\"http://activeden.net/\" title=\"activeden\">
                                                <img width=\"190\" height=\"55\" src=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-3.png\" class=\"attachment-partners-logo size-partners-logo wp-post-image\" alt=\"activeden\" title=\"activeden\" srcset=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-3.png 190w, http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-3-150x43.png 150w\" sizes=\"(max-width: 190px) 100vw, 190px\" />                                            </a>
                                        </li>
                                                                                <li>
                                            <a target=\"_blank\" href=\"http://graphicriver.net/\" title=\"graphicriver\">
                                                <img width=\"190\" height=\"55\" src=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-4.png\" class=\"attachment-partners-logo size-partners-logo wp-post-image\" alt=\"graphicriver\" title=\"graphicriver\" srcset=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-4.png 190w, http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-4-150x43.png 150w\" sizes=\"(max-width: 190px) 100vw, 190px\" />                                            </a>
                                        </li>
                                                                                <li>
                                            <a target=\"_blank\" href=\"http://videohive.net/\" title=\"videohive\">
                                                <img width=\"190\" height=\"55\" src=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-5.png\" class=\"attachment-partners-logo size-partners-logo wp-post-image\" alt=\"videohive\" title=\"videohive\" srcset=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-5.png 190w, http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-5-150x43.png 150w\" sizes=\"(max-width: 190px) 100vw, 190px\" />                                            </a>
                                        </li>
                                                                                <li>
                                            <a target=\"_blank\" href=\"http://photodune.net/\" title=\"photodune\">
                                                <img width=\"190\" height=\"55\" src=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-2.png\" class=\"attachment-partners-logo size-partners-logo wp-post-image\" alt=\"photodune\" title=\"photodune\" srcset=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-2.png 190w, http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-2-150x43.png 150w\" sizes=\"(max-width: 190px) 100vw, 190px\" />                                            </a>
                                        </li>
                                                                                <li>
                                            <a target=\"_blank\" href=\"http://themeforest.net/\" title=\"themeforest\">
                                                <img width=\"190\" height=\"55\" src=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-1.png\" class=\"attachment-partners-logo size-partners-logo wp-post-image\" alt=\"themeforest\" title=\"themeforest\" srcset=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-1.png 190w, http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-1-150x43.png 150w\" sizes=\"(max-width: 190px) 100vw, 190px\" />                                            </a>
                                        </li>
                                                                    </ul>
                </section>
            </div>
        </div>
    </div>
    
<!-- Start Footer -->
<footer id=\"footer-wrapper\">

       <div id=\"footer\" class=\"container\">

                <div class=\"row\">

                        <div class=\"span3\">
                            <section id=\"text-3\" class=\"widget clearfix widget_text\"><h3 class=\"title\">About Real Homes</h3>\t\t\t<div class=\"textwidget\"><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
<p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
</div>
\t\t</section>                        </div>

                        <div class=\"span3\">
                            \t\t<section id=\"recent-posts-4\" class=\"widget clearfix widget_recent_entries\">\t\t<h3 class=\"title\">Recent Posts</h3>\t\t<ul>
\t\t\t\t\t<li>
\t\t\t\t<a href=\"http://realhomes.inspirythemes.biz/lorem-post-with-image-format/\">Lorem Post With Image Format</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t<a href=\"http://realhomes.inspirythemes.biz/example-video-blog-post/\">Example Video Blog Post</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t<a href=\"http://realhomes.inspirythemes.biz/example-post-with-gallery-post-format/\">Example Post With Gallery Post Format</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t<a href=\"http://realhomes.inspirythemes.biz/example-post-with-image-post-format/\">Example Post With Image Post Format</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t<a href=\"http://realhomes.inspirythemes.biz/lorem-ipsum-dolor-sit-amet/\">Lorem Ipsum Dolor Sit Amet</a>
\t\t\t\t\t\t</li>
\t\t\t\t</ul>
\t\t</section>\t\t                        </div>

                        <div class=\"clearfix visible-tablet\"></div>

                        <div class=\"span3\">
                            <section id=\"displaytweetswidget-2\" class=\"widget clearfix widget_displaytweetswidget\"><h3 class=\"title\">Latest Tweets</h3><p>You should clean out your <a href=\"http://twitter.com/search?q=%23WordPress&src=hash\" target=\"_blank\">#WordPress</a> Themes Directory.
<a href=\"https://t.co/bsDsl4qjJA\" target=\"_blank\">https://t.co/bsDsl4qjJA</a><br /><small class=\"muted\">- Friday May 13 - 4:59am</small></p><p>Both of our <a href=\"http://twitter.com/search?q=%23RealEstate&src=hash\" target=\"_blank\">#RealEstate</a> <a href=\"http://twitter.com/search?q=%23WordPRess&src=hash\" target=\"_blank\">#WordPRess</a> Themes have the latest Visual Composer Plugin 4.11.2.1
<a href=\"https://t.co/j9ofmliCmS\" target=\"_blank\">https://t.co/j9ofmliCmS</a>
<a href=\"https://t.co/J3iLTWPJ0J\" target=\"_blank\">https://t.co/J3iLTWPJ0J</a><br /><small class=\"muted\">- Thursday Apr 28 - 8:53am</small></p></section>                        </div>

                        <div class=\"span3\">
                            <section id=\"text-2\" class=\"widget clearfix widget_text\"><h3 class=\"title\">Contact Info</h3>\t\t\t<div class=\"textwidget\"><p>3015 Grand Ave, Coconut Grove,<br />
Merrick Way, FL 12345</p>
<p>Phone: 123-456-7890</p>
<p>Email: <a href=\"mailto:info@yourwebsite.com\">info@yourwebsite.com</a></p>
</div>
\t\t</section>                        </div>
                </div>

       </div>

        <!-- Footer Bottom -->
        <div id=\"footer-bottom\" class=\"container\">

                <div class=\"row\">
                        <div class=\"span6\">
                            <p class=\"copyright\">Copyright © 2015. All Rights Reserved.</p>                        </div>
                        <div class=\"span6\">
                            <p class=\"designed-by\">A Theme by <a target=\"_blank\" href=\"http://themeforest.net/user/inspirythemes/portfolio\">Inspiry Themes</a></p>                        </div>
                </div>

        </div>
        <!-- End Footer Bottom -->

</footer><!-- End Footer -->

<!-- Login Modal -->
<div id=\"login-modal\" class=\"forms-modal modal hide fade\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">

    <div class=\"modal-header\">
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
        <p>You need to log in to use member only features.</p>
    </div>

    <!-- start of modal body -->
    <div class=\"modal-body\">

        <!-- login section -->
        <div class=\"login-section modal-section\">
            <h4>Login</h4>
            <form id=\"login-form\" class=\"login-form\" action=\"http://realhomes.inspirythemes.biz/wp-admin/admin-ajax.php\" method=\"post\" enctype=\"multipart/form-data\">
                <div class=\"form-option\">
                    <label for=\"username\">User Name<span>*</span></label>
                    <input id=\"username\" name=\"log\" type=\"text\" class=\"required\" title=\"* Provide user name!\" autofocus required/>
                </div>
                <div class=\"form-option\">
                    <label for=\"password\">Password<span>*</span></label>
                    <input id=\"password\" name=\"pwd\" type=\"password\" class=\"required\" title=\"* Provide password!\" required/>
                </div>
                <input type=\"hidden\" name=\"action\" value=\"inspiry_ajax_login\" />
                <input type=\"hidden\" id=\"inspiry-secure-login\" name=\"inspiry-secure-login\" value=\"72515f4468\" /><input type=\"hidden\" name=\"_wp_http_referer\" value=\"/property/villa-in-hialeah-dade-county/\" /><input type=\"hidden\" name=\"redirect_to\" value=\"http://realhomes.inspirythemes.biz/property/villa-in-hialeah-dade-county/\" />                <input type=\"hidden\" name=\"user-cookie\" value=\"1\" />
                <input type=\"submit\" id=\"login-button\" name=\"submit\" value=\"Log in\" class=\"real-btn login-btn\" />
                <img id=\"login-loader\" class=\"modal-loader\" src=\"http://realhomes.inspirythemes.biz/wp-content/themes/realhomes/images/ajax-loader.gif\" alt=\"Working...\">
                <div>
                    <div id=\"login-message\" class=\"modal-message\"></div>
                    <div id=\"login-error\" class=\"modal-error\"></div>
                </div>
            </form>
            <p>
                                    <a class=\"activate-section\" data-section=\"register-section\" href=\"#\">Register Here</a>
                    <span class=\"divider\">-</span>
                                <a class=\"activate-section\" data-section=\"forgot-section\" href=\"#\">Forgot Password</a>
            </p>
        </div>

        <!-- forgot section -->
        <div class=\"forgot-section modal-section\">
            <h4>Reset Password</h4>
            <form action=\"http://realhomes.inspirythemes.biz/wp-admin/admin-ajax.php\" id=\"forgot-form\"  method=\"post\" enctype=\"multipart/form-data\">
                <div class=\"form-option\">
                    <label for=\"reset_username_or_email\">Username or Email<span>*</span></label>
                    <input id=\"reset_username_or_email\" name=\"reset_username_or_email\" type=\"text\" class=\"required\" title=\"* Provide username or email!\" required/>
                </div>
                <input type=\"hidden\" name=\"action\" value=\"inspiry_ajax_forgot\" />
                <input type=\"hidden\" name=\"user-cookie\" value=\"1\" />
                <input type=\"submit\"  id=\"forgot-button\" name=\"user-submit\" value=\"Reset Password\" class=\"real-btn register-btn\" />
\t            <img id=\"forgot-loader\" class=\"modal-loader\" src=\"http://realhomes.inspirythemes.biz/wp-content/themes/realhomes/images/ajax-loader.gif\" alt=\"Working...\">
                <input type=\"hidden\" id=\"inspiry-secure-reset\" name=\"inspiry-secure-reset\" value=\"a5876dde24\" /><input type=\"hidden\" name=\"_wp_http_referer\" value=\"/property/villa-in-hialeah-dade-county/\" />                <div>
                    <div id=\"forgot-message\" class=\"modal-message\"></div>
                    <div id=\"forgot-error\" class=\"modal-error\"></div>
                </div>
            </form>
            <p>
                <a class=\"activate-section\" data-section=\"login-section\" href=\"#\">Login Here</a>
                                    <span class=\"divider\">-</span>
                    <a class=\"activate-section\" data-section=\"register-section\" href=\"#\">Register Here</a>
                            </p>
        </div>

                    <!-- register section -->
            <div class=\"register-section modal-section\">
                <h4>Register</h4>
                <form action=\"http://realhomes.inspirythemes.biz/wp-admin/admin-ajax.php\" id=\"register-form\"  method=\"post\" enctype=\"multipart/form-data\">

                    <div class=\"form-option\">
                        <label for=\"register_username\" class=\"\">User Name<span>*</span></label>
                        <input id=\"register_username\" name=\"register_username\" type=\"text\" class=\"required\"
                               title=\"* Provide user name!\" required/>
                    </div>

                    <div class=\"form-option\">
                        <label for=\"register_email\" class=\"\">Email<span>*</span></label>
                        <input id=\"register_email\" name=\"register_email\" type=\"text\" class=\"email required\"
                               title=\"* Provide valid email address!\" required/>
                    </div>

                    <input type=\"hidden\" name=\"user-cookie\" value=\"1\" />
                    <input type=\"submit\" id=\"register-button\" name=\"user-submit\" value=\"Register\" class=\"real-btn register-btn\" />
\t                <img id=\"register-loader\" class=\"modal-loader\" src=\"http://realhomes.inspirythemes.biz/wp-content/themes/realhomes/images/ajax-loader.gif\" alt=\"Working...\">
                    <input type=\"hidden\" name=\"action\" value=\"inspiry_ajax_register\" />
                    <input type=\"hidden\" id=\"inspiry-secure-register\" name=\"inspiry-secure-register\" value=\"f998f8232b\" /><input type=\"hidden\" name=\"_wp_http_referer\" value=\"/property/villa-in-hialeah-dade-county/\" /><input type=\"hidden\" name=\"redirect_to\" value=\"http://realhomes.inspirythemes.biz/property/villa-in-hialeah-dade-county/\" />
                    <div>
                        <div id=\"register-message\" class=\"modal-message\"></div>
                        <div id=\"register-error\" class=\"modal-error\"></div>
                    </div>

                </form>
                <p>
                    <a class=\"activate-section\" data-section=\"login-section\" href=\"#\">Login Here</a>
                    <span class=\"divider\">-</span>
                    <a class=\"activate-section\" data-section=\"forgot-section\" href=\"#\">Forgot Password</a>
                </p>
            </div>
        
    </div>
    <!-- end of modal-body -->

</div>
<a href=\"#top\" id=\"scroll-top\"><i class=\"fa fa-chevron-up\"></i></a>

<script type='text/javascript' src='http://realhomes.inspirythemes.biz/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://realhomes.inspirythemes.biz/wp-includes/js/jquery/ui/widget.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://realhomes.inspirythemes.biz/wp-includes/js/jquery/ui/position.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://realhomes.inspirythemes.biz/wp-includes/js/jquery/ui/menu.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://realhomes.inspirythemes.biz/wp-includes/js/wp-a11y.min.js?ver=4.5.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var uiAutocompleteL10n = {\"noResults\":\"No search results.\",\"oneResult\":\"1 result found. Use up and down arrow keys to navigate.\",\"manyResults\":\"%d results found. Use up and down arrow keys to navigate.\"};
/* ]]> */
</script>
<script type='text/javascript' src='http://realhomes.inspirythemes.biz/wp-includes/js/jquery/ui/autocomplete.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://realhomes.inspirythemes.biz/wp-content/themes/realhomes/js/inspiry-login-register.js?ver=2.5.5'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var localizedSearchParams = {\"rent_slug\":\"for-rent\"};
var locationData = {\"any_text\":\"Any\",\"any_value\":\"any\",\"all_locations\":[{\"term_id\":27,\"name\":\"Miami\",\"slug\":\"miami\",\"parent\":0},{\"term_id\":41,\"name\":\"Little Havana\",\"slug\":\"little-havana\",\"parent\":27},{\"term_id\":30,\"name\":\"Perrine\",\"slug\":\"perrine\",\"parent\":27},{\"term_id\":40,\"name\":\"Doral\",\"slug\":\"doral\",\"parent\":27},{\"term_id\":48,\"name\":\"Hialeah\",\"slug\":\"hialeah\",\"parent\":27}],\"select_names\":[\"location\",\"child-location\",\"grandchild-location\",\"great-grandchild-location\"],\"select_count\":\"1\",\"locations_in_params\":[]};
/* ]]> */
</script>
<script type='text/javascript' src='http://realhomes.inspirythemes.biz/wp-content/themes/realhomes/js/inspiry-search-form.js?ver=2.5.5'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var localized = {\"nav_title\":\"Go to...\"};
/* ]]> */
</script>
<script type='text/javascript' src='http://realhomes.inspirythemes.biz/wp-content/themes/realhomes/js/custom.js?ver=2.5.5'></script>
<script type='text/javascript' src='http://realhomes.inspirythemes.biz/wp-includes/js/wp-embed.min.js?ver=4.5.2'></script>
<script type='text/javascript' src='http://realhomes.inspirythemes.biz/wp-content/plugins/dsidxpress/js/autocomplete.js?ver=2.1.32'></script>
</body>
";
        
        $__internal_0d2d6802322f3e28c8b9ddda1024dfc1e31ac1ab244d5f3d0bb7faf0ccaa4fcb->leave($__internal_0d2d6802322f3e28c8b9ddda1024dfc1e31ac1ab244d5f3d0bb7faf0ccaa4fcb_prof);

    }

    public function getTemplateName()
    {
        return "executari/details.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 6,  34 => 5,  11 => 1,);
    }
}
/* {% extends 'head.html.twig' %}*/
/* {#{% block body %}#}*/
/*     {#{% extends 'base.html.twig' %}#}*/
/* */
/* {% block body %}*/
/*     <body class="single single-property postid-79">*/
/* */
/*         <!-- Start Header -->*/
/*         <div class="header-wrapper">*/
/* */
/*             <div class="container"><!-- Start Header Container -->*/
/* */
/*                 <header id="header" class="clearfix">*/
/* */
/*                     <div id="header-top" class="clearfix">*/
/*                                                     <h2 id="contact-email">*/
/*                                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">*/
/* <path class="path" d="M8.174 15.926l-6.799 5.438c-0.431 0.346-0.501 0.975-0.156 1.406s0.974 0.5 1.4 0.156l7.211-5.769L8.174 15.926z"/>*/
/* <path class="path" d="M15.838 15.936l-1.685 1.214l7.222 5.777c0.433 0.3 1.1 0.3 1.406-0.156c0.345-0.432 0.274-1.061-0.157-1.406 L15.838 15.936z"/>*/
/* <polygon class="path" points="1,10.2 1.6,10.9 12,2.6 22,10.6 22,22 2,22 2,10.2 1,10.2 1.6,10.9 1,10.2 0,10.2 0,24 24,24 24,9.7 12,0 0,9.7 0,10.2 1,10.2 1,10.2"/>*/
/* <polygon class="path" points="23.6,11.7 12.6,19.7 11.4,19.7 0.4,11.7 0.4,11.7 0.4,11.7 1.6,10.1 12,17.6 22.4,10.1"/>*/
/* </svg>*/
/* Email us at :*/
/*                                 <a href="mailto:s&#97;&#108;e&#115;&#64;&#121;&#111;&#117;rwebs&#105;te.c&#111;m">&#115;&#97;&#108;&#101;s&#64;you&#114;&#119;&#101;&#98;s&#105;t&#101;.&#99;&#111;m</a>*/
/*                             </h2>*/
/*                             */
/*                         <!-- Social Navigation -->*/
/*                             <ul class="social_networks clearfix">*/
/*                             <li class="facebook">*/
/*                     <a target="_blank" href="https://www.facebook.com/InspiryThemes"><i class="fa fa-facebook fa-lg"></i></a>*/
/*                 </li>*/
/*                                 <li class="twitter">*/
/*                     <a target="_blank" href="https://twitter.com/InspiryThemes"><i class="fa fa-twitter fa-lg"></i></a>*/
/*                 </li>*/
/*                                 <li class="linkedin">*/
/*                     <a target="_blank" href="#"><i class="fa fa-linkedin fa-lg"></i></a>*/
/*                 </li>*/
/*                                 <li class="gplus">*/
/*                     <a target="_blank" href="#"><i class="fa fa-google-plus fa-lg"></i></a>*/
/*                 </li>*/
/*                                 <li class="rss">*/
/*                     <a target="_blank" href="http://realhomes.inspirythemes.biz/feed/?post_type=property"> <i class="fa fa-rss fa-lg"></i></a>*/
/*                 </li>*/
/*                     </ul>*/
/*     */
/*                         <!-- User Navigation -->*/
/*                         	<div class="user-nav clearfix">*/
/* 					<a href="http://realhomes.inspirythemes.biz/favorites/">*/
/* 				<i class="fa fa-star"></i>Favorites			</a>*/
/* 			<a class="last" href="#login-modal" data-toggle="modal"><i class="fa fa-sign-in"></i>Login / Register</a>	</div>*/
/* 	*/
/*                     </div>*/
/* */
/*                     <!-- Logo -->*/
/*                     <div id="logo">*/
/* */
/*                                                     <a title="Real Homes" href="http://realhomes.inspirythemes.biz">*/
/*                                 <img src="http://realhomes.inspirythemes.biz/wp-content/uploads/2015/08/logo.png" alt="Real Homes">*/
/*                             </a>*/
/*                             <h2 class="logo-heading only-for-print">*/
/*                                 <a href="http://realhomes.inspirythemes.biz"  title="Real Homes">*/
/*                                     Real Homes                                </a>*/
/*                             </h2>*/
/*                             <div class="tag-line"><span>No. 1 Real Estate Theme</span></div>                    </div>*/
/* */
/* */
/*                     <div class="menu-and-contact-wrap">*/
/*                         <h2  class="contact-number "><i class="fa fa-phone"></i><span class="desktop-version">1-800-555-1234</span><a class="mobile-version" href="tel://1-800-555-1234" title="Make a Call">1-800-555-1234</a><span class="outer-strip"></span></h2>*/
/*                         <!-- Start Main Menu-->*/
/*                         <nav class="main-menu">*/
/*                             <div class="menu-main-menu-container"><ul id="menu-main-menu" class="clearfix"><li id="menu-item-3646" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3646"><a href="http://realhomes.inspirythemes.biz/">Home</a>*/
/* <ul class="sub-menu">*/
/* 	<li id="menu-item-3636" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3636"><a href="http://realhomes.inspirythemes.biz/?module=revolution-slider">Home with Revolution Slider</a></li>*/
/* 	<li id="menu-item-3630" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3630"><a href="http://realhomes.inspirythemes.biz/?module=properties-map">Home with Google Map</a></li>*/
/* 	<li id="menu-item-3645" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3645"><a href="http://realhomes.inspirythemes.biz/?news-on-home=true">Home with News Posts</a></li>*/
/* 	<li id="menu-item-3629" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3629"><a href="http://realhomes.inspirythemes.biz/?module=slides-slider">Home with Custom Slider</a></li>*/
/* 	<li id="menu-item-3631" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3631"><a href="http://realhomes.inspirythemes.biz/?module=properties-slider">Home with Properties Slider</a></li>*/
/* 	<li id="menu-item-3632" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3632"><a href="http://realhomes.inspirythemes.biz/?module=banner">Home with Simple Banner</a></li>*/
/* </ul>*/
/* </li>*/
/* <li id="menu-item-3654" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3654"><a href="http://realhomes.inspirythemes.biz/listing/">Listing</a>*/
/* <ul class="sub-menu">*/
/* 	<li id="menu-item-3655" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3655"><a href="http://realhomes.inspirythemes.biz/listing/">Simple Listing</a></li>*/
/* 	<li id="menu-item-3633" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3633"><a href="http://realhomes.inspirythemes.biz/listing/?module=properties-map">Simple Listing with Google Map</a></li>*/
/* 	<li id="menu-item-3653" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3653"><a href="http://realhomes.inspirythemes.biz/grid-listing/">Grid Listing</a></li>*/
/* 	<li id="menu-item-3634" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3634"><a href="http://realhomes.inspirythemes.biz/grid-listing/?module=properties-map">Grid Listing with Google Map</a></li>*/
/* </ul>*/
/* </li>*/
/* <li id="menu-item-3641" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3641"><a href="http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/">Property</a>*/
/* <ul class="sub-menu">*/
/* 	<li id="menu-item-3642" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3642"><a href="http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/">Default &#8211; Variation</a></li>*/
/* 	<li id="menu-item-3643" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3643"><a href="http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/?variation=agent-in-sidebar">Agent in Sidebar &#8211; Variation</a></li>*/
/* 	<li id="menu-item-3644" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3644"><a href="http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/?slider-type=thumb-on-bottom">Gallery &#8211; Variation</a></li>*/
/* </ul>*/
/* </li>*/
/* <li id="menu-item-3647" class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-3647"><a href="http://realhomes.inspirythemes.biz/news/">News</a></li>*/
/* <li id="menu-item-3648" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3648"><a href="http://realhomes.inspirythemes.biz/3-columns-gallery/">Gallery</a>*/
/* <ul class="sub-menu">*/
/* 	<li id="menu-item-3651" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3651"><a href="http://realhomes.inspirythemes.biz/2-columns-gallery/">2 Columns Gallery</a></li>*/
/* 	<li id="menu-item-3650" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3650"><a href="http://realhomes.inspirythemes.biz/3-columns-gallery/">3 Columns Gallery</a></li>*/
/* 	<li id="menu-item-3649" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3649"><a href="http://realhomes.inspirythemes.biz/4-columns-gallery/">4 Columns Gallery</a></li>*/
/* </ul>*/
/* </li>*/
/* <li id="menu-item-760" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-760"><a href="#">Pages</a>*/
/* <ul class="sub-menu">*/
/* 	<li id="menu-item-3608" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3608"><a href="http://realhomes.inspirythemes.biz/agents/">Agents</a></li>*/
/* 	<li id="menu-item-3580" class="menu-item menu-item-type-taxonomy menu-item-object-property-status menu-item-3580"><a href="http://realhomes.inspirythemes.biz/property-status/for-rent/">For Rent</a></li>*/
/* 	<li id="menu-item-3579" class="menu-item menu-item-type-taxonomy menu-item-object-property-status menu-item-3579"><a href="http://realhomes.inspirythemes.biz/property-status/for-sale/">For Sale</a></li>*/
/* 	<li id="menu-item-761" class="menu-item menu-item-type-taxonomy menu-item-object-property-city menu-item-761"><a href="http://realhomes.inspirythemes.biz/property-city/miami/">Miami City</a></li>*/
/* 	<li id="menu-item-3616" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3616"><a href="http://realhomes.inspirythemes.biz/faqs-filterable/">FAQs</a>*/
/* 	<ul class="sub-menu">*/
/* 		<li id="menu-item-3615" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3615"><a href="http://realhomes.inspirythemes.biz/faqs-filterable/">FAQs &#8211; Filterable</a></li>*/
/* 		<li id="menu-item-3617" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3617"><a href="http://realhomes.inspirythemes.biz/faqs-toggle-style/">FAQs &#8211; Toggle Style</a></li>*/
/* 		<li id="menu-item-3618" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3618"><a href="http://realhomes.inspirythemes.biz/faqs-simple-list/">FAQs &#8211; Simple List</a></li>*/
/* 	</ul>*/
/* </li>*/
/* 	<li id="menu-item-3619" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3619"><a href="http://realhomes.inspirythemes.biz/testimonials/">Testimonials</a></li>*/
/* 	<li id="menu-item-3613" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3613"><a href="http://realhomes.inspirythemes.biz/typography/">Typography</a></li>*/
/* 	<li id="menu-item-3614" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3614"><a href="http://realhomes.inspirythemes.biz/columns/">Columns</a></li>*/
/* 	<li id="menu-item-3587" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3587"><a href="http://realhomes.inspirythemes.biz/idx/">IDX using dsIDXpress Plugin</a></li>*/
/* </ul>*/
/* </li>*/
/* <li id="menu-item-3578" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3578"><a href="#">Types</a>*/
/* <ul class="sub-menu">*/
/* 	<li id="menu-item-763" class="menu-item menu-item-type-taxonomy menu-item-object-property-type menu-item-763"><a href="http://realhomes.inspirythemes.biz/property-type/single-family-home/">Single Family Home</a></li>*/
/* 	<li id="menu-item-3590" class="menu-item menu-item-type-taxonomy menu-item-object-property-type menu-item-3590"><a href="http://realhomes.inspirythemes.biz/property-type/apartment-building/">Apartment Building</a></li>*/
/* 	<li id="menu-item-3589" class="menu-item menu-item-type-taxonomy menu-item-object-property-type menu-item-3589"><a href="http://realhomes.inspirythemes.biz/property-type/apartment/">Apartment</a></li>*/
/* 	<li id="menu-item-3591" class="menu-item menu-item-type-taxonomy menu-item-object-property-type menu-item-3591"><a href="http://realhomes.inspirythemes.biz/property-type/office/">Office</a></li>*/
/* 	<li id="menu-item-3592" class="menu-item menu-item-type-taxonomy menu-item-object-property-type menu-item-3592"><a href="http://realhomes.inspirythemes.biz/property-type/shop/">Shop</a></li>*/
/* 	<li id="menu-item-762" class="menu-item menu-item-type-taxonomy menu-item-object-property-type menu-item-762"><a href="http://realhomes.inspirythemes.biz/property-type/villa/">Villa</a></li>*/
/* 	<li id="menu-item-764" class="menu-item menu-item-type-taxonomy menu-item-object-property-type menu-item-764"><a href="http://realhomes.inspirythemes.biz/property-type/condominium/">Condominium</a></li>*/
/* </ul>*/
/* </li>*/
/* <li id="menu-item-3656" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3656"><a href="http://realhomes.inspirythemes.biz/contact-us/">Contact us</a></li>*/
/* </ul></div>                        </nav>*/
/*                         <!-- End Main Menu -->*/
/*                     </div>*/
/* */
/*                 </header>*/
/* */
/*             </div> <!-- End Header Container -->*/
/* */
/*         </div><!-- End Header -->*/
/* */
/*     <div class="page-head" style="background-repeat: no-repeat;background-position: center top;background-image: url('http://realhomes.inspirythemes.biz/wp-content/uploads/2015/08/banner.jpg'); background-size: cover;">*/
/*                     <div class="container">*/
/*                 <div class="wrap clearfix">*/
/*                     <h1 class="page-title"><span>Villa in Hialeah, Dade County</span></h1>*/
/*                             <div class="page-breadcrumbs">*/
/*         <nav class="property-breadcrumbs">*/
/*             <ul>*/
/*             <li><a href="http://realhomes.inspirythemes.biz/">Home</a><i class="breadcrumbs-separator fa fa-angle-right"></i></li><li><a href="http://realhomes.inspirythemes.biz/property-city/miami/">Miami</a><i class="breadcrumbs-separator fa fa-angle-right"></i></li><li><a href="http://realhomes.inspirythemes.biz/property-city/hialeah/">Hialeah</a></li>            </ul>*/
/*         </nav>*/
/*         </div>*/
/*                         </div>*/
/*             </div>*/
/*             </div><!-- End Page Head -->*/
/* */
/*     <!-- Content -->*/
/*     <div class="container contents detail">*/
/*         <div class="row">*/
/*             <div class="span9 main-wrap">*/
/* */
/*                 <!-- Main Content -->*/
/*                 <div class="main">*/
/* */
/*                     <div id="overview">*/
/*                             <div id="property-detail-flexslider" class="clearfix">*/
/*         <div class="flexslider">*/
/*             <ul class="slides">*/
/*                 <li data-thumb="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/kitchen-82x60.jpg"><a href="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/kitchen.jpg" class="swipebox" rel="gallery_real_homes"><img src="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/kitchen-770x386.jpg" alt="kitchen" /></a></li><li data-thumb="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/guest-room-82x60.jpg"><a href="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/guest-room.jpg" class="swipebox" rel="gallery_real_homes"><img src="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/guest-room-770x386.jpg" alt="guest-room" /></a></li><li data-thumb="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/drawing-room-82x60.jpg"><a href="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/drawing-room.jpg" class="swipebox" rel="gallery_real_homes"><img src="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/drawing-room-770x386.jpg" alt="drawing-room" /></a></li><li data-thumb="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/interior-one-82x60.jpg"><a href="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/interior-one.jpg" class="swipebox" rel="gallery_real_homes"><img src="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/interior-one-770x386.jpg" alt="interior one" /></a></li><li data-thumb="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/living-room-02-82x60.jpg"><a href="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/living-room-02.jpg" class="swipebox" rel="gallery_real_homes"><img src="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/living-room-02-770x386.jpg" alt="living room" /></a></li><li data-thumb="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/interior-two-82x60.jpg"><a href="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/interior-two.jpg" class="swipebox" rel="gallery_real_homes"><img src="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/interior-two-770x386.jpg" alt="interior two" /></a></li><li data-thumb="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/kids-room-82x60.jpg"><a href="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/kids-room.jpg" class="swipebox" rel="gallery_real_homes"><img src="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/kids-room-770x386.jpg" alt="kids room" /></a></li><li data-thumb="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/property-08-82x60.jpg"><a href="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/property-08.jpg" class="swipebox" rel="gallery_real_homes"><img src="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/property-08-770x386.jpg" alt="property 08" /></a></li>            </ul>*/
/*         </div>*/
/*     </div>*/
/*             <div id="property-featured-image" class="clearfix only-for-print">*/
/*             <img src="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/property-08.jpg" alt="Villa in Hialeah, Dade County" />        </div>*/
/*         <article class="property-item clearfix">*/
/*     <div class="wrap clearfix">*/
/*         <h4 class="title">*/
/*             Property ID : RH1002        </h4>*/
/*         <h5 class="price">*/
/*             <span class="status-label">*/
/*                 For Rent            </span>*/
/*             <span>*/
/*                 $7,500 Per Month<small> - Single Family Home</small>            </span>*/
/*         </h5>*/
/*     </div>*/
/* */
/*     <div class="property-meta clearfix">*/
/*         <span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">*/
/* <path class="path" d="M14 7.001H2.999C1.342 7 0 8.3 0 10v11c0 1.7 1.3 3 3 3H14c1.656 0 3-1.342 3-3V10 C17 8.3 15.7 7 14 7.001z M14.998 21c0 0.551-0.447 1-0.998 1.002H2.999C2.448 22 2 21.6 2 21V10 c0.001-0.551 0.449-0.999 1-0.999H14c0.551 0 1 0.4 1 0.999V21z"/>*/
/* <path class="path" d="M14.266 0.293c-0.395-0.391-1.034-0.391-1.429 0c-0.395 0.39-0.395 1 0 1.415L13.132 2H3.869l0.295-0.292 c0.395-0.391 0.395-1.025 0-1.415c-0.394-0.391-1.034-0.391-1.428 0L0 3l2.736 2.707c0.394 0.4 1 0.4 1.4 0 c0.395-0.391 0.395-1.023 0-1.414L3.869 4.001h9.263l-0.295 0.292c-0.395 0.392-0.395 1 0 1.414s1.034 0.4 1.4 0L17 3 L14.266 0.293z"/>*/
/* <path class="path" d="M18.293 9.734c-0.391 0.395-0.391 1 0 1.429s1.023 0.4 1.4 0L20 10.868v9.263l-0.292-0.295 c-0.392-0.395-1.024-0.395-1.415 0s-0.391 1 0 1.428L21 24l2.707-2.736c0.391-0.394 0.391-1.033 0-1.428s-1.023-0.395-1.414 0 l-0.292 0.295v-9.263l0.292 0.295c0.392 0.4 1 0.4 1.4 0s0.391-1.034 0-1.429L21 7L18.293 9.734z"/>*/
/* </svg>*/
/* 4800&nbsp;sq ft</span><span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">*/
/* <circle class="circle" cx="5" cy="8.3" r="2.2"/>*/
/* <path class="path" d="M0 22.999C0 23.6 0.4 24 1 24S2 23.6 2 22.999V18H2h20h0.001v4.999c0 0.6 0.4 1 1 1 C23.552 24 24 23.6 24 22.999V10C24 9.4 23.6 9 23 9C22.447 9 22 9.4 22 10v1H22h-0.999V10.5 C20.999 8 20 6 17.5 6H11C9.769 6.1 8.2 6.3 8 8v3H2H2V9C2 8.4 1.6 8 1 8S0 8.4 0 9V22.999z M10.021 8.2 C10.19 8.1 10.6 8 11 8h5.5c1.382 0 2.496-0.214 2.5 2.501v0.499h-9L10.021 8.174z M22 16H2v-2.999h20V16z"/>*/
/* </svg>*/
/* 4&nbsp;Bedrooms</span><span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">*/
/* <path class="path" d="M23.001 12h-1.513C21.805 11.6 22 11.1 22 10.5C22 9.1 20.9 8 19.5 8S17 9.1 17 10.5 c0 0.6 0.2 1.1 0.5 1.5H2.999c0-0.001 0-0.002 0-0.002V2.983V2.98c0.084-0.169-0.083-0.979 1-0.981h0.006 C4.008 2 4.3 2 4.5 2.104L4.292 2.292c-0.39 0.392-0.39 1 0 1.415c0.391 0.4 1 0.4 1.4 0l2-1.999 c0.39-0.391 0.39-1.025 0-1.415c-0.391-0.391-1.023-0.391-1.415 0L5.866 0.72C5.775 0.6 5.7 0.5 5.5 0.4 C4.776 0 4.1 0 4 0H3.984v0.001C1.195 0 1 2.7 1 2.98v0.019v0.032v8.967c0 0 0 0 0 0.002H0.999 C0.447 12 0 12.4 0 12.999S0.447 14 1 14H1v2.001c0.001 2.6 1.7 4.8 4 5.649V23c0 0.6 0.4 1 1 1s1-0.447 1-1v-1h10v1 c0 0.6 0.4 1 1 1s1-0.447 1-1v-1.102c2.745-0.533 3.996-3.222 4-5.897V14h0.001C23.554 14 24 13.6 24 13 S23.554 12 23 12z M21.001 16.001c-0.091 2.539-0.927 3.97-3.001 3.997H7c-2.208-0.004-3.996-1.79-4-3.997V14h15.173 c-0.379 0.484-0.813 0.934-1.174 1.003c-0.54 0.104-0.999 0.446-0.999 1c0 0.6 0.4 1 1 1 c2.159-0.188 3.188-2.006 3.639-2.999h0.363V16.001z"/>*/
/* <rect class="rect" x="6.6" y="4.1" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 15.6319 3.2336)" width="1" height="1.4"/>*/
/* <rect class="rect" x="9.4" y="2.4" transform="matrix(0.7066 0.7076 -0.7076 0.7066 4.9969 -6.342)" width="1.4" height="1"/>*/
/* <rect class="rect" x="9.4" y="6.4" transform="matrix(0.7071 0.7071 -0.7071 0.7071 7.8179 -5.167)" width="1.4" height="1"/>*/
/* <rect class="rect" x="12.4" y="4.4" transform="matrix(0.7069 0.7073 -0.7073 0.7069 7.2858 -7.8754)" width="1.4" height="1"/>*/
/* <rect class="rect" x="13.4" y="7.4" transform="matrix(-0.7064 -0.7078 0.7078 -0.7064 18.5823 23.4137)" width="1.4" height="1"/>*/
/* </svg>*/
/* 3&nbsp;Bathrooms</span><span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">*/
/* <path class="path" d="M23.958 0.885c-0.175-0.64-0.835-1.016-1.475-0.842l-11 3.001c-0.64 0.173-1.016 0.833-0.842 1.5 c0.175 0.6 0.8 1 1.5 0.842L16 4.299V6.2h-0.001H13c-2.867 0-4.892 1.792-5.664 2.891L5.93 11.2H5.024 c-0.588-0.029-2.517-0.02-3.851 1.221C0.405 13.1 0 14.1 0 15.201V18.2v2H2h2.02C4.126 22.3 5.9 24 8 24 c2.136 0 3.873-1.688 3.979-3.801H16V24h2V3.754l5.116-1.396C23.756 2.2 24.1 1.5 24 0.885z M8 22 c-1.104 0-2-0.896-2-2.001s0.896-2 2-2S10 18.9 10 20S9.105 22 8 22.001z M11.553 18.2C10.891 16.9 9.6 16 8 16 c-1.556 0-2.892 0.901-3.553 2.201H2v-2.999c0-0.599 0.218-1.019 0.537-1.315C3.398 13.1 5 13.2 5 13.2h2L9 10.2 c0 0 1.407-1.999 4-1.999h2.999H16v10H11.553z"/>*/
/* </svg>*/
/* 2&nbsp;Garages</span>    <span class="add-to-fav">*/
/*                     <form action="http://realhomes.inspirythemes.biz/wp-admin/admin-ajax.php" method="post" id="add-to-favorite-form">*/
/*                 <input type="hidden" name="property_id" value="79" />*/
/*                 <input type="hidden" name="action" value="add_to_favorite" />*/
/*             </form>*/
/*             <div id="fav_output">*/
/* 	            <i class="fa fa-star-o dim"></i>&nbsp;*/
/* 	            <span id="fav_target" class="dim"></span>*/
/*             </div>*/
/*             <a id="add-to-favorite" href="#add-to-favorite">*/
/* 	            <i class="fa fa-star-o"></i>&nbsp;Add to Favorites            </a>*/
/*                 </span>*/
/*             <!-- Print link -->*/
/*         <span class="printer-icon"><a href="javascript:window.print()"><i class="fa fa-print"></i>Print</a></span>*/
/*     </div>*/
/* */
/*     <div class="content clearfix">*/
/*         <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>*/
/* <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>*/
/* <h4 class="additional-title">Additional Details</h4><ul class="additional-details clearfix">        <li>*/
/*             <strong>AC:</strong>*/
/*             <span>Central</span>*/
/*         </li>*/
/*                 <li>*/
/*             <strong>ACRES:</strong>*/
/*             <span>0.14</span>*/
/*         </li>*/
/*                 <li>*/
/*             <strong>ACRES SOURCE:</strong>*/
/*             <span>Assessor</span>*/
/*         </li>*/
/*                 <li>*/
/*             <strong>AP #:</strong>*/
/*             <span>2639-017-020</span>*/
/*         </li>*/
/*                 <li>*/
/*             <strong>APPLIANCES:</strong>*/
/*             <span>Cooktop - Gas, Dishwasher, Freestanding Gas Range, Garbage Disposal</span>*/
/*         </li>*/
/*                 <li>*/
/*             <strong>BATHROOM DESCRIPTION:</strong>*/
/*             <span>Remodeled, Skylight(s), Tile Walls</span>*/
/*         </li>*/
/*                 <li>*/
/*             <strong>CROSS STREETS:</strong>*/
/*             <span>NORDOFF/ WAKEFIELD</span>*/
/*         </li>*/
/*                 <li>*/
/*             <strong>DISABILITY ACCESS:</strong>*/
/*             <span>Main Level Entry</span>*/
/*         </li>*/
/*                 <li>*/
/*             <strong>DOORS &amp; WINDOWS:</strong>*/
/*             <span>Double Pane Windows, Skylights</span>*/
/*         </li>*/
/*                 <li>*/
/*             <strong>FIREPLACE DESCRIPTION:</strong>*/
/*             <span>Brick</span>*/
/*         </li>*/
/*                 <li>*/
/*             <strong>FIREPLACE FUEL:</strong>*/
/*             <span>Uses Both Gas &amp; Wood</span>*/
/*         </li>*/
/*                 <li>*/
/*             <strong>FIREPLACE LOCATION:</strong>*/
/*             <span>Living Room</span>*/
/*         </li>*/
/*                 <li>*/
/*             <strong>FLOORS:</strong>*/
/*             <span>Carpet - Partial, Ceramic Tile</span>*/
/*         </li>*/
/*                 <li>*/
/*             <strong>PLUMBING:</strong>*/
/*             <span>Full Copper Plumbing</span>*/
/*         </li>*/
/*         </ul>        <div class="common-note">*/
/*             <h4 class="common-note-heading">Common Note</h4><p>A common note across all properties to describe some policies or terms that are applicable on all the properties. This common note can be disabled from theme options.</p>        </div>*/
/*             </div>*/
/* */
/* */
/*             <div class="features">*/
/*             <h4 class="title">Features</h4>            <ul class="arrow-bullet-list clearfix">*/
/*             <li><a href="http://realhomes.inspirythemes.biz/property-feature/2-stories/">2 Stories</a></li><li><a href="http://realhomes.inspirythemes.biz/property-feature/central-heating/">Central Heating</a></li><li><a href="http://realhomes.inspirythemes.biz/property-feature/dual-sinks/">Dual Sinks</a></li><li><a href="http://realhomes.inspirythemes.biz/property-feature/electric-range/">Electric Range</a></li><li><a href="http://realhomes.inspirythemes.biz/property-feature/fire-place/">Fire Place</a></li><li><a href="http://realhomes.inspirythemes.biz/property-feature/laundry-room/">Laundry Room</a></li><li><a href="http://realhomes.inspirythemes.biz/property-feature/lawn/">Lawn</a></li><li><a href="http://realhomes.inspirythemes.biz/property-feature/marble-floors/">Marble Floors</a></li><li><a href="http://realhomes.inspirythemes.biz/property-feature/swimming-pool/">Swimming Pool</a></li>            </ul>*/
/*         </div>*/
/*         </article>        <div class="floor-plans">*/
/* */
/* 			<h3 class="floor-plans-label">Floor Plans</h3>*/
/* */
/* 			<div class="floor-plans-accordions">*/
/*                 					<div class="floor-plan">*/
/* 						<div class="floor-plan-title clearfix">*/
/* 							<i class="fa fa-plus"></i>*/
/* 							<h3>Ground Floor</h3>*/
/* 							<div class="floor-plan-meta">*/
/* 								<div>2500 sq ft</div><div>1 Bedroom</div><div>1 Bathroom</div><div class="floor-price"><span class="floor-price-value">$4,000</span> Per Month</div>							</div>*/
/* 						</div>*/
/* 						<div class="floor-plan-content">*/
/* */
/* 		                    	                            <div class="floor-plan-desc">*/
/* 		                            <p>This is a sample floor plan to demonstrate how you can display your floor plans using this theme. </p>*/
/* <p>Curabitur blandit tempus porttitor. Sed posuere consectetur est at lobortis. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Etiam porta sem malesuada magna mollis euismod. Nulla vitae elit libero, a pharetra augue. Vestibulum id ligula porta felis euismod semper.</p>*/
/* 	                            </div>*/
/* 		                    */
/* 		                    	                        <div class="floor-plan-map">*/
/* 	                            <a href="http://realhomes.inspirythemes.biz/wp-content/uploads/2015/08/inspiry-0-ground-floor.jpg" class="swipebox" rel="gallery_floor_plans">*/
/* 	                                <img src="http://realhomes.inspirythemes.biz/wp-content/uploads/2015/08/inspiry-0-ground-floor.jpg" >*/
/* 	                            </a>*/
/* 	                        </div>*/
/* 		                    						</div>*/
/* 					</div>*/
/* 	                					<div class="floor-plan">*/
/* 						<div class="floor-plan-title clearfix">*/
/* 							<i class="fa fa-plus"></i>*/
/* 							<h3>First Floor</h3>*/
/* 							<div class="floor-plan-meta">*/
/* 								<div>2300 sq ft</div><div>3 Bedrooms</div><div>2 Bathrooms</div><div class="floor-price"><span class="floor-price-value">$3,500</span> Per Month</div>							</div>*/
/* 						</div>*/
/* 						<div class="floor-plan-content">*/
/* */
/* 		                    	                            <div class="floor-plan-desc">*/
/* 		                            <p>This is a sample floor plan to demonstrate how you can display your floor plans using this theme.</p>*/
/* <p>Curabitur blandit tempus porttitor. Sed posuere consectetur est at lobortis. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Etiam porta sem malesuada magna mollis euismod. Nulla vitae elit libero, a pharetra augue. Vestibulum id ligula porta felis euismod semper.</p>*/
/* 	                            </div>*/
/* 		                    */
/* 		                    	                        <div class="floor-plan-map">*/
/* 	                            <a href="http://realhomes.inspirythemes.biz/wp-content/uploads/2015/08/inspiry-0-first-floor.jpg" class="swipebox" rel="gallery_floor_plans">*/
/* 	                                <img src="http://realhomes.inspirythemes.biz/wp-content/uploads/2015/08/inspiry-0-first-floor.jpg" >*/
/* 	                            </a>*/
/* 	                        </div>*/
/* 		                    						</div>*/
/* 					</div>*/
/* 	                			</div>*/
/*         </div>*/
/*                */
/*             <div class="attachments-wrap clearfix">*/
/*             <span class="attachments-label">Property Attachments</span>            <div class="attachments-inner clearfix">*/
/*                 <ul class="attachments-list clearfix"><li class="jpg"><a target="_blank" href="http://realhomes.inspirythemes.biz/wp-content/uploads/2015/08/inspiry-0-ground-floor.jpg"><i class="fa fa-file-image-o"></i>Ground Floor</a></li><li class="jpg"><a target="_blank" href="http://realhomes.inspirythemes.biz/wp-content/uploads/2015/08/inspiry-0-first-floor.jpg"><i class="fa fa-file-image-o"></i>First Floor</a></li><li class="pdf"><a target="_blank" href="http://realhomes.inspirythemes.biz/wp-content/uploads/2014/06/demo.pdf"><i class="fa fa-file-pdf-o"></i>Demo PDF</a></li></ul>            </div>*/
/*         </div>*/
/*         	<div class="agent-detail clearfix">*/
/* */
/* 		<div class="left-box">*/
/* 			<h3>Agent Julia Robert</h3>					<figure>*/
/* 						<a href="http://realhomes.inspirythemes.biz/agent/julia-robert/">*/
/* 							<img width="210" height="210" src="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/agent.jpg" class="attachment-agent-image size-agent-image wp-post-image" alt="Julia Robert" srcset="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/agent.jpg 210w, http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/agent-150x150.jpg 150w" sizes="(max-width: 210px) 100vw, 210px" />						</a>*/
/* 					</figure>*/
/* 								<ul class="contacts-list">*/
/* 									<li class="office">*/
/* 						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">*/
/* <path class="path" d="M5.597 1.999C6.4 2 8 2 8.1 5.51C8.017 6.1 7.8 6.3 7.5 6.658C7.12 7.1 6.4 7.9 6.4 9.2 c0 1.5 1 3.1 3.1 5.198c1.143 1.2 3.3 3.1 4.8 3.123c1.222 0 1.856-0.836 2.128-1.192 c0.18-0.236 0.284-0.373 0.862-0.468h0.055c3.91 0 4.6 0.8 4.6 2.476c0 0.358-0.151 1.891-0.893 2.8 c-0.297 0.382-1.161 0.789-2.695 0.789c-3.012 0-7.658-1.598-11.249-5.162C2.085 11.7 2 4.7 2 4.6 C2.021 2.2 4.8 2 5.6 2 M5.598-0.001c-0.385 0-5.578 0.084-5.578 4.624c0 0-0.041 7.8 5.8 13.6 c3.825 3.8 8.9 5.7 12.7 5.747c1.914 0 3.46-0.515 4.273-1.56c1.314-1.688 1.314-4.07 1.314-4.07 c0-4.618-4.734-4.47-6.801-4.476c-2.345 0.324-2.198 1.66-2.843 1.66c-0.187 0-1.32-0.393-3.409-2.523 c-2.106-2.085-2.527-3.234-2.527-3.797c0-1.265 1.396-1.328 1.662-3.602c-0.004-1.028-0.011-5.563-4.482-5.632 C5.621-0.001 5.613-0.001 5.598-0.001L5.598-0.001z"/>*/
/* </svg>*/
/* Office : 041-456-3692					</li>*/
/* 										<li class="mobile">*/
/* 						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">*/
/* <path class="path" fill-rule="evenodd" clip-rule="evenodd" d="M18 24c1.657 0 2.999-1.341 3-2.999V2.999C20.999 1.3 19.7 0 18 0H6.001 C4.343 0 3 1.3 3 2.999v18.002C3.001 22.7 4.3 24 6 24H18z M6.001 22.001c-0.553 0-1-0.448-1.001-1V2.999 C5.001 2.4 5.4 2 6 2h9C15.552 2 16 2.4 16 2.999v18.002c-0.002 0.552-0.448 1-0.999 1H6.001z M19 21 c-0.001 0.552-0.447 1-1 1h-0.184c0.111-0.314 0.184-0.647 0.184-1V2.999C18 2.6 17.9 2.3 17.8 2H18 c0.553 0 1 0.4 1 0.999V21.001z"/>*/
/* <circle class="circle" fill-rule="evenodd" clip-rule="evenodd" cx="10.5" cy="19.5" r="1.5"/>*/
/* <path class="path" fill-rule="evenodd" clip-rule="evenodd" d="M12 2.999H8.999C8.447 3 8 3.4 8 4c0 0.6 0.4 1 1 1H12 c0.552 0 1-0.448 1-1C13 3.4 12.6 3 12 2.999z"/>*/
/* </svg>*/
/* Mobile : 0200-123-4567					</li>*/
/* 										<li class="fax">*/
/* 						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">*/
/* <g>*/
/* 	<path class="path" d="M14.415 0H6v10h1h11V3.585L14.415 0z M16 8H8V2h5.586L16 4.413V8z"/>*/
/* 	<path class="path" d="M15.001 14.001H8.999C7.343 14 6 15.3 6 17v7h1c0.469 0 0.9 0 2 0h6.002c1.104 0 1.6 0 2 0h1v-7 C18 15.3 16.7 14 15 14.001z M16 22c-0.264 0-0.586 0-0.999 0H8.999c-0.417 0-0.737 0-1 0v-5 c0.001-0.552 0.449-0.998 1-1h6.002c0.55 0 1 0.4 1 1V22z"/>*/
/* </g>*/
/* <g>*/
/* 	<path class="path" d="M7 18H3.001c-0.917-0.029-0.844-0.142-0.855-0.146C2.067 17.8 2 17.1 2 16v-6c0.029-1.481 0.922-1.894 3.001-2H7V6 H5.001C2.66 6 0 7.1 0 10v6c0.02 1.111-0.047 1.9 0.4 2.803c0.499 0.9 1.6 1.2 2.6 1.197H7V18z"/>*/
/* 	<path class="path" d="M19.001 6H17v2h1.999c2.078 0.1 3 0.5 3 2v6c0.02 1.098-0.067 1.78-0.146 1.9 c-0.011 0 0.1 0.116-0.855 0.146H17v2h3.999c1.046 0 2.117-0.255 2.616-1.197C24.048 17.9 24 17.1 24 16v-6 C23.995 7.1 21.3 6 19 6z"/>*/
/* </g>*/
/* <polygon class="path" fill-rule="evenodd" clip-rule="evenodd" points="18,4.4 13.6,4.4 13.6,0 13.6,0 18,4.4 18,4.4"/>*/
/* <path class="path" d="M20.499 17c-0.275 0-0.5-0.224-0.5-0.5v-0.999c0-0.276 0.225-0.5 0.5-0.5c0.277 0 0.5 0.2 0.5 0.5V16.5 C21.001 16.8 20.8 17 20.5 17L20.499 17z M4 11C3.292 11 3 10.7 3 10C3 9.2 3.3 9 4 8.999c0.77 0 1 0.2 1 1 C5 10.7 4.8 11 4 11L4 11z"/>*/
/* </svg>*/
/* Fax : 041-789-4561					</li>*/
/* 								</ul>*/
/* 			<p>Lorem agent info ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna&hellip;<br/><a class="real-btn" href="http://realhomes.inspirythemes.biz/agent/julia-robert/">Know More</a></p>*/
/* 		</div>*/
/* */
/* 					<div class="contact-form">*/
/* 				<h3>Contact</h3>*/
/* 				<form id="agent-form-id110" class="agent-form contact-form-small" method="post" action="http://realhomes.inspirythemes.biz/wp-admin/admin-ajax.php">*/
/* 					<input type="text" name="name" placeholder="Name" class="required" title="* Please provide your name">*/
/* 					<input type="text" name="email" placeholder="Email" class="email required" title="* Please provide valid email address">*/
/* 					<textarea name="message" class="required" placeholder="Message" title="* Please provide your message"></textarea>*/
/* 					    <div id="recaptcha_widget" style="display:none" class="recaptcha_widget">*/
/*         <div id="recaptcha_image"></div>*/
/*         <div class="recaptcha_only_if_incorrect_sol" style="color:red">Incorrect. Please try again.</div>*/
/* */
/*         <div class="recaptcha_input">*/
/*             <label class="recaptcha_only_if_image" for="recaptcha_response_field">Enter the words above:</label>*/
/*             <label class="recaptcha_only_if_audio" for="recaptcha_response_field">Enter the numbers you hear:</label>*/
/* */
/*             <input type="text" id="recaptcha_response_field" name="recaptcha_response_field">*/
/*         </div>*/
/* */
/*         <ul class="recaptcha_options">*/
/*             <li>*/
/*                 <a href="javascript:Recaptcha.reload()">*/
/*                     <i class="icon-refresh"></i>*/
/*                     <span class="captcha_hide">Get another CAPTCHA</span>*/
/*                 </a>*/
/*             </li>*/
/*             <li class="recaptcha_only_if_image">*/
/*                 <a href="javascript:Recaptcha.switch_type('audio')">*/
/*                     <i class="icon-volume-up"></i><span class="captcha_hide"> Get an audio CAPTCHA</span>*/
/*                 </a>*/
/*             </li>*/
/*             <li class="recaptcha_only_if_audio">*/
/*                 <a href="javascript:Recaptcha.switch_type('image')">*/
/*                     <i class="icon-picture"></i><span class="captcha_hide"> Get an image CAPTCHA</span>*/
/*                 </a>*/
/*             </li>*/
/*             <li>*/
/*                 <a href="javascript:Recaptcha.showhelp()">*/
/*                     <i class="icon-question-sign"></i><span class="captcha_hide"> Help</span>*/
/*                 </a>*/
/*             </li>*/
/*         </ul>*/
/*     </div>*/
/* */
/*     <script type="text/javascript" src="//www.google.com/recaptcha/api/challenge?k=6Ldf7fQSAAAAAD52MkF0ZMwi09JoycTSGBLe2RW5"></script>*/
/*     <noscript>*/
/*         <iframe src="//www.google.com/recaptcha/api/noscript?k=6Ldf7fQSAAAAAD52MkF0ZMwi09JoycTSGBLe2RW5" height="300" width="220" frameborder="0"></iframe><br>*/
/*         <textarea name="recaptcha_challenge_field"></textarea>*/
/*         <input type="hidden" name="recaptcha_response_field" value="manual_challenge">*/
/*     </noscript>*/
/*     					<input type="hidden" name="nonce" value="7c7cc1e8b9"/>*/
/* 					<input type="hidden" name="target" value="r&#111;&#98;&#111;t&#64;in&#115;p&#105;ry&#116;h&#101;m&#101;&#115;.&#98;&#105;&#122;">*/
/* 					<input type="hidden" name="action" value="send_message_to_agent"/>*/
/* 					<input type="hidden" name="property_title" value="Villa in Hialeah, Dade County"/>*/
/* 					<input type="hidden" name="property_permalink" value="http://realhomes.inspirythemes.biz/property/villa-in-hialeah-dade-county/"/>*/
/* 					<input type="submit" value="Send Message" name="submit" class="submit-button real-btn">*/
/* 					<img src="http://realhomes.inspirythemes.biz/wp-content/themes/realhomes/images/ajax-loader.gif" class="ajax-loader" alt="Loading...">*/
/* 					<div class="clearfix form-separator"></div>*/
/* 					<div class="error-container"></div>*/
/* 					<div class="message-container"></div>*/
/* 				</form>*/
/* 			</div>*/
/* 			<script type="text/javascript">*/
/* 				(function($){*/
/* 					"use strict";*/
/* */
/* 					if ( jQuery().validate && jQuery().ajaxSubmit ) {*/
/* */
/* 						var agentForm = $('#agent-form-id110');*/
/* 						var submitButton = agentForm.find( '.submit-button' ),*/
/* 							ajaxLoader = agentForm.find( '.ajax-loader' ),*/
/* 							messageContainer = agentForm.find( '.message-container' ),*/
/* 							errorContainer = agentForm.find( ".error-container" );*/
/* */
/* 						// Property detail page form*/
/* 						agentForm.validate( {*/
/* 							errorLabelContainer: errorContainer,*/
/* 							submitHandler : function( form ) {*/
/* 								$(form).ajaxSubmit( {*/
/* 									beforeSubmit: function(){*/
/* 										submitButton.attr('disabled','disabled');*/
/* 										ajaxLoader.fadeIn('fast');*/
/* 										messageContainer.fadeOut('fast');*/
/* 										errorContainer.fadeOut('fast');*/
/* 									},*/
/* 									success: function( ajax_response, statusText, xhr, $form) {*/
/* 										var response = $.parseJSON ( ajax_response );*/
/* 										ajaxLoader.fadeOut('fast');*/
/* 										submitButton.removeAttr('disabled');*/
/* 										if( response.success ) {*/
/* 											$form.resetForm();*/
/* 											messageContainer.html( response.message ).fadeIn('fast');*/
/* 										} else {*/
/* 											errorContainer.html( response.message ).fadeIn('fast');*/
/* 										}*/
/* 									}*/
/* 								} );*/
/* 							}*/
/* 						} );*/
/* */
/* 					}*/
/* */
/* 				})(jQuery);*/
/* 			</script>*/
/* 				</div>*/
/* 	                    </div>*/
/* */
/*                 </div><!-- End Main Content -->*/
/* */
/*                             <section class="listing-layout property-grid">*/
/*                 <div class="list-container clearfix">*/
/*                     <h3>Similar Properties</h3><article class="property-item clearfix">*/
/* */
/*     <figure>*/
/*         <a href="http://realhomes.inspirythemes.biz/property/apartment-building-having-35-apartments/15-apartments-of-type-b/">*/
/*             <img width="246" height="162" src="http://realhomes.inspirythemes.biz/wp-content/uploads/2014/06/apartment-b-246x162.jpg" class="attachment-grid-view-image size-grid-view-image wp-post-image" alt="Apartment interior" srcset="http://realhomes.inspirythemes.biz/wp-content/uploads/2014/06/apartment-b-246x162.jpg 246w, http://realhomes.inspirythemes.biz/wp-content/uploads/2014/06/apartment-b-150x100.jpg 150w" sizes="(max-width: 246px) 100vw, 246px" />        </a>*/
/* */
/*         <figcaption class="for-rent">For Rent</figcaption>*/
/*     </figure>*/
/* */
/* */
/*     <h4><a href="http://realhomes.inspirythemes.biz/property/apartment-building-having-35-apartments/15-apartments-of-type-b/">15 Apartments of Type B</a></h4>*/
/*     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed&hellip; <a class="more-details" href="http://realhomes.inspirythemes.biz/property/apartment-building-having-35-apartments/15-apartments-of-type-b/">More Details <i class="fa fa-caret-right"></i></a></p>*/
/*     <span>$2,200 Per Month</span></article><article class="property-item clearfix">*/
/* */
/*     <figure>*/
/*         <a href="http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/">*/
/*             <img width="246" height="162" src="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/property-10-246x162.jpg" class="attachment-grid-view-image size-grid-view-image wp-post-image" alt="Demo JPG" />        </a>*/
/* */
/*         <figcaption class="for-rent">For Rent</figcaption>*/
/*     </figure>*/
/* */
/* */
/*     <h4><a href="http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/">15421 Southwest 39th Terrace</a></h4>*/
/*     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed&hellip; <a class="more-details" href="http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/">More Details <i class="fa fa-caret-right"></i></a></p>*/
/*     <span>$3,850 Per Month</span></article><article class="property-item clearfix">*/
/* */
/*     <figure>*/
/*         <a href="http://realhomes.inspirythemes.biz/property/60-merrick-way-miami/">*/
/*             <img width="246" height="162" src="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/property-07-246x162.jpg" class="attachment-grid-view-image size-grid-view-image wp-post-image" alt="property-07" />        </a>*/
/* */
/*         <figcaption class="for-sale">For Sale</figcaption>*/
/*     </figure>*/
/* */
/* */
/*     <h4><a href="http://realhomes.inspirythemes.biz/property/60-merrick-way-miami/">60 Merrick Way, Miami</a></h4>*/
/*     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed&hellip; <a class="more-details" href="http://realhomes.inspirythemes.biz/property/60-merrick-way-miami/">More Details <i class="fa fa-caret-right"></i></a></p>*/
/*     <span>$440,000 </span></article>                </div>*/
/*             </section>*/
/*             */
/*             </div> <!-- End span9 -->*/
/* */
/*             <div class="span3 sidebar-wrap">*/
/* */
/*     <!-- Sidebar -->*/
/*     <aside class="sidebar">*/
/*         <section class="widget advance-search"><h4 class="title search-heading">Find Your Home<i class="fa fa-search"></i></h4>	<div class="as-form-wrap">*/
/* 	    <form class="advance-search-form clearfix" action="http://realhomes.inspirythemes.biz/property-search/" method="get">*/
/* 	    <div class="option-bar large">*/
/* 	<label for="keyword-txt">*/
/* 		Keyword	</label>*/
/* 	<input type="text" name="keyword" id="keyword-txt"*/
/* 	       value=""*/
/* 	       placeholder="Any"/>*/
/* </div><div class="option-bar large">*/
/* 	<label for="property-id-txt">*/
/* 		Property ID	</label>*/
/* 	<input type="text" name="property-id" id="property-id-txt"*/
/* 	       value=""*/
/* 	       placeholder="Any" />*/
/* </div>	<div class="option-bar large">*/
/* 		<label for="location">*/
/* 			Location		</label>*/
/*         <span class="selectwrap">*/
/*             <select name="location" id="location" class="search-select"></select>*/
/*         </span>*/
/* 	</div>*/
/* 	<div class="option-bar large">*/
/* 	<label for="select-status">*/
/* 		Property Status	</label>*/
/*     <span class="selectwrap">*/
/*         <select name="status" id="select-status" class="search-select">*/
/*             <option value="any" selected="selected">Any</option><option value="for-rent">For Rent</option><option value="for-sale">For Sale</option>        </select>*/
/*     </span>*/
/* </div><div class="option-bar large">*/
/* 	<label for="select-property-type">*/
/* 		Property Type	</label>*/
/*     <span class="selectwrap">*/
/*         <select name="type" id="select-property-type" class="search-select">*/
/* 	        <option value="any" selected="selected">Any</option><option value="commercial"> Commercial</option><option value="office">-  Office</option><option value="shop">-  Shop</option><option value="residential"> Residential</option><option value="apartment">-  Apartment</option><option value="apartment-building">-  Apartment Building</option><option value="condominium">-  Condominium</option><option value="single-family-home">-  Single Family Home</option><option value="villa">-  Villa</option>        </select>*/
/*     </span>*/
/* </div><div class="option-bar small">*/
/* 	<label for="select-bedrooms">*/
/* 		Min Beds	</label>*/
/*     <span class="selectwrap">*/
/*         <select name="bedrooms" id="select-bedrooms" class="search-select">*/
/*             <option value="any" selected="selected">Any</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option>        </select>*/
/*     </span>*/
/* </div><div class="option-bar small">*/
/* 	<label for="select-bathrooms">*/
/* 		Min Baths	</label>*/
/*     <span class="selectwrap">*/
/*         <select name="bathrooms" id="select-bathrooms" class="search-select">*/
/*             <option value="any" selected="selected">Any</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option>        </select>*/
/*     </span>*/
/* </div><div class="option-bar small price-for-others">*/
/* 	<label for="select-min-price">*/
/* 		Min Price	</label>*/
/*     <span class="selectwrap">*/
/*         <select name="min-price" id="select-min-price" class="search-select">*/
/*             <option value="any" selected="selected">Any</option><option value="1000">$1,000</option><option value="5000">$5,000</option><option value="10000">$10,000</option><option value="50000">$50,000</option><option value="100000">$100,000</option><option value="200000">$200,000</option><option value="300000">$300,000</option><option value="400000">$400,000</option><option value="500000">$500,000</option><option value="600000">$600,000</option><option value="700000">$700,000</option><option value="800000">$800,000</option><option value="900000">$900,000</option><option value="1000000">$1,000,000</option><option value="1500000">$1,500,000</option><option value="2000000">$2,000,000</option><option value="2500000">$2,500,000</option><option value="5000000">$5,000,000</option>        </select>*/
/*     </span>*/
/* </div>*/
/* */
/* <div class="option-bar small price-for-others">*/
/* 	<label for="select-max-price">*/
/* 		Max Price	</label>*/
/*     <span class="selectwrap">*/
/*         <select name="max-price" id="select-max-price" class="search-select">*/
/*             <option value="any" selected="selected">Any</option><option value="5000">$5,000</option><option value="10000">$10,000</option><option value="50000">$50,000</option><option value="100000">$100,000</option><option value="200000">$200,000</option><option value="300000">$300,000</option><option value="400000">$400,000</option><option value="500000">$500,000</option><option value="600000">$600,000</option><option value="700000">$700,000</option><option value="800000">$800,000</option><option value="900000">$900,000</option><option value="1000000">$1,000,000</option><option value="1500000">$1,500,000</option><option value="2000000">$2,000,000</option><option value="2500000">$2,500,000</option><option value="5000000">$5,000,000</option><option value="10000000">$10,000,000</option>        </select>*/
/*     </span>*/
/* </div>*/
/* */
/* <div class="option-bar small price-for-rent hide-fields">*/
/* 	<label for="select-min-price-for-rent">*/
/* 		Min Price	</label>*/
/*     <span class="selectwrap">*/
/*         <select name="min-price" id="select-min-price-for-rent" class="search-select" disabled="disabled">*/
/*             <option value="any" selected="selected">Any</option><option value="500">$500</option><option value="1000">$1,000</option><option value="2000">$2,000</option><option value="3000">$3,000</option><option value="4000">$4,000</option><option value="5000">$5,000</option><option value="7500">$7,500</option><option value="10000">$10,000</option><option value="15000">$15,000</option><option value="20000">$20,000</option><option value="25000">$25,000</option><option value="30000">$30,000</option><option value="40000">$40,000</option><option value="50000">$50,000</option><option value="75000">$75,000</option><option value="100000">$100,000</option>        </select>*/
/*     </span>*/
/* </div>*/
/* */
/* <div class="option-bar small price-for-rent hide-fields">*/
/* 	<label for="select-max-price-for-rent">*/
/* 		Max Price	</label>*/
/*     <span class="selectwrap">*/
/*         <select name="max-price" id="select-max-price-for-rent" class="search-select" disabled="disabled">*/
/*             <option value="any" selected="selected">Any</option><option value="1000">$1,000</option><option value="2000">$2,000</option><option value="3000">$3,000</option><option value="4000">$4,000</option><option value="5000">$5,000</option><option value="7500">$7,500</option><option value="10000">$10,000</option><option value="15000">$15,000</option><option value="20000">$20,000</option><option value="25000">$25,000</option><option value="30000">$30,000</option><option value="40000">$40,000</option><option value="50000">$50,000</option><option value="75000">$75,000</option><option value="100000">$100,000</option><option value="150000">$150,000</option>        </select>*/
/*     </span>*/
/* </div><div class="option-bar small">*/
/* 	<label for="min-area">*/
/* 		Min Area		<span>(sq ft)</span>*/
/* 	</label>*/
/* 	<input type="text" name="min-area" id="min-area" pattern="[0-9]+"*/
/* 	       value=""*/
/* 	       placeholder="Any"*/
/* 	       title="Only provide digits!" />*/
/* </div>*/
/* */
/* <div class="option-bar small">*/
/* 	<label for="max-area">*/
/* 		Max Area		<span>(sq ft)</span>*/
/* 	</label>*/
/* 	<input type="text" name="max-area" id="max-area" pattern="[0-9]+"*/
/* 	       value=""*/
/* 	       placeholder="Any"*/
/* 	       title="Only provide digits!" />*/
/* </div><div class="option-bar">*/
/* 		<input type="submit" value="Search" class="real-btn btn">*/
/* </div>	<div class="clearfix"></div>*/
/* */
/* 	<div class="more-option-trigger">*/
/* 		<a href="#">*/
/* 			<i class="fa fa-plus-square-o"></i>*/
/* 			Looking for certain features		</a>*/
/* 	</div>*/
/* */
/* 	<div class="more-options-wrapper clearfix collapsed">*/
/* 					<div class="option-bar">*/
/* 				<input type="checkbox"*/
/* 				       id="feature-2-stories"*/
/* 				       name="features[]"*/
/* 				       value="2-stories"*/
/* 					 />*/
/* 				<label for="feature-2-stories">2 Stories <small>(6)</small></label>*/
/* 			</div>*/
/* 						<div class="option-bar">*/
/* 				<input type="checkbox"*/
/* 				       id="feature-26-ceilings"*/
/* 				       name="features[]"*/
/* 				       value="26-ceilings"*/
/* 					 />*/
/* 				<label for="feature-26-ceilings">26' Ceilings <small>(1)</small></label>*/
/* 			</div>*/
/* 						<div class="option-bar">*/
/* 				<input type="checkbox"*/
/* 				       id="feature-central-heating"*/
/* 				       name="features[]"*/
/* 				       value="central-heating"*/
/* 					 />*/
/* 				<label for="feature-central-heating">Central Heating <small>(7)</small></label>*/
/* 			</div>*/
/* 						<div class="option-bar">*/
/* 				<input type="checkbox"*/
/* 				       id="feature-dual-sinks"*/
/* 				       name="features[]"*/
/* 				       value="dual-sinks"*/
/* 					 />*/
/* 				<label for="feature-dual-sinks">Dual Sinks <small>(7)</small></label>*/
/* 			</div>*/
/* 						<div class="option-bar">*/
/* 				<input type="checkbox"*/
/* 				       id="feature-electric-range"*/
/* 				       name="features[]"*/
/* 				       value="electric-range"*/
/* 					 />*/
/* 				<label for="feature-electric-range">Electric Range <small>(10)</small></label>*/
/* 			</div>*/
/* 						<div class="option-bar">*/
/* 				<input type="checkbox"*/
/* 				       id="feature-emergency-exit"*/
/* 				       name="features[]"*/
/* 				       value="emergency-exit"*/
/* 					 />*/
/* 				<label for="feature-emergency-exit">Emergency Exit <small>(7)</small></label>*/
/* 			</div>*/
/* 						<div class="option-bar">*/
/* 				<input type="checkbox"*/
/* 				       id="feature-fire-alarm"*/
/* 				       name="features[]"*/
/* 				       value="fire-alarm"*/
/* 					 />*/
/* 				<label for="feature-fire-alarm">Fire Alarm <small>(5)</small></label>*/
/* 			</div>*/
/* 						<div class="option-bar">*/
/* 				<input type="checkbox"*/
/* 				       id="feature-fire-place"*/
/* 				       name="features[]"*/
/* 				       value="fire-place"*/
/* 					 />*/
/* 				<label for="feature-fire-place">Fire Place <small>(9)</small></label>*/
/* 			</div>*/
/* 						<div class="option-bar">*/
/* 				<input type="checkbox"*/
/* 				       id="feature-home-theater"*/
/* 				       name="features[]"*/
/* 				       value="home-theater"*/
/* 					 />*/
/* 				<label for="feature-home-theater">Home Theater <small>(3)</small></label>*/
/* 			</div>*/
/* 						<div class="option-bar">*/
/* 				<input type="checkbox"*/
/* 				       id="feature-hurricane-shutters"*/
/* 				       name="features[]"*/
/* 				       value="hurricane-shutters"*/
/* 					 />*/
/* 				<label for="feature-hurricane-shutters">Hurricane Shutters <small>(1)</small></label>*/
/* 			</div>*/
/* 						<div class="option-bar">*/
/* 				<input type="checkbox"*/
/* 				       id="feature-laundry-room"*/
/* 				       name="features[]"*/
/* 				       value="laundry-room"*/
/* 					 />*/
/* 				<label for="feature-laundry-room">Laundry Room <small>(6)</small></label>*/
/* 			</div>*/
/* 						<div class="option-bar">*/
/* 				<input type="checkbox"*/
/* 				       id="feature-lawn"*/
/* 				       name="features[]"*/
/* 				       value="lawn"*/
/* 					 />*/
/* 				<label for="feature-lawn">Lawn <small>(6)</small></label>*/
/* 			</div>*/
/* 						<div class="option-bar">*/
/* 				<input type="checkbox"*/
/* 				       id="feature-marble-floors"*/
/* 				       name="features[]"*/
/* 				       value="marble-floors"*/
/* 					 />*/
/* 				<label for="feature-marble-floors">Marble Floors <small>(8)</small></label>*/
/* 			</div>*/
/* 						<div class="option-bar">*/
/* 				<input type="checkbox"*/
/* 				       id="feature-next-to-busway"*/
/* 				       name="features[]"*/
/* 				       value="next-to-busway"*/
/* 					 />*/
/* 				<label for="feature-next-to-busway">NEXT To Busway <small>(2)</small></label>*/
/* 			</div>*/
/* 						<div class="option-bar">*/
/* 				<input type="checkbox"*/
/* 				       id="feature-swimming-pool"*/
/* 				       name="features[]"*/
/* 				       value="swimming-pool"*/
/* 					 />*/
/* 				<label for="feature-swimming-pool">Swimming Pool <small>(6)</small></label>*/
/* 			</div>*/
/* 						<div class="option-bar">*/
/* 				<input type="checkbox"*/
/* 				       id="feature-wifi"*/
/* 				       name="features[]"*/
/* 				       value="wifi"*/
/* 					 />*/
/* 				<label for="feature-wifi">Wifi <small>(6)</small></label>*/
/* 			</div>*/
/* 				</div>*/
/* 		    </form>*/
/* 	</div>*/
/* 	</section><section id="featured_properties_widget-11" class="widget clearfix Featured_Properties_Widget"><h3 class="title">Featured Properties</h3>            <ul class="featured-properties">*/
/*                                     <li>*/
/* */
/*                         <figure>*/
/*                             <a href="http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/">*/
/*                             <img width="246" height="162" src="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/property-10-246x162.jpg" class="attachment-grid-view-image size-grid-view-image wp-post-image" alt="Demo JPG" />                            </a>*/
/*                         </figure>*/
/* */
/*                         <h4><a href="http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/">15421 Southwest 39th Terrace</a></h4>*/
/*                         <p>Lorem ipsum dolor sit amet, consectetuer adipiscing&hellip; <a href="http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/">Read More</a></p>*/
/*                         <span class="price">$3,850 Per Month</span>                    </li>*/
/*                                         <li>*/
/* */
/*                         <figure>*/
/*                             <a href="http://realhomes.inspirythemes.biz/property/60-merrick-way-miami/">*/
/*                             <img width="246" height="162" src="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/property-07-246x162.jpg" class="attachment-grid-view-image size-grid-view-image wp-post-image" alt="property-07" />                            </a>*/
/*                         </figure>*/
/* */
/*                         <h4><a href="http://realhomes.inspirythemes.biz/property/60-merrick-way-miami/">60 Merrick Way, Miami</a></h4>*/
/*                         <p>Lorem ipsum dolor sit amet, consectetuer adipiscing&hellip; <a href="http://realhomes.inspirythemes.biz/property/60-merrick-way-miami/">Read More</a></p>*/
/*                         <span class="price">$440,000 </span>                    </li>*/
/*                                 </ul>*/
/*             </section>    </aside><!-- End Sidebar -->*/
/* */
/* </div>*/
/*         </div><!-- End contents row -->*/
/*     </div><!-- End Content -->*/
/* */
/*     <div class="container page-carousel">*/
/*         <div class="row">*/
/*             <div class="span12">*/
/*                 <section class="brands-carousel  clearfix">*/
/*                                         <h3><span>Partners</span></h3>*/
/*                             <ul class="brands-carousel-list clearfix">*/
/*                                                                         <li>*/
/*                                             <a target="_blank" href="http://graphicriver.net/" title="graphicriver">*/
/*                                                 <img width="190" height="55" src="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-4.png" class="attachment-partners-logo size-partners-logo wp-post-image" alt="graphicriver" title="graphicriver" srcset="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-4.png 190w, http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-4-150x43.png 150w" sizes="(max-width: 190px) 100vw, 190px" />                                            </a>*/
/*                                         </li>*/
/*                                                                                 <li>*/
/*                                             <a target="_blank" href="http://photodune.net/" title="photodune">*/
/*                                                 <img width="190" height="55" src="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-2.png" class="attachment-partners-logo size-partners-logo wp-post-image" alt="photodune" title="photodune" srcset="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-2.png 190w, http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-2-150x43.png 150w" sizes="(max-width: 190px) 100vw, 190px" />                                            </a>*/
/*                                         </li>*/
/*                                                                                 <li>*/
/*                                             <a target="_blank" href="http://themeforest.net/" title="themeforest">*/
/*                                                 <img width="190" height="55" src="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-1.png" class="attachment-partners-logo size-partners-logo wp-post-image" alt="themeforest" title="themeforest" srcset="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-1.png 190w, http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-1-150x43.png 150w" sizes="(max-width: 190px) 100vw, 190px" />                                            </a>*/
/*                                         </li>*/
/*                                                                                 <li>*/
/*                                             <a target="_blank" href="http://activeden.net/" title="activeden">*/
/*                                                 <img width="190" height="55" src="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-3.png" class="attachment-partners-logo size-partners-logo wp-post-image" alt="activeden" title="activeden" srcset="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-3.png 190w, http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-3-150x43.png 150w" sizes="(max-width: 190px) 100vw, 190px" />                                            </a>*/
/*                                         </li>*/
/*                                                                                 <li>*/
/*                                             <a target="_blank" href="http://graphicriver.net/" title="graphicriver">*/
/*                                                 <img width="190" height="55" src="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-4.png" class="attachment-partners-logo size-partners-logo wp-post-image" alt="graphicriver" title="graphicriver" srcset="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-4.png 190w, http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-4-150x43.png 150w" sizes="(max-width: 190px) 100vw, 190px" />                                            </a>*/
/*                                         </li>*/
/*                                                                                 <li>*/
/*                                             <a target="_blank" href="http://videohive.net/" title="videohive">*/
/*                                                 <img width="190" height="55" src="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-5.png" class="attachment-partners-logo size-partners-logo wp-post-image" alt="videohive" title="videohive" srcset="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-5.png 190w, http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-5-150x43.png 150w" sizes="(max-width: 190px) 100vw, 190px" />                                            </a>*/
/*                                         </li>*/
/*                                                                                 <li>*/
/*                                             <a target="_blank" href="http://photodune.net/" title="photodune">*/
/*                                                 <img width="190" height="55" src="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-2.png" class="attachment-partners-logo size-partners-logo wp-post-image" alt="photodune" title="photodune" srcset="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-2.png 190w, http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-2-150x43.png 150w" sizes="(max-width: 190px) 100vw, 190px" />                                            </a>*/
/*                                         </li>*/
/*                                                                                 <li>*/
/*                                             <a target="_blank" href="http://themeforest.net/" title="themeforest">*/
/*                                                 <img width="190" height="55" src="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-1.png" class="attachment-partners-logo size-partners-logo wp-post-image" alt="themeforest" title="themeforest" srcset="http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-1.png 190w, http://realhomes.inspirythemes.biz/wp-content/uploads/2013/08/logo-1-150x43.png 150w" sizes="(max-width: 190px) 100vw, 190px" />                                            </a>*/
/*                                         </li>*/
/*                                                                     </ul>*/
/*                 </section>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     */
/* <!-- Start Footer -->*/
/* <footer id="footer-wrapper">*/
/* */
/*        <div id="footer" class="container">*/
/* */
/*                 <div class="row">*/
/* */
/*                         <div class="span3">*/
/*                             <section id="text-3" class="widget clearfix widget_text"><h3 class="title">About Real Homes</h3>			<div class="textwidget"><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>*/
/* <p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>*/
/* </div>*/
/* 		</section>                        </div>*/
/* */
/*                         <div class="span3">*/
/*                             		<section id="recent-posts-4" class="widget clearfix widget_recent_entries">		<h3 class="title">Recent Posts</h3>		<ul>*/
/* 					<li>*/
/* 				<a href="http://realhomes.inspirythemes.biz/lorem-post-with-image-format/">Lorem Post With Image Format</a>*/
/* 						</li>*/
/* 					<li>*/
/* 				<a href="http://realhomes.inspirythemes.biz/example-video-blog-post/">Example Video Blog Post</a>*/
/* 						</li>*/
/* 					<li>*/
/* 				<a href="http://realhomes.inspirythemes.biz/example-post-with-gallery-post-format/">Example Post With Gallery Post Format</a>*/
/* 						</li>*/
/* 					<li>*/
/* 				<a href="http://realhomes.inspirythemes.biz/example-post-with-image-post-format/">Example Post With Image Post Format</a>*/
/* 						</li>*/
/* 					<li>*/
/* 				<a href="http://realhomes.inspirythemes.biz/lorem-ipsum-dolor-sit-amet/">Lorem Ipsum Dolor Sit Amet</a>*/
/* 						</li>*/
/* 				</ul>*/
/* 		</section>		                        </div>*/
/* */
/*                         <div class="clearfix visible-tablet"></div>*/
/* */
/*                         <div class="span3">*/
/*                             <section id="displaytweetswidget-2" class="widget clearfix widget_displaytweetswidget"><h3 class="title">Latest Tweets</h3><p>You should clean out your <a href="http://twitter.com/search?q=%23WordPress&src=hash" target="_blank">#WordPress</a> Themes Directory.*/
/* <a href="https://t.co/bsDsl4qjJA" target="_blank">https://t.co/bsDsl4qjJA</a><br /><small class="muted">- Friday May 13 - 4:59am</small></p><p>Both of our <a href="http://twitter.com/search?q=%23RealEstate&src=hash" target="_blank">#RealEstate</a> <a href="http://twitter.com/search?q=%23WordPRess&src=hash" target="_blank">#WordPRess</a> Themes have the latest Visual Composer Plugin 4.11.2.1*/
/* <a href="https://t.co/j9ofmliCmS" target="_blank">https://t.co/j9ofmliCmS</a>*/
/* <a href="https://t.co/J3iLTWPJ0J" target="_blank">https://t.co/J3iLTWPJ0J</a><br /><small class="muted">- Thursday Apr 28 - 8:53am</small></p></section>                        </div>*/
/* */
/*                         <div class="span3">*/
/*                             <section id="text-2" class="widget clearfix widget_text"><h3 class="title">Contact Info</h3>			<div class="textwidget"><p>3015 Grand Ave, Coconut Grove,<br />*/
/* Merrick Way, FL 12345</p>*/
/* <p>Phone: 123-456-7890</p>*/
/* <p>Email: <a href="mailto:info@yourwebsite.com">info@yourwebsite.com</a></p>*/
/* </div>*/
/* 		</section>                        </div>*/
/*                 </div>*/
/* */
/*        </div>*/
/* */
/*         <!-- Footer Bottom -->*/
/*         <div id="footer-bottom" class="container">*/
/* */
/*                 <div class="row">*/
/*                         <div class="span6">*/
/*                             <p class="copyright">Copyright © 2015. All Rights Reserved.</p>                        </div>*/
/*                         <div class="span6">*/
/*                             <p class="designed-by">A Theme by <a target="_blank" href="http://themeforest.net/user/inspirythemes/portfolio">Inspiry Themes</a></p>                        </div>*/
/*                 </div>*/
/* */
/*         </div>*/
/*         <!-- End Footer Bottom -->*/
/* */
/* </footer><!-- End Footer -->*/
/* */
/* <!-- Login Modal -->*/
/* <div id="login-modal" class="forms-modal modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">*/
/* */
/*     <div class="modal-header">*/
/*         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>*/
/*         <p>You need to log in to use member only features.</p>*/
/*     </div>*/
/* */
/*     <!-- start of modal body -->*/
/*     <div class="modal-body">*/
/* */
/*         <!-- login section -->*/
/*         <div class="login-section modal-section">*/
/*             <h4>Login</h4>*/
/*             <form id="login-form" class="login-form" action="http://realhomes.inspirythemes.biz/wp-admin/admin-ajax.php" method="post" enctype="multipart/form-data">*/
/*                 <div class="form-option">*/
/*                     <label for="username">User Name<span>*</span></label>*/
/*                     <input id="username" name="log" type="text" class="required" title="* Provide user name!" autofocus required/>*/
/*                 </div>*/
/*                 <div class="form-option">*/
/*                     <label for="password">Password<span>*</span></label>*/
/*                     <input id="password" name="pwd" type="password" class="required" title="* Provide password!" required/>*/
/*                 </div>*/
/*                 <input type="hidden" name="action" value="inspiry_ajax_login" />*/
/*                 <input type="hidden" id="inspiry-secure-login" name="inspiry-secure-login" value="72515f4468" /><input type="hidden" name="_wp_http_referer" value="/property/villa-in-hialeah-dade-county/" /><input type="hidden" name="redirect_to" value="http://realhomes.inspirythemes.biz/property/villa-in-hialeah-dade-county/" />                <input type="hidden" name="user-cookie" value="1" />*/
/*                 <input type="submit" id="login-button" name="submit" value="Log in" class="real-btn login-btn" />*/
/*                 <img id="login-loader" class="modal-loader" src="http://realhomes.inspirythemes.biz/wp-content/themes/realhomes/images/ajax-loader.gif" alt="Working...">*/
/*                 <div>*/
/*                     <div id="login-message" class="modal-message"></div>*/
/*                     <div id="login-error" class="modal-error"></div>*/
/*                 </div>*/
/*             </form>*/
/*             <p>*/
/*                                     <a class="activate-section" data-section="register-section" href="#">Register Here</a>*/
/*                     <span class="divider">-</span>*/
/*                                 <a class="activate-section" data-section="forgot-section" href="#">Forgot Password</a>*/
/*             </p>*/
/*         </div>*/
/* */
/*         <!-- forgot section -->*/
/*         <div class="forgot-section modal-section">*/
/*             <h4>Reset Password</h4>*/
/*             <form action="http://realhomes.inspirythemes.biz/wp-admin/admin-ajax.php" id="forgot-form"  method="post" enctype="multipart/form-data">*/
/*                 <div class="form-option">*/
/*                     <label for="reset_username_or_email">Username or Email<span>*</span></label>*/
/*                     <input id="reset_username_or_email" name="reset_username_or_email" type="text" class="required" title="* Provide username or email!" required/>*/
/*                 </div>*/
/*                 <input type="hidden" name="action" value="inspiry_ajax_forgot" />*/
/*                 <input type="hidden" name="user-cookie" value="1" />*/
/*                 <input type="submit"  id="forgot-button" name="user-submit" value="Reset Password" class="real-btn register-btn" />*/
/* 	            <img id="forgot-loader" class="modal-loader" src="http://realhomes.inspirythemes.biz/wp-content/themes/realhomes/images/ajax-loader.gif" alt="Working...">*/
/*                 <input type="hidden" id="inspiry-secure-reset" name="inspiry-secure-reset" value="a5876dde24" /><input type="hidden" name="_wp_http_referer" value="/property/villa-in-hialeah-dade-county/" />                <div>*/
/*                     <div id="forgot-message" class="modal-message"></div>*/
/*                     <div id="forgot-error" class="modal-error"></div>*/
/*                 </div>*/
/*             </form>*/
/*             <p>*/
/*                 <a class="activate-section" data-section="login-section" href="#">Login Here</a>*/
/*                                     <span class="divider">-</span>*/
/*                     <a class="activate-section" data-section="register-section" href="#">Register Here</a>*/
/*                             </p>*/
/*         </div>*/
/* */
/*                     <!-- register section -->*/
/*             <div class="register-section modal-section">*/
/*                 <h4>Register</h4>*/
/*                 <form action="http://realhomes.inspirythemes.biz/wp-admin/admin-ajax.php" id="register-form"  method="post" enctype="multipart/form-data">*/
/* */
/*                     <div class="form-option">*/
/*                         <label for="register_username" class="">User Name<span>*</span></label>*/
/*                         <input id="register_username" name="register_username" type="text" class="required"*/
/*                                title="* Provide user name!" required/>*/
/*                     </div>*/
/* */
/*                     <div class="form-option">*/
/*                         <label for="register_email" class="">Email<span>*</span></label>*/
/*                         <input id="register_email" name="register_email" type="text" class="email required"*/
/*                                title="* Provide valid email address!" required/>*/
/*                     </div>*/
/* */
/*                     <input type="hidden" name="user-cookie" value="1" />*/
/*                     <input type="submit" id="register-button" name="user-submit" value="Register" class="real-btn register-btn" />*/
/* 	                <img id="register-loader" class="modal-loader" src="http://realhomes.inspirythemes.biz/wp-content/themes/realhomes/images/ajax-loader.gif" alt="Working...">*/
/*                     <input type="hidden" name="action" value="inspiry_ajax_register" />*/
/*                     <input type="hidden" id="inspiry-secure-register" name="inspiry-secure-register" value="f998f8232b" /><input type="hidden" name="_wp_http_referer" value="/property/villa-in-hialeah-dade-county/" /><input type="hidden" name="redirect_to" value="http://realhomes.inspirythemes.biz/property/villa-in-hialeah-dade-county/" />*/
/*                     <div>*/
/*                         <div id="register-message" class="modal-message"></div>*/
/*                         <div id="register-error" class="modal-error"></div>*/
/*                     </div>*/
/* */
/*                 </form>*/
/*                 <p>*/
/*                     <a class="activate-section" data-section="login-section" href="#">Login Here</a>*/
/*                     <span class="divider">-</span>*/
/*                     <a class="activate-section" data-section="forgot-section" href="#">Forgot Password</a>*/
/*                 </p>*/
/*             </div>*/
/*         */
/*     </div>*/
/*     <!-- end of modal-body -->*/
/* */
/* </div>*/
/* <a href="#top" id="scroll-top"><i class="fa fa-chevron-up"></i></a>*/
/* */
/* <script type='text/javascript' src='http://realhomes.inspirythemes.biz/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4'></script>*/
/* <script type='text/javascript' src='http://realhomes.inspirythemes.biz/wp-includes/js/jquery/ui/widget.min.js?ver=1.11.4'></script>*/
/* <script type='text/javascript' src='http://realhomes.inspirythemes.biz/wp-includes/js/jquery/ui/position.min.js?ver=1.11.4'></script>*/
/* <script type='text/javascript' src='http://realhomes.inspirythemes.biz/wp-includes/js/jquery/ui/menu.min.js?ver=1.11.4'></script>*/
/* <script type='text/javascript' src='http://realhomes.inspirythemes.biz/wp-includes/js/wp-a11y.min.js?ver=4.5.2'></script>*/
/* <script type='text/javascript'>*/
/* /* <![CDATA[ *//* */
/* var uiAutocompleteL10n = {"noResults":"No search results.","oneResult":"1 result found. Use up and down arrow keys to navigate.","manyResults":"%d results found. Use up and down arrow keys to navigate."};*/
/* /* ]]> *//* */
/* </script>*/
/* <script type='text/javascript' src='http://realhomes.inspirythemes.biz/wp-includes/js/jquery/ui/autocomplete.min.js?ver=1.11.4'></script>*/
/* <script type='text/javascript' src='http://realhomes.inspirythemes.biz/wp-content/themes/realhomes/js/inspiry-login-register.js?ver=2.5.5'></script>*/
/* <script type='text/javascript'>*/
/* /* <![CDATA[ *//* */
/* var localizedSearchParams = {"rent_slug":"for-rent"};*/
/* var locationData = {"any_text":"Any","any_value":"any","all_locations":[{"term_id":27,"name":"Miami","slug":"miami","parent":0},{"term_id":41,"name":"Little Havana","slug":"little-havana","parent":27},{"term_id":30,"name":"Perrine","slug":"perrine","parent":27},{"term_id":40,"name":"Doral","slug":"doral","parent":27},{"term_id":48,"name":"Hialeah","slug":"hialeah","parent":27}],"select_names":["location","child-location","grandchild-location","great-grandchild-location"],"select_count":"1","locations_in_params":[]};*/
/* /* ]]> *//* */
/* </script>*/
/* <script type='text/javascript' src='http://realhomes.inspirythemes.biz/wp-content/themes/realhomes/js/inspiry-search-form.js?ver=2.5.5'></script>*/
/* <script type='text/javascript'>*/
/* /* <![CDATA[ *//* */
/* var localized = {"nav_title":"Go to..."};*/
/* /* ]]> *//* */
/* </script>*/
/* <script type='text/javascript' src='http://realhomes.inspirythemes.biz/wp-content/themes/realhomes/js/custom.js?ver=2.5.5'></script>*/
/* <script type='text/javascript' src='http://realhomes.inspirythemes.biz/wp-includes/js/wp-embed.min.js?ver=4.5.2'></script>*/
/* <script type='text/javascript' src='http://realhomes.inspirythemes.biz/wp-content/plugins/dsidxpress/js/autocomplete.js?ver=2.1.32'></script>*/
/* </body>*/
/* {% endblock %}*/
