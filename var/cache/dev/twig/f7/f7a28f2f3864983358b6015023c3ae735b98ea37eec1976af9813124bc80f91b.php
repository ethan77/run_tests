<?php

/* AssetsImobile/list.html.twig */
class __TwigTemplate_6b09a8cf37aa648322b9b10ce8655a8b0c0c336b06a3a37245a1e596a061601d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("navigation.html.twig", "AssetsImobile/list.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "navigation.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_42e0037f1da5330d43d880d399be5f012376621cc86683e2fada3f0b4fc637ed = $this->env->getExtension("native_profiler");
        $__internal_42e0037f1da5330d43d880d399be5f012376621cc86683e2fada3f0b4fc637ed->enter($__internal_42e0037f1da5330d43d880d399be5f012376621cc86683e2fada3f0b4fc637ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AssetsImobile/list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_42e0037f1da5330d43d880d399be5f012376621cc86683e2fada3f0b4fc637ed->leave($__internal_42e0037f1da5330d43d880d399be5f012376621cc86683e2fada3f0b4fc637ed_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_39ea72c8a7224cb1fd804e1e13d62107d6d89b858917bf3c1bc15dffc141bc4a = $this->env->getExtension("native_profiler");
        $__internal_39ea72c8a7224cb1fd804e1e13d62107d6d89b858917bf3c1bc15dffc141bc4a->enter($__internal_39ea72c8a7224cb1fd804e1e13d62107d6d89b858917bf3c1bc15dffc141bc4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    descriere ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["asset"]) ? $context["asset"] : $this->getContext($context, "asset")), "descriere", array()), "html", null, true);
        echo "
";
        // line 5
        echo "   ";
        
        $__internal_39ea72c8a7224cb1fd804e1e13d62107d6d89b858917bf3c1bc15dffc141bc4a->leave($__internal_39ea72c8a7224cb1fd804e1e13d62107d6d89b858917bf3c1bc15dffc141bc4a_prof);

    }

    public function getTemplateName()
    {
        return "AssetsImobile/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 5,  40 => 3,  34 => 2,  11 => 1,);
    }
}
/*     {% extends 'navigation.html.twig' %}*/
/* {% block body %}*/
/*     descriere {{asset.descriere}}*/
/* {#    Id-ul pentru {{Executari.name}} este {{Executari.id}} */
/* #}   {# <form action="{{ path('get_asset_details')}}">*/
/*         <input type="text" name="mmmmmmm">*/
/*         <input type="submit" value="Submit">*/
/*     </form>#}*/
/* {% endblock %}*/
