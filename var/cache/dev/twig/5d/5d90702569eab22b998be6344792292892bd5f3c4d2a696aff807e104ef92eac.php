<?php

/* base.html.twig */
class __TwigTemplate_859923e3154886188e448a1cf4452d7d95b2b17c53f8995fa8049abf4d1a7e64 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6461dc417be07ec8ee397bf5df58a647e50f2e322ba40810c1da9440ec2deef1 = $this->env->getExtension("native_profiler");
        $__internal_6461dc417be07ec8ee397bf5df58a647e50f2e322ba40810c1da9440ec2deef1->enter($__internal_6461dc417be07ec8ee397bf5df58a647e50f2e322ba40810c1da9440ec2deef1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        ";
        // line 4
        $this->displayBlock('head', $context, $blocks);
        // line 5
        echo "    </head>
    <body>
        ";
        // line 7
        $this->displayBlock('body', $context, $blocks);
        // line 8
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 9
        echo "        ";
        // line 11
        echo "        </div>
    </body>
</html>
";
        
        $__internal_6461dc417be07ec8ee397bf5df58a647e50f2e322ba40810c1da9440ec2deef1->leave($__internal_6461dc417be07ec8ee397bf5df58a647e50f2e322ba40810c1da9440ec2deef1_prof);

    }

    // line 4
    public function block_head($context, array $blocks = array())
    {
        $__internal_989c19af6324f0506377789a825fe6f0fecb3a4cd096f2baa71725f4778f8347 = $this->env->getExtension("native_profiler");
        $__internal_989c19af6324f0506377789a825fe6f0fecb3a4cd096f2baa71725f4778f8347->enter($__internal_989c19af6324f0506377789a825fe6f0fecb3a4cd096f2baa71725f4778f8347_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_989c19af6324f0506377789a825fe6f0fecb3a4cd096f2baa71725f4778f8347->leave($__internal_989c19af6324f0506377789a825fe6f0fecb3a4cd096f2baa71725f4778f8347_prof);

    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        $__internal_14591b9e2b019ea7e939b401435c3d0c5df1e991628d04d72bfbcc22c00262d7 = $this->env->getExtension("native_profiler");
        $__internal_14591b9e2b019ea7e939b401435c3d0c5df1e991628d04d72bfbcc22c00262d7->enter($__internal_14591b9e2b019ea7e939b401435c3d0c5df1e991628d04d72bfbcc22c00262d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_14591b9e2b019ea7e939b401435c3d0c5df1e991628d04d72bfbcc22c00262d7->leave($__internal_14591b9e2b019ea7e939b401435c3d0c5df1e991628d04d72bfbcc22c00262d7_prof);

    }

    // line 8
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_7cf4fc94fd58d9d97b3e6abd406eb98abf012b0fe7e68f5f916c6e9349a0413f = $this->env->getExtension("native_profiler");
        $__internal_7cf4fc94fd58d9d97b3e6abd406eb98abf012b0fe7e68f5f916c6e9349a0413f->enter($__internal_7cf4fc94fd58d9d97b3e6abd406eb98abf012b0fe7e68f5f916c6e9349a0413f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_7cf4fc94fd58d9d97b3e6abd406eb98abf012b0fe7e68f5f916c6e9349a0413f->leave($__internal_7cf4fc94fd58d9d97b3e6abd406eb98abf012b0fe7e68f5f916c6e9349a0413f_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  75 => 8,  64 => 7,  53 => 4,  43 => 11,  41 => 9,  38 => 8,  36 => 7,  32 => 5,  30 => 4,  25 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         {% block head %}{% endblock %}*/
/*     </head>*/
/*     <body>*/
/*         {% block body %}{% endblock %}*/
/*         {% block javascripts %}{% endblock %}*/
/*         {#<div class="yoz" style="color:red">{% block asset %}*/
/*             {% endblock asset%}#}*/
/*         </div>*/
/*     </body>*/
/* </html>*/
/* */
