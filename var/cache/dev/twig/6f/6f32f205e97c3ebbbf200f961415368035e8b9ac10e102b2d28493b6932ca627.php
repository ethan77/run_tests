<?php

/* navigation.html.twig */
class __TwigTemplate_40aefc6a2cbb992d5766385f96e06ab8708ccbba4ff8f3b11a14abdfddf3cde0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_54691527305c92a7e0bb8e3f86a6535517d7955270f33ad4dbb68758ff216378 = $this->env->getExtension("native_profiler");
        $__internal_54691527305c92a7e0bb8e3f86a6535517d7955270f33ad4dbb68758ff216378->enter($__internal_54691527305c92a7e0bb8e3f86a6535517d7955270f33ad4dbb68758ff216378_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "navigation.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_54691527305c92a7e0bb8e3f86a6535517d7955270f33ad4dbb68758ff216378->leave($__internal_54691527305c92a7e0bb8e3f86a6535517d7955270f33ad4dbb68758ff216378_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_fd2279417061e08be6fd0218babc204fac2b163d979104cd691352048a902ffd = $this->env->getExtension("native_profiler");
        $__internal_fd2279417061e08be6fd0218babc204fac2b163d979104cd691352048a902ffd->enter($__internal_fd2279417061e08be6fd0218babc204fac2b163d979104cd691352048a902ffd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome nigga!";
        
        $__internal_fd2279417061e08be6fd0218babc204fac2b163d979104cd691352048a902ffd->leave($__internal_fd2279417061e08be6fd0218babc204fac2b163d979104cd691352048a902ffd_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_ee74cb332ae58216636d6f6d2341641fef340687681b36db419483303fb6e8f0 = $this->env->getExtension("native_profiler");
        $__internal_ee74cb332ae58216636d6f6d2341641fef340687681b36db419483303fb6e8f0->enter($__internal_ee74cb332ae58216636d6f6d2341641fef340687681b36db419483303fb6e8f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_ee74cb332ae58216636d6f6d2341641fef340687681b36db419483303fb6e8f0->leave($__internal_ee74cb332ae58216636d6f6d2341641fef340687681b36db419483303fb6e8f0_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_56b8ed2955f1206496b7cdd8fe58dd191218083d62d38874a6050dbc6671e3f5 = $this->env->getExtension("native_profiler");
        $__internal_56b8ed2955f1206496b7cdd8fe58dd191218083d62d38874a6050dbc6671e3f5->enter($__internal_56b8ed2955f1206496b7cdd8fe58dd191218083d62d38874a6050dbc6671e3f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_56b8ed2955f1206496b7cdd8fe58dd191218083d62d38874a6050dbc6671e3f5->leave($__internal_56b8ed2955f1206496b7cdd8fe58dd191218083d62d38874a6050dbc6671e3f5_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_002b3e98dc92eb88cfe6f4262c59bc8beaa30af93f33db433e31ec4613b515cf = $this->env->getExtension("native_profiler");
        $__internal_002b3e98dc92eb88cfe6f4262c59bc8beaa30af93f33db433e31ec4613b515cf->enter($__internal_002b3e98dc92eb88cfe6f4262c59bc8beaa30af93f33db433e31ec4613b515cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_002b3e98dc92eb88cfe6f4262c59bc8beaa30af93f33db433e31ec4613b515cf->leave($__internal_002b3e98dc92eb88cfe6f4262c59bc8beaa30af93f33db433e31ec4613b515cf_prof);

    }

    public function getTemplateName()
    {
        return "navigation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome nigga!{% endblock %}</title>*/
/*         {% block stylesheets %}{% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*         {% block body %}{% endblock %}*/
/*         {% block javascripts %}{% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
