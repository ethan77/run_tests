<?php

/* send_offer_modal.html.twig */
class __TwigTemplate_60dba119203e7fae5a2d98036951e2dd460d4a48eb5b2ea16a50e723329e72a3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4a2856416ff045ca86abfbaa43e8f6cf7b7061a265ec92251e403dd73906da47 = $this->env->getExtension("native_profiler");
        $__internal_4a2856416ff045ca86abfbaa43e8f6cf7b7061a265ec92251e403dd73906da47->enter($__internal_4a2856416ff045ca86abfbaa43e8f6cf7b7061a265ec92251e403dd73906da47_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "send_offer_modal.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_4a2856416ff045ca86abfbaa43e8f6cf7b7061a265ec92251e403dd73906da47->leave($__internal_4a2856416ff045ca86abfbaa43e8f6cf7b7061a265ec92251e403dd73906da47_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_1dacefd0c7d353c58aed463e1fdda25211fe4ca1c25e6fff41e5ecf8e97cb2fc = $this->env->getExtension("native_profiler");
        $__internal_1dacefd0c7d353c58aed463e1fdda25211fe4ca1c25e6fff41e5ecf8e97cb2fc->enter($__internal_1dacefd0c7d353c58aed463e1fdda25211fe4ca1c25e6fff41e5ecf8e97cb2fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome nigga!";
        
        $__internal_1dacefd0c7d353c58aed463e1fdda25211fe4ca1c25e6fff41e5ecf8e97cb2fc->leave($__internal_1dacefd0c7d353c58aed463e1fdda25211fe4ca1c25e6fff41e5ecf8e97cb2fc_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_e0a09526910603cbe20db477a6c88f58ff9e3e64cbf3c7baa000cabe4b601211 = $this->env->getExtension("native_profiler");
        $__internal_e0a09526910603cbe20db477a6c88f58ff9e3e64cbf3c7baa000cabe4b601211->enter($__internal_e0a09526910603cbe20db477a6c88f58ff9e3e64cbf3c7baa000cabe4b601211_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_e0a09526910603cbe20db477a6c88f58ff9e3e64cbf3c7baa000cabe4b601211->leave($__internal_e0a09526910603cbe20db477a6c88f58ff9e3e64cbf3c7baa000cabe4b601211_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_a5c404297aed3bcc9a210a1f0e2371edccc82eb8d8fc86e36db6de68ff93bedc = $this->env->getExtension("native_profiler");
        $__internal_a5c404297aed3bcc9a210a1f0e2371edccc82eb8d8fc86e36db6de68ff93bedc->enter($__internal_a5c404297aed3bcc9a210a1f0e2371edccc82eb8d8fc86e36db6de68ff93bedc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_a5c404297aed3bcc9a210a1f0e2371edccc82eb8d8fc86e36db6de68ff93bedc->leave($__internal_a5c404297aed3bcc9a210a1f0e2371edccc82eb8d8fc86e36db6de68ff93bedc_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_9debed09b4075daa97a8535a9b928d793dd421380473691946b64fbbc421153c = $this->env->getExtension("native_profiler");
        $__internal_9debed09b4075daa97a8535a9b928d793dd421380473691946b64fbbc421153c->enter($__internal_9debed09b4075daa97a8535a9b928d793dd421380473691946b64fbbc421153c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_9debed09b4075daa97a8535a9b928d793dd421380473691946b64fbbc421153c->leave($__internal_9debed09b4075daa97a8535a9b928d793dd421380473691946b64fbbc421153c_prof);

    }

    public function getTemplateName()
    {
        return "send_offer_modal.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome nigga!{% endblock %}</title>*/
/*         {% block stylesheets %}{% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*         {% block body %}{% endblock %}*/
/*         {% block javascripts %}{% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
