<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_2d410a3256ca05164dc1d3b58c6337c94c21ab9e464c01aa91fd645436793270 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4196e4a9a5a8cb9ae7a47b685c365d7caf59d62a17e603019ed1ee79a1f0bb19 = $this->env->getExtension("native_profiler");
        $__internal_4196e4a9a5a8cb9ae7a47b685c365d7caf59d62a17e603019ed1ee79a1f0bb19->enter($__internal_4196e4a9a5a8cb9ae7a47b685c365d7caf59d62a17e603019ed1ee79a1f0bb19_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4196e4a9a5a8cb9ae7a47b685c365d7caf59d62a17e603019ed1ee79a1f0bb19->leave($__internal_4196e4a9a5a8cb9ae7a47b685c365d7caf59d62a17e603019ed1ee79a1f0bb19_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_680c48981e423b3c3f30d656df600f37ae23891114022eb61d60fe69294858dd = $this->env->getExtension("native_profiler");
        $__internal_680c48981e423b3c3f30d656df600f37ae23891114022eb61d60fe69294858dd->enter($__internal_680c48981e423b3c3f30d656df600f37ae23891114022eb61d60fe69294858dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_680c48981e423b3c3f30d656df600f37ae23891114022eb61d60fe69294858dd->leave($__internal_680c48981e423b3c3f30d656df600f37ae23891114022eb61d60fe69294858dd_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_fcf14663f30f38b792d46c2fd087ca0e6d4187dc7390d7efd3ae6a7cd47ee566 = $this->env->getExtension("native_profiler");
        $__internal_fcf14663f30f38b792d46c2fd087ca0e6d4187dc7390d7efd3ae6a7cd47ee566->enter($__internal_fcf14663f30f38b792d46c2fd087ca0e6d4187dc7390d7efd3ae6a7cd47ee566_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_fcf14663f30f38b792d46c2fd087ca0e6d4187dc7390d7efd3ae6a7cd47ee566->leave($__internal_fcf14663f30f38b792d46c2fd087ca0e6d4187dc7390d7efd3ae6a7cd47ee566_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_fffa49f793a2a26990d1f537c2796acf9d3c7bfb19cee71340085455b08ecefd = $this->env->getExtension("native_profiler");
        $__internal_fffa49f793a2a26990d1f537c2796acf9d3c7bfb19cee71340085455b08ecefd->enter($__internal_fffa49f793a2a26990d1f537c2796acf9d3c7bfb19cee71340085455b08ecefd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_fffa49f793a2a26990d1f537c2796acf9d3c7bfb19cee71340085455b08ecefd->leave($__internal_fffa49f793a2a26990d1f537c2796acf9d3c7bfb19cee71340085455b08ecefd_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
