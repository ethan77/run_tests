<?php

/* footer.html.twig */
class __TwigTemplate_8081b5fa4d150cb9e1e5e8e8f9b2b2af64b2811315a492e79943964a1dc43b9a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6bce2d8d894895e0655794aef5716e8a4b5ac171953cf2a12f50176fdfe3da51 = $this->env->getExtension("native_profiler");
        $__internal_6bce2d8d894895e0655794aef5716e8a4b5ac171953cf2a12f50176fdfe3da51->enter($__internal_6bce2d8d894895e0655794aef5716e8a4b5ac171953cf2a12f50176fdfe3da51_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "footer.html.twig"));

        // line 1
        echo "<footer id=\"footer-wrapper\">
            <div id=\"footer\" class=\"container\">
                <div class=\"row\">
                    <div class=\"span3\">
                        <section id=\"text-3\" class=\"widget clearfix widget_text\">
                            <h3 class=\"title\">About Real Homes</h3>
                            <div class=\"textwidget\">
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
                                <p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
                            </div>
                        </section>
                    </div>
                    <div class=\"span3\">
                        <section id=\"recent-posts-4\" class=\"widget clearfix widget_recent_entries\">
                            <h3 class=\"title\">Recent Posts</h3>
                            <ul>
                                <li>
                                    <a href=\"http://realhomes.inspirythemes.biz/lorem-post-with-image-format/\">Lorem Post With Image Format</a>
                                </li>
                                <li>
                                    <a href=\"http://realhomes.inspirythemes.biz/example-video-blog-post/\">Example Video Blog Post</a>
                                </li>
                                <li>
                                    <a href=\"http://realhomes.inspirythemes.biz/example-post-with-gallery-post-format/\">Example Post With Gallery Post Format</a>
                                </li>
                                <li>
                                    <a href=\"http://realhomes.inspirythemes.biz/example-post-with-image-post-format/\">Example Post With Image Post Format</a>
                                </li>
                                <li>
                                    <a href=\"http://realhomes.inspirythemes.biz/lorem-ipsum-dolor-sit-amet/\">Lorem Ipsum Dolor Sit Amet</a>
                                </li>
                            </ul>
                        </section>
                    </div>
                    <div class=\"clearfix visible-tablet\"></div>
                    <div class=\"span3\">
                        <section id=\"displaytweetswidget-2\" class=\"widget clearfix widget_displaytweetswidget\">
                            <h3 class=\"title\">Latest Tweets</h3>
                            <p>You should clean out your <a href=\"http://twitter.com/search?q=%23WordPress&amp;src=hash\" target=\"_blank\">#WordPress</a> Themes Directory.
                                <a href=\"https://t.co/bsDsl4qjJA\" target=\"_blank\">https://t.co/bsDsl4qjJA</a><br><small class=\"muted\">- Friday May 13 - 4:59am</small>
                            </p>
                            <p>Both of our <a href=\"http://twitter.com/search?q=%23RealEstate&amp;src=hash\" target=\"_blank\">#RealEstate</a> <a href=\"http://twitter.com/search?q=%23WordPRess&amp;src=hash\" target=\"_blank\">#WordPRess</a> Themes have the latest Visual Composer Plugin 4.11.2.1
                                <a href=\"https://t.co/j9ofmliCmS\" target=\"_blank\">https://t.co/j9ofmliCmS</a>
                                <a href=\"https://t.co/J3iLTWPJ0J\" target=\"_blank\">https://t.co/J3iLTWPJ0J</a><br><small class=\"muted\">- Thursday Apr 28 - 8:53am</small>
                            </p>
                        </section>
                    </div>
                    <div class=\"span3\">
                        <section id=\"text-2\" class=\"widget clearfix widget_text\">
                            <h3 class=\"title\">Contact Info</h3>
                            <div class=\"textwidget\">
                                <p>3015 Grand Ave, Coconut Grove,<br>
                                    Merrick Way, FL 12345
                                </p>
                                <p>Phone: 123-456-7890</p>
                                <p>Email: <a href=\"mailto:info@yourwebsite.com\">info@yourwebsite.com</a></p>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
            <!-- Footer Bottom -->
            <div id=\"footer-bottom\" class=\"container\">
                <div class=\"row\">
                    <div class=\"span6\">
                        <p class=\"copyright\">Copyright © 2015. All Rights Reserved.</p>
                    </div>
                    <div class=\"span6\">
                        <p class=\"designed-by\">A Theme by <a target=\"_blank\" href=\"http://themeforest.net/user/inspirythemes/portfolio\">Inspiry Themes</a></p>
                    </div>
                </div>
            </div>
            <!-- End Footer Bottom -->
        </footer>";
        
        $__internal_6bce2d8d894895e0655794aef5716e8a4b5ac171953cf2a12f50176fdfe3da51->leave($__internal_6bce2d8d894895e0655794aef5716e8a4b5ac171953cf2a12f50176fdfe3da51_prof);

    }

    public function getTemplateName()
    {
        return "footer.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <footer id="footer-wrapper">*/
/*             <div id="footer" class="container">*/
/*                 <div class="row">*/
/*                     <div class="span3">*/
/*                         <section id="text-3" class="widget clearfix widget_text">*/
/*                             <h3 class="title">About Real Homes</h3>*/
/*                             <div class="textwidget">*/
/*                                 <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>*/
/*                                 <p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>*/
/*                             </div>*/
/*                         </section>*/
/*                     </div>*/
/*                     <div class="span3">*/
/*                         <section id="recent-posts-4" class="widget clearfix widget_recent_entries">*/
/*                             <h3 class="title">Recent Posts</h3>*/
/*                             <ul>*/
/*                                 <li>*/
/*                                     <a href="http://realhomes.inspirythemes.biz/lorem-post-with-image-format/">Lorem Post With Image Format</a>*/
/*                                 </li>*/
/*                                 <li>*/
/*                                     <a href="http://realhomes.inspirythemes.biz/example-video-blog-post/">Example Video Blog Post</a>*/
/*                                 </li>*/
/*                                 <li>*/
/*                                     <a href="http://realhomes.inspirythemes.biz/example-post-with-gallery-post-format/">Example Post With Gallery Post Format</a>*/
/*                                 </li>*/
/*                                 <li>*/
/*                                     <a href="http://realhomes.inspirythemes.biz/example-post-with-image-post-format/">Example Post With Image Post Format</a>*/
/*                                 </li>*/
/*                                 <li>*/
/*                                     <a href="http://realhomes.inspirythemes.biz/lorem-ipsum-dolor-sit-amet/">Lorem Ipsum Dolor Sit Amet</a>*/
/*                                 </li>*/
/*                             </ul>*/
/*                         </section>*/
/*                     </div>*/
/*                     <div class="clearfix visible-tablet"></div>*/
/*                     <div class="span3">*/
/*                         <section id="displaytweetswidget-2" class="widget clearfix widget_displaytweetswidget">*/
/*                             <h3 class="title">Latest Tweets</h3>*/
/*                             <p>You should clean out your <a href="http://twitter.com/search?q=%23WordPress&amp;src=hash" target="_blank">#WordPress</a> Themes Directory.*/
/*                                 <a href="https://t.co/bsDsl4qjJA" target="_blank">https://t.co/bsDsl4qjJA</a><br><small class="muted">- Friday May 13 - 4:59am</small>*/
/*                             </p>*/
/*                             <p>Both of our <a href="http://twitter.com/search?q=%23RealEstate&amp;src=hash" target="_blank">#RealEstate</a> <a href="http://twitter.com/search?q=%23WordPRess&amp;src=hash" target="_blank">#WordPRess</a> Themes have the latest Visual Composer Plugin 4.11.2.1*/
/*                                 <a href="https://t.co/j9ofmliCmS" target="_blank">https://t.co/j9ofmliCmS</a>*/
/*                                 <a href="https://t.co/J3iLTWPJ0J" target="_blank">https://t.co/J3iLTWPJ0J</a><br><small class="muted">- Thursday Apr 28 - 8:53am</small>*/
/*                             </p>*/
/*                         </section>*/
/*                     </div>*/
/*                     <div class="span3">*/
/*                         <section id="text-2" class="widget clearfix widget_text">*/
/*                             <h3 class="title">Contact Info</h3>*/
/*                             <div class="textwidget">*/
/*                                 <p>3015 Grand Ave, Coconut Grove,<br>*/
/*                                     Merrick Way, FL 12345*/
/*                                 </p>*/
/*                                 <p>Phone: 123-456-7890</p>*/
/*                                 <p>Email: <a href="mailto:info@yourwebsite.com">info@yourwebsite.com</a></p>*/
/*                             </div>*/
/*                         </section>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <!-- Footer Bottom -->*/
/*             <div id="footer-bottom" class="container">*/
/*                 <div class="row">*/
/*                     <div class="span6">*/
/*                         <p class="copyright">Copyright © 2015. All Rights Reserved.</p>*/
/*                     </div>*/
/*                     <div class="span6">*/
/*                         <p class="designed-by">A Theme by <a target="_blank" href="http://themeforest.net/user/inspirythemes/portfolio">Inspiry Themes</a></p>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <!-- End Footer Bottom -->*/
/*         </footer>*/
