<?php

/* executari/list.html.twig */
class __TwigTemplate_1bb2d88ff717e14c65a994067d90fcab075537c4a91b0994e5fcfab27854597d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "executari/list.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b60e49ed7f6a6ef80b3a78ffb05689da3797d0857e85a5a042563b644da8330c = $this->env->getExtension("native_profiler");
        $__internal_b60e49ed7f6a6ef80b3a78ffb05689da3797d0857e85a5a042563b644da8330c->enter($__internal_b60e49ed7f6a6ef80b3a78ffb05689da3797d0857e85a5a042563b644da8330c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "executari/list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b60e49ed7f6a6ef80b3a78ffb05689da3797d0857e85a5a042563b644da8330c->leave($__internal_b60e49ed7f6a6ef80b3a78ffb05689da3797d0857e85a5a042563b644da8330c_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_66ea3f0fe5437d7f33f69baffd0a0f06b14537af01e7cd93347dfa7b6f2df8b2 = $this->env->getExtension("native_profiler");
        $__internal_66ea3f0fe5437d7f33f69baffd0a0f06b14537af01e7cd93347dfa7b6f2df8b2->enter($__internal_66ea3f0fe5437d7f33f69baffd0a0f06b14537af01e7cd93347dfa7b6f2df8b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<!-- saved from url=(0043)http://realhomes.inspirythemes.biz/listing/ -->
    <!--<![endif]-->
    <head>
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <link rel=\"profile\" href=\"http://gmpg.org/xfn/11\">
        <meta name=\"format-detection\" content=\"telephone=no\">
        <link rel=\"shortcut icon\" href=\"http://realhomes.inspirythemes.biz/wp-content/uploads/2015/08/favicon.png\">
        <link rel=\"pingback\" href=\"http://realhomes.inspirythemes.biz/xmlrpc.php\">
        <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/yoz.css"), "html", null, true);
        echo "\">
        <title><span>Listing – Real Homes</span></title>
        <link rel=\"alternate\" type=\"application/rss+xml\" title=\"Real Homes » Listing Comments Feed\" href=\"http://realhomes.inspirythemes.biz/listing/feed/\">
        ";
        // line 21
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/wp-emoji-release.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
        <style type=\"text/css\">
            img.wp-smiley,
            img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
            }
            .popa ul { list-style-type: none; overflow: hidden; width:auto; margin-left:66px;}
.popa ul li { float:left; width: 50px; }
.popa {width:500px;}
        </style>
        <link rel=\"stylesheet\" id=\"dsidx-css\" href=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/0e43ba"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"dsidxpress-icons-css\" href=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/dsidx-icons.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"dsidxpress-unconditional-css\" href=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/client.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"dsidxwidgets-unconditional-css\" href=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/client(1).css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"paypal-ipnpublic-css\" href=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/paypal-ipn-for-wordpress-public.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"paypal-ipnpublicDataTablecss-css\" href=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/jquery.dataTables.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"paypal-ipnpublicDataTable-css\" href=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/dataTables.responsive.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"font-awesome-css\" href=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/font-awesome.min.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"quick-and-easy-faqs-css\" href=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/quick-and-easy-faqs-public.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"quick-and-easy-testimonials-css\" href=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/quick-and-easy-testimonials-public.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"lidd_mc-css\" href=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/style.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"screen\">
        <link rel=\"stylesheet\" id=\"rs-plugin-settings-css\" href=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/settings.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <style id=\"rs-plugin-settings-inline-css\" type=\"text/css\">
            .tp-button.green.custom{font-size:16px;text-transform:uppercase;border-radius:0;box-shadow:none;text-shadow:none;padding:10px 15px; letter-spacing:1px;background:#ec894d}
        </style>
        <link rel=\"stylesheet\" id=\"theme-roboto-css\" href=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/css"), "html", null, true);
        echo " type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"theme-lato-css\" href=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/css(1)"), "html", null, true);
        echo " type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"bootstrap-css-css\" href=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/bootstrap.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"responsive-css-css\" href=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/responsive.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"flexslider-css\" href=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/flexslider.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"pretty-photo-css-css\" href=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/prettyPhoto.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"swipebox-css\" href=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/swipebox.min.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"select2-css\" href=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/select2.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"main-css-css\" href=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/main.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"custom-responsive-css-css\" href=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/custom-responsive.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"parent-default-css\" href=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/style(1).css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <link rel=\"stylesheet\" id=\"parent-custom-css\" href=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/custom.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\">
        <script type=\"text/javascript\" src=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/jquery.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/jquery-migrate.min.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 68
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/ed75b6"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/paypal-ipn-for-wordpress.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/dataTables.responsive.js"), "html", null, true);
        echo "\"></script>
        <link rel=\"EditURI\" type=\"application/rsd+xml\" title=\"RSD\" href=\"http://realhomes.inspirythemes.biz/xmlrpc.php?rsd\">
        <link rel=\"wlwmanifest\" type=\"application/wlwmanifest+xml\" href=\"http://realhomes.inspirythemes.biz/wp-includes/wlwmanifest.xml\">
        <meta name=\"generator\" content=\"WordPress 4.5.2\">
        <link rel=\"canonical\" href=\"http://realhomes.inspirythemes.biz/listing/\">
        <link rel=\"shortlink\" href=\"http://realhomes.inspirythemes.biz/?p=129\">
        ";
        // line 79
        echo "<style type=\"text/css\" id=\"dynamic-css\">
            .header-wrapper, #currency-switcher #selected-currency, #currency-switcher-list li{
            background-color:#252A2B;
            }
            #logo h2 a{
            color:#ffffff;
            }
            #logo h2 a:hover, #logo h2 a:focus, #logo h2 a:active{
            color:#4dc7ec;
            }
            .tag-line span{
            color:#8b9293;
            }
            .tag-line span{
            background-color:#343a3b;
            }
            .page-head h1.page-title span{
            color:#394041;
            }
            .page-head h1.page-title span{
            background-color:#f5f4f3;
            }
            .page-head p{
            color:#ffffff;
            }
            .page-head p{
            background-color:#37B3D9;
            }
            .header-wrapper, #contact-email, #contact-email a, .user-nav a, .social_networks li a, #currency-switcher #selected-currency, #currency-switcher-list li{
            color:#929A9B;
            }
            #contact-email a:hover, .user-nav a:hover{
            color:#b0b8b9;
            }
            #header-top, .social_networks li a, .user-nav a, .header-wrapper .social_networks, #currency-switcher #selected-currency, #currency-switcher-list li{
            border-color:#343A3B;
            }
            .main-menu ul li a{
            color:#afb4b5;
            }
            .main-menu ul li.current-menu-ancestor > a, .main-menu ul li.current-menu-parent > a, .main-menu ul li.current-menu-item > a, .main-menu ul li.current_page_item > a, .main-menu ul li:hover > a, .main-menu ul li ul, .main-menu ul li ul li ul{
            background-color:#ec894d;
            }
            .main-menu ul li.current-menu-ancestor > a, .main-menu ul li.current-menu-parent > a, .main-menu ul li.current-menu-item > a, .main-menu ul li.current_page_item > a, .main-menu ul li:hover > a, .main-menu ul li ul, .main-menu ul li ul li a, .main-menu ul li ul li ul, .main-menu ul li ul li ul li a{
            color:#ffffff;
            }
            .main-menu ul li ul li:hover > a, .main-menu ul li ul li ul li:hover > a{
            background-color:#dc7d44;
            }
            .slide-description h3, .slide-description h3 a{
            color:#394041;
            }
            .slide-description h3 a:hover, .slide-description h3 a:focus, .slide-description h3 a:active{
            color:#df5400;
            }
            .slide-description p{
            color:#8b9293;
            }
            .slide-description span{
            color:#df5400;
            }
            .slide-description .know-more{
            color:#ffffff;
            }
            .slide-description .know-more{
            background-color:#37b3d9;
            }
            .slide-description .know-more:hover{
            background-color:#2aa6cc;
            }
            .property-item{
            background-color:#ffffff;
            }
            .property-item, .property-item .property-meta, .property-item .property-meta span{
            border-color:#dedede;
            }
            .property-item h4, .property-item h4 a, .es-carousel-wrapper ul li h4 a{
            color:#394041;
            }
            .property-item h4 a:hover, .property-item h4 a:focus, .property-item h4 a:active, .es-carousel-wrapper ul li h4 a:hover, .es-carousel-wrapper ul li h4 a:focus, .es-carousel-wrapper ul li h4 a:active{
            color:#df5400;
            }
            .property-item .price, .es-carousel-wrapper ul li .price, .property-item .price small{
            color:#ffffff;
            }
            .property-item .price, .es-carousel-wrapper ul li .price{
            background-color:#4dc7ec;
            }
            .property-item figure figcaption{
            color:#ffffff;
            }
            .property-item figure figcaption{
            background-color:#ec894d;
            }
            .property-item p, .es-carousel-wrapper ul li p{
            color:#8b9293;
            }
            .more-details, .es-carousel-wrapper ul li p a{
            color:#394041;
            }
            .more-details:hover, .more-details:focus, .more-details:active, .es-carousel-wrapper ul li p a:hover, .es-carousel-wrapper ul li p a:focus, .es-carousel-wrapper ul li p a:active{
            color:#df5400;
            }
            .property-item .property-meta span{
            color:#394041;
            }
            .property-item .property-meta{
            background-color:#f5f5f5;
            }
            #footer .widget .title{
            color:#394041;
            }
            #footer .widget .textwidget, #footer .widget, #footer-bottom p{
            color:#8b9293;
            }
            #footer .widget ul li a, #footer .widget a, #footer-bottom a{
            color:#75797A;
            }
            #footer .widget ul li a:hover, #footer .widget ul li a:focus, #footer.widget ul li a:active, #footer .widget a:hover, #footer .widget a:focus, #footer .widget a:active, #footer-bottom a:hover, #footer-bottom a:focus, #footer-bottom a:active{
            color:#dc7d44;
            }
            #footer-bottom{
            border-color:#dedede;
            }
            .real-btn{
            color:#ffffff;
            }
            .real-btn{
            background-color:#ec894d;
            }
            .real-btn:hover, .real-btn.current{
            color:#ffffff;
            }
            .real-btn:hover, .real-btn.current{
            background-color:#e3712c;
            }
            @media (min-width: 980px) {
            .contact-number, .contact-number .outer-strip{
            background-color:#4dc7ec;
            }
            .contact-number{
            color:#e7eff7;
            }
            .contact-number .fa-phone{
            background-color:#37b3d9;
            }
            }
        </style>
        <script type=\"text/javascript\">
            var RecaptchaOptions = {
            \ttheme : 'custom', custom_theme_widget : 'recaptcha_widget'
            };
        </script>
        <!--[if lt IE 9]>
        <script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\")}}\"></script>
        <![endif]-->
        <meta name=\"generator\" content=\"Powered by Slider Revolution 5.0.4.1 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface.\">
        <!-- BEGIN GADWP v4.9.3.1 Universal Tracking - https://deconf.com/google-analytics-dashboard-wordpress/ -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js\")}}','ga');
            ga('create', 'UA-31893023-8', 'auto');
            ga('send', 'pageview');
        </script>
        <!-- END GADWP Universal Tracking -->
        <script type=\"text/javascript\">
            if (typeof localdsidx == \"undefined\" || !localdsidx) { var localdsidx = {}; };
            localdsidx.pluginUrl = \"/wp-content/plugins/dsidxpress/\";
            localdsidx.homeUrl = \"http://realhomes.inspirythemes.biz\";
        </script>\t\t\t\t<script type=\"text/javascript\">
            if (typeof localdsidx == \"undefined\" || !localdsidx) { var localdsidx = {}; };
            localdsidx.pluginUrl = \"/wp-content/plugins/dsidxpress/\";
            localdsidx.homeUrl = \"http://realhomes.inspirythemes.biz\";
        </script>
    </head>
    <body class=\"page page-id-129 page-template page-template-template-property-listing page-template-template-property-listing-php\">
        <!-- Start Header -->
        <div class=\"header-wrapper\">
            <div class=\"container\">
                <!-- Start Header Container -->
                <header id=\"header\" class=\"clearfix\">
                    <div id=\"header-top\" class=\"clearfix\">
                        <h2 id=\"contact-email\">
                            <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Layer_1\" x=\"0px\" y=\"0px\" width=\"16px\" height=\"16px\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
                                <path class=\"path\" d=\"M8.174 15.926l-6.799 5.438c-0.431 0.346-0.501 0.975-0.156 1.406s0.974 0.5 1.4 0.156l7.211-5.769L8.174 15.926z\"></path>
                                <path class=\"path\" d=\"M15.838 15.936l-1.685 1.214l7.222 5.777c0.433 0.3 1.1 0.3 1.406-0.156c0.345-0.432 0.274-1.061-0.157-1.406 L15.838 15.936z\"></path>
                                <polygon class=\"path\" points=\"1,10.2 1.6,10.9 12,2.6 22,10.6 22,22 2,22 2,10.2 1,10.2 1.6,10.9 1,10.2 0,10.2 0,24 24,24 24,9.7 12,0 0,9.7 0,10.2 1,10.2 1,10.2\"></polygon>
                                <polygon class=\"path\" points=\"23.6,11.7 12.6,19.7 11.4,19.7 0.4,11.7 0.4,11.7 0.4,11.7 1.6,10.1 12,17.6 22.4,10.1\"></polygon>
                            </svg>
                            <span id=\"anda\">Email us at :</span>
                            <a href=\"mailto:sales@yourwebsite.com\">sales@yourwebsite.com</a>
                        </h2>
                        <!-- Social Navigation -->
                        <ul class=\"social_networks clearfix\">
                            <li class=\"facebook\">
                                <a target=\"_blank\" href=\"https://www.facebook.com/InspiryThemes\"><i class=\"fa fa-facebook fa-lg\"></i></a>
                            </li>
                            <li class=\"twitter\">
                                <a target=\"_blank\" href=\"https://twitter.com/InspiryThemes\"><i class=\"fa fa-twitter fa-lg\"></i></a>
                            </li>
                            <li class=\"linkedin\">
                                <a target=\"_blank\" href=\"http://realhomes.inspirythemes.biz/listing/#\"><i class=\"fa fa-linkedin fa-lg\"></i></a>
                            </li>
                            <li class=\"gplus\">
                                <a target=\"_blank\" href=\"http://realhomes.inspirythemes.biz/listing/#\"><i class=\"fa fa-google-plus fa-lg\"></i></a>
                            </li>
                            <li class=\"rss\">
                                <a target=\"_blank\" href=\"http://realhomes.inspirythemes.biz/feed/?post_type=property\"> <i class=\"fa fa-rss fa-lg\"></i></a>
                            </li>
                        </ul>
                        <!-- User Navigation -->
                        <div class=\"user-nav clearfix\">
                            <a href=\"http://realhomes.inspirythemes.biz/favorites/\">
                            <i class=\"fa fa-star\"></i>Favorites\t\t\t</a>
                            <a class=\"last\" href=\"#\" data-toggle=\"modal\"><i class=\"fa fa-sign-in\"></i>
                              Login / Register</a>\t
                        </div>
                    </div>
                    <!-- Logo -->
                    <div id=\"logo\">
                        <a title=\"Real Homes\" href=\"http://realhomes.inspirythemes.biz/\">
                        <img src=\"";
        // line 302
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/logo.png"), "html", null, true);
        echo "\" alt=\"Real Homes\">
                        </a>
                        <h2 class=\"logo-heading only-for-print\">
                            <a href=\"http://realhomes.inspirythemes.biz/\" title=\"Real Homes\">
                            Real Homes                                </a>
                        </h2>
                        <div class=\"tag-line\"><span>No. 1 Real Estate Theme</span></div>
                    </div>
                    <div class=\"menu-and-contact-wrap\">
                        <h2 class=\"contact-number \"><i class=\"fa fa-phone\"></i><span class=\"desktop-version\">1-800-555-1234</span><a class=\"mobile-version\" href=\"tel://1-800-555-1234\" title=\"Make a Call\">1-800-555-1234</a><span class=\"outer-strip\" style=\"right: -27.5px; width: 27.5px;\"></span></h2>
                        <!-- Start Main Menu-->
                        <nav class=\"main-menu\">
                            <div class=\"menu-main-menu-container\">
                                <ul id=\"menu-main-menu\" class=\"clearfix\">
                                    <li id=\"menu-item-3646\" class=\"menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3646\">
                                        <a href=\"http://realhomes.inspirythemes.biz/\">Imobile in Proprietatea bancii</a>
                                        <ul class=\"sub-menu\">
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/banca//imobile//1\">Apartamente</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/banca/imobile/2\">Case/Vile</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/banca/imobile/3\">Case de vacanta/Cabane</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/banca/imobile/4\">Terenuri</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/banca/imobile/5\">Ansambluri rezidentiale</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/banca/imobile/6\">Spatii comerciale</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/banca/imobile/7\">Spatii de birouri</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/banca/imobile/8\">Spatii industriale</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/banca/imobile/9\">Hoteluri si pensiuni</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/banca/imobile/10\">Benzinarii</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/banca/imobile/11\">Ferme</a></li>
                                        </ul>
                                    </li>
                                    <li id=\"menu-item-3654\" class=\"menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-129 current_page_item current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children menu-item-3654\">
                                        <a href=\"http://realhomes.inspirythemes.biz/listing/\"><span style=\"font-size:13px\">Imobile in lichidare patrimoniala</span></a>
                                        <ul class=\"sub-menu\" style=\"display: none;\">
                                            
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/patrimoniu/imobile/1\">Apartamente</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/patrimoniu/imobile/2\">Case/Vile</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/patrimoniu/imobile/3\">Case de vacanta/Cabane</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/patrimoniu/imobile/4\">Terenuri</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/patrimoniu/imobile/5\">Ansambluri rezidentiale</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/patrimoniu/imobile/6\">Spatii comerciale</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/patrimoniu/imobile/7\">Spatii de birouri</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/patrimoniu/imobile/8\">Spatii industriale</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/patrimoniu/imobile/9\">Hoteluri si pensiuni</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/patrimoniu/imobile/10\">Benzinarii</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/patrimoniu/imobile/11\">Ferme</a></li>
                                        </ul>
                                    </li>
                                    <li id=\"menu-item-3641\" class=\"menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3641\">
                                        <a href=\"http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/\">Imobile in executare silita</a>
                                        <ul class=\"sub-menu\"><li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/executare_silita/imobileimobile/1\">Apartamente</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/executare_silita/imobileimobile/2\">Case/Vile</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/executare_silita/imobileimobile/3\">Case de vacanta/Cabane</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/executare_silita/imobileimobile/4\">Terenuri</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/executare_silita/imobileimobile/5\">Ansambluri rezidentiale</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/executare_silita/imobileimobile/6\">Spatii comerciale</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/executare_silita/imobileimobile/7\">Spatii de birouri</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/executare_silita/imobileimobile/8\">Spatii industriale</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/executare_silita/imobileimobile/9\">Hoteluri si pensiuni</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/executare_silita/imobileimobile/10\">Benzinarii</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/executari/executare_silita/imobileimobile/11\">Ferme</a></li>
                                        </ul>
                                    </li>
                                    <li id=\"menu-item-3641\" class=\"menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3641\">
                                        <a href=\"http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/\">Bunuri mobile recuperate</a>
                                        <ul class=\"sub-menu\">
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/genus/showAssets/mobile/1\">Autoturisme</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/genus/showAssets/mobile/2\">Autocamioane</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/genus/showAssets/mobile/3\">Autoutilitare</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/genus/showAssets/mobile/4\">Remarci/semiremarci</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/genus/showAssets/mobile/5\">Echipamente</a></li>
                                            <li class=\"menu-item menu-item-type-custom menu-item-object-custom\"><a href=\"/genus/showAssets/mobile/6\">Altele</a></li>
                                        </ul>
                                    </li>
                                    <li id=\"menu-item-3656\" class=\"menu-item menu-item-type-post_type menu-item-object-page menu-item-3656\"><a href=\"http://realhomes.inspirythemes.biz/contact-us/\">Contact </a></li>
                                </ul>
                                <select class=\"responsive-nav\">
                                    <option value=\"\" selected=\"\">Go to...</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/\"> Home</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/?module=revolution-slider\"> -  Home with Revolution Slider</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/?module=properties-map\"> -  Home with Google Map</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/?news-on-home=true\"> -  Home with News Posts</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/?module=slides-slider\"> -  Home with Custom Slider</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/?module=properties-slider\"> -  Home with Properties Slider</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/?module=banner\"> -  Home with Simple Banner</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/listing/\"> Listing</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/listing/\"> -  Simple Listing</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/listing/?module=properties-map\"> -  Simple Listing with Google Map</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/grid-listing/\"> -  Grid Listing</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/grid-listing/?module=properties-map\"> -  Grid Listing with Google Map</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/\"> Property</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/\"> -  Default – Variation</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/?variation=agent-in-sidebar\"> -  Agent in Sidebar – Variation</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/?slider-type=thumb-on-bottom\"> -  Gallery – Variation</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/news/\"> News</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/3-columns-gallery/\"> Gallery</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/2-columns-gallery/\"> -  2 Columns Gallery</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/3-columns-gallery/\"> -  3 Columns Gallery</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/4-columns-gallery/\"> -  4 Columns Gallery</option>
                                    <option value=\"#\"> Pages</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/agents/\"> -  Agents</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/property-status/for-rent/\"> -  For Rent</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/property-status/for-sale/\"> -  For Sale</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/property-city/miami/\"> -  Miami City</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/faqs-filterable/\"> -  FAQs</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/faqs-filterable/\"> -  -  FAQs – Filterable</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/faqs-toggle-style/\"> -  -  FAQs – Toggle Style</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/faqs-simple-list/\"> -  -  FAQs – Simple List</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/testimonials/\"> -  Testimonials</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/typography/\"> -  Typography</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/columns/\"> -  Columns</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/idx/\"> -  IDX using dsIDXpress Plugin</option>
                                    <option value=\"#\"> Types</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/property-type/single-family-home/\"> -  Single Family Home</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/property-type/apartment-building/\"> -  Apartment Building</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/property-type/apartment/\"> -  Apartment</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/property-type/office/\"> -  Office</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/property-type/shop/\"> -  Shop</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/property-type/villa/\"> -  Villa</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/property-type/condominium/\"> -  Condominium</option>
                                    <option value=\"http://realhomes.inspirythemes.biz/contact-us/\"> Contact us</option>
                                </select>
                            </div>
                        </nav>
                        <!-- End Main Menu -->
                    </div>
                </header>
            </div>
            <!-- End Header Container -->
        </div>
        <!-- End Header -->
        <div class=\"page-head\" style=\"background-repeat: no-repeat;background-position: center top;background-image: url(&#39;http://realhomes.inspirythemes.biz/wp-content/uploads/2015/08/banner.jpg&#39;); background-size: cover; \">
            <div class=\"container\">
                <div class=\"wrap clearfix\">
                    <h1 class=\"page-title\"><span>Properties Listing</span></h1>
                    <p>Properties Listing in Simple Layout</p>
                </div>
            </div>
        </div>
        <div style=\"width:200px\">
        <span class=\"selectwrap\">
\t\t   <select name=\"location\" id=\"location1\" class=\"search-select select2-hidden-accessible\" tabindex=\"-1\" aria-hidden=\"true\">
\t\t\t  <option value=\"any\" selected=\"selected\">Anyssssssssssssss</option>
\t\t\t  <option value=\"miami\">Miami</option>
\t\t\t  <option value=\"little-havana\">- Little Havana</option>
\t\t\t  <option value=\"perrine\">- Perrine</option>
\t\t\t  <option value=\"doral\">- Doral</option>
\t\t\t  <option value=\"hialeah\">- Hialeah</option>
\t\t   </select>
\t\t   <span class=\"select2 select2-container select2-container--default\" dir=\"ltr\"><span class=\"selection\"><span class=\"select2-selection select2-selection--single\" role=\"combobox\" aria-haspopup=\"true\" aria-expanded=\"false\" tabindex=\"0\" aria-labelledby=\"select2-location-container\"><span class=\"select2-selection__rendered\" id=\"select2-location-container\" title=\"Any\">Any</span><span class=\"select2-selection__arrow\" role=\"presentation\"><b role=\"presentation\"></b></span></span></span><span class=\"dropdown-wrapper\" aria-hidden=\"true\"></span></span>
        </span></div>
        <div class=\"popa\">
\t<ul>
\t\t<li>
\t\t<span class=\"selectwrap\">
\t\t   <select name=\"location\" id=\"location1\" class=\"search-select select2-hidden-accessible\" tabindex=\"-1\" aria-hidden=\"true\">
\t\t\t  <option value=\"any\" selected=\"selected\">Anyssssssssssssss</option>
\t\t\t  <option value=\"miami\">Miami</option>
\t\t\t  <option value=\"little-havana\">- Little Havana</option>
\t\t\t  <option value=\"perrine\">- Perrine</option>
\t\t\t  <option value=\"doral\">- Doral</option>
\t\t\t  <option value=\"hialeah\">- Hialeah</option>
\t\t   </select>
\t\t   <span class=\"select2 select2-container select2-container--default\" dir=\"ltr\"><span class=\"selection\"><span class=\"select2-selection select2-selection--single\" role=\"combobox\" aria-haspopup=\"true\" aria-expanded=\"false\" tabindex=\"0\" aria-labelledby=\"select2-location-container\"><span class=\"select2-selection__rendered\" id=\"select2-location-container\" title=\"Any\">Any</span><span class=\"select2-selection__arrow\" role=\"presentation\"><b role=\"presentation\"></b></span></span></span><span class=\"dropdown-wrapper\" aria-hidden=\"true\"></span></span>
\t\t</span>
\t\t</li>
\t\t\t";
        // line 529
        echo "\t</ul>
\t\t
\t\t
\t</div>
        <!-- End Page Head -->
        <!-- Content -->
        <div class=\"container contents listing-grid-layout\">
            <div class=\"row\">
                <div class=\"span9 main-wrap\">
                    <!-- Main Content -->
                    <div class=\"main\">
                        <section class=\"listing-layout\">
                            <h3 class=\"title-heading\">Listing</h3>
                            <div class=\"view-type clearfix\">
                                <a class=\"list active\" href=\"http://realhomes.inspirythemes.biz/listing/?view=list\">
                                    <svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" width=\"24px\" height=\"24px\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
                                        <rect class=\"boxes\" x=\"9\" y=\"3\" width=\"13\" height=\"4\"></rect>
                                        <rect class=\"boxes\" x=\"9\" y=\"10\" width=\"13\" height=\"4\"></rect>
                                        <rect class=\"boxes\" x=\"9\" y=\"17\" width=\"13\" height=\"4\"></rect>
                                        <rect class=\"boxes\" x=\"2\" y=\"3\" width=\"4\" height=\"4\"></rect>
                                        <rect class=\"boxes\" x=\"2\" y=\"10\" width=\"4\" height=\"4\"></rect>
                                        <rect class=\"boxes\" x=\"2\" y=\"17\" width=\"4\" height=\"4\"></rect>
                                        <path fill=\"none\" d=\"M0,0h24v24H0V0z\"></path>
                                    </svg>
                                </a>
                                <a class=\"grid \" href=\"http://realhomes.inspirythemes.biz/listing/?view=grid\">
                                    <svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" width=\"24px\" height=\"24px\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
                                        <rect class=\"boxes\" x=\"10\" y=\"3\" width=\"4\" height=\"4\"></rect>
                                        <rect class=\"boxes\" x=\"10\" y=\"10\" width=\"4\" height=\"4\"></rect>
                                        <rect class=\"boxes\" x=\"10\" y=\"17\" width=\"4\" height=\"4\"></rect>
                                        <rect class=\"boxes\" x=\"17\" y=\"3\" width=\"4\" height=\"4\"></rect>
                                        <rect class=\"boxes\" x=\"17\" y=\"10\" width=\"4\" height=\"4\"></rect>
                                        <rect class=\"boxes\" x=\"17\" y=\"17\" width=\"4\" height=\"4\"></rect>
                                        <rect class=\"boxes\" x=\"3\" y=\"3\" width=\"4\" height=\"4\"></rect>
                                        <rect class=\"boxes\" x=\"3\" y=\"10\" width=\"4\" height=\"4\"></rect>
                                        <rect class=\"boxes\" x=\"3\" y=\"17\" width=\"4\" height=\"4\"></rect>
                                        <path fill=\"none\" d=\"M0,0h24v24H0V0z\"></path>
                                    </svg>
                                </a>
                            </div>
                            <div class=\"list-container clearfix\">
                                <div class=\"sort-controls\">
                                    <strong>Sort By:</strong>
                                    &nbsp;
                                    <select name=\"sort-properties\" id=\"sort-properties\">
                                        <option value=\"default\">Default Order</option>
                                        <option value=\"price-asc\">Price Low to High</option>
                                        <option value=\"price-desc\">Price High to Low</option>
                                        <option value=\"date-asc\">Date Old to New</option>
                                        <option value=\"date-desc\">Date New to Old</option>
                                    </select>
                                </div>
                                <div class=\"property-item-wrapper\">
                                    ";
        // line 582
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")));
        foreach ($context['_seq'] as $context["_key"] => $context["imobil"]) {
            // line 583
            echo "                                    <article class=\"property-item clearfix\">
                                        <h4><a href=\"imobil/getDetails/";
            // line 584
            echo twig_escape_filter($this->env, $this->getAttribute($context["imobil"], "id", array()), "html", null, true);
            echo "\">, ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["imobil"], "oras", array()), "html", null, true);
            echo "</a></h4>
                                        <figure>
                                            <a href=\"http://realhomes.inspirythemes.biz/property/villa-in-hialeah-dade-county/\">
                                            <img width=\"244\" height=\"163\" src=\"";
            // line 587
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/property-08-244x163.jpg"), "html", null, true);
            echo "\" class=\"attachment-property-thumb-image size-property-thumb-image wp-post-image\" alt=\"property 08\">            </a>
                                        </figure>
                                        <div class=\"detail\">
                                            <h5 class=\"price\">
                                                ";
            // line 591
            echo twig_escape_filter($this->env, $this->getAttribute($context["imobil"], "pretInitial", array()), "html", null, true);
            echo "<small> - ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["imobil"], "tipImobil", array()), "html", null, true);
            echo "</small>            
                                            </h5>
                                            <p>";
            // line 593
            echo twig_escape_filter($this->env, $this->getAttribute($context["imobil"], "descriere", array()), "html", null, true);
            echo "</p>
                                            <a class=\"more-details\" href=\"imobil/getDetails/{imobil.id}\">Detalii <i class=\"fa fa-caret-right\"></i></a>
                                        </div>
                                        <div class=\"property-meta\">
                                            <span>
                                                <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Layer_1\" x=\"0px\" y=\"0px\" width=\"16px\" height=\"16px\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
                                                    <path class=\"path\" d=\"M14 7.001H2.999C1.342 7 0 8.3 0 10v11c0 1.7 1.3 3 3 3H14c1.656 0 3-1.342 3-3V10 C17 8.3 15.7 7 14 7.001z M14.998 21c0 0.551-0.447 1-0.998 1.002H2.999C2.448 22 2 21.6 2 21V10 c0.001-0.551 0.449-0.999 1-0.999H14c0.551 0 1 0.4 1 0.999V21z\"></path>
                                                    <path class=\"path\" d=\"M14.266 0.293c-0.395-0.391-1.034-0.391-1.429 0c-0.395 0.39-0.395 1 0 1.415L13.132 2H3.869l0.295-0.292 c0.395-0.391 0.395-1.025 0-1.415c-0.394-0.391-1.034-0.391-1.428 0L0 3l2.736 2.707c0.394 0.4 1 0.4 1.4 0 c0.395-0.391 0.395-1.023 0-1.414L3.869 4.001h9.263l-0.295 0.292c-0.395 0.392-0.395 1 0 1.414s1.034 0.4 1.4 0L17 3 L14.266 0.293z\"></path>
                                                    <path class=\"path\" d=\"M18.293 9.734c-0.391 0.395-0.391 1 0 1.429s1.023 0.4 1.4 0L20 10.868v9.263l-0.292-0.295 c-0.392-0.395-1.024-0.395-1.415 0s-0.391 1 0 1.428L21 24l2.707-2.736c0.391-0.394 0.391-1.033 0-1.428s-1.023-0.395-1.414 0 l-0.292 0.295v-9.263l0.292 0.295c0.392 0.4 1 0.4 1.4 0s0.391-1.034 0-1.429L21 7L18.293 9.734z\"></path>
                                                </svg>
                                                ";
            // line 603
            echo twig_escape_filter($this->env, $this->getAttribute($context["imobil"], "arieUtila", array()), "html", null, true);
            echo "m<sup style=\"font-size:0.7em;\">2</sup>
                                            </span>
                                            <span>
                                                <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Layer_1\" x=\"0px\" y=\"0px\" width=\"16px\" height=\"16px\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
                                                    <circle class=\"circle\" cx=\"5\" cy=\"8.3\" r=\"2.2\"></circle>
                                                    <path class=\"path\" d=\"M0 22.999C0 23.6 0.4 24 1 24S2 23.6 2 22.999V18H2h20h0.001v4.999c0 0.6 0.4 1 1 1 C23.552 24 24 23.6 24 22.999V10C24 9.4 23.6 9 23 9C22.447 9 22 9.4 22 10v1H22h-0.999V10.5 C20.999 8 20 6 17.5 6H11C9.769 6.1 8.2 6.3 8 8v3H2H2V9C2 8.4 1.6 8 1 8S0 8.4 0 9V22.999z M10.021 8.2 C10.19 8.1 10.6 8 11 8h5.5c1.382 0 2.496-0.214 2.5 2.501v0.499h-9L10.021 8.174z M22 16H2v-2.999h20V16z\"></path>
                                                </svg>
                                                4&nbsp;Bedrooms
                                            </span>
                                            <span>
                                                <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Layer_1\" x=\"0px\" y=\"0px\" width=\"16px\" height=\"16px\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
                                                    <path class=\"path\" d=\"M23.001 12h-1.513C21.805 11.6 22 11.1 22 10.5C22 9.1 20.9 8 19.5 8S17 9.1 17 10.5 c0 0.6 0.2 1.1 0.5 1.5H2.999c0-0.001 0-0.002 0-0.002V2.983V2.98c0.084-0.169-0.083-0.979 1-0.981h0.006 C4.008 2 4.3 2 4.5 2.104L4.292 2.292c-0.39 0.392-0.39 1 0 1.415c0.391 0.4 1 0.4 1.4 0l2-1.999 c0.39-0.391 0.39-1.025 0-1.415c-0.391-0.391-1.023-0.391-1.415 0L5.866 0.72C5.775 0.6 5.7 0.5 5.5 0.4 C4.776 0 4.1 0 4 0H3.984v0.001C1.195 0 1 2.7 1 2.98v0.019v0.032v8.967c0 0 0 0 0 0.002H0.999 C0.447 12 0 12.4 0 12.999S0.447 14 1 14H1v2.001c0.001 2.6 1.7 4.8 4 5.649V23c0 0.6 0.4 1 1 1s1-0.447 1-1v-1h10v1 c0 0.6 0.4 1 1 1s1-0.447 1-1v-1.102c2.745-0.533 3.996-3.222 4-5.897V14h0.001C23.554 14 24 13.6 24 13 S23.554 12 23 12z M21.001 16.001c-0.091 2.539-0.927 3.97-3.001 3.997H7c-2.208-0.004-3.996-1.79-4-3.997V14h15.173 c-0.379 0.484-0.813 0.934-1.174 1.003c-0.54 0.104-0.999 0.446-0.999 1c0 0.6 0.4 1 1 1 c2.159-0.188 3.188-2.006 3.639-2.999h0.363V16.001z\"></path>
                                                    <rect class=\"rect\" x=\"6.6\" y=\"4.1\" transform=\"matrix(-0.7071 0.7071 -0.7071 -0.7071 15.6319 3.2336)\" width=\"1\" height=\"1.4\"></rect>
                                                    <rect class=\"rect\" x=\"9.4\" y=\"2.4\" transform=\"matrix(0.7066 0.7076 -0.7076 0.7066 4.9969 -6.342)\" width=\"1.4\" height=\"1\"></rect>
                                                    <rect class=\"rect\" x=\"9.4\" y=\"6.4\" transform=\"matrix(0.7071 0.7071 -0.7071 0.7071 7.8179 -5.167)\" width=\"1.4\" height=\"1\"></rect>
                                                    <rect class=\"rect\" x=\"12.4\" y=\"4.4\" transform=\"matrix(0.7069 0.7073 -0.7073 0.7069 7.2858 -7.8754)\" width=\"1.4\" height=\"1\"></rect>
                                                    <rect class=\"rect\" x=\"13.4\" y=\"7.4\" transform=\"matrix(-0.7064 -0.7078 0.7078 -0.7064 18.5823 23.4137)\" width=\"1.4\" height=\"1\"></rect>
                                                </svg>
                                                3&nbsp;Bathrooms
                                            </span>
                                            <span>
                                                <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Layer_1\" x=\"0px\" y=\"0px\" width=\"16px\" height=\"16px\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
                                                    <path class=\"path\" d=\"M23.958 0.885c-0.175-0.64-0.835-1.016-1.475-0.842l-11 3.001c-0.64 0.173-1.016 0.833-0.842 1.5 c0.175 0.6 0.8 1 1.5 0.842L16 4.299V6.2h-0.001H13c-2.867 0-4.892 1.792-5.664 2.891L5.93 11.2H5.024 c-0.588-0.029-2.517-0.02-3.851 1.221C0.405 13.1 0 14.1 0 15.201V18.2v2H2h2.02C4.126 22.3 5.9 24 8 24 c2.136 0 3.873-1.688 3.979-3.801H16V24h2V3.754l5.116-1.396C23.756 2.2 24.1 1.5 24 0.885z M8 22 c-1.104 0-2-0.896-2-2.001s0.896-2 2-2S10 18.9 10 20S9.105 22 8 22.001z M11.553 18.2C10.891 16.9 9.6 16 8 16 c-1.556 0-2.892 0.901-3.553 2.201H2v-2.999c0-0.599 0.218-1.019 0.537-1.315C3.398 13.1 5 13.2 5 13.2h2L9 10.2 c0 0 1.407-1.999 4-1.999h2.999H16v10H11.553z\"></path>
                                                </svg>
                                                2&nbsp;Garages
                                            </span>
                                        </div>
                                    </article>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['imobil'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 632
        echo "                                </div>
                                        <h4><a href=\"http://realhomes.inspirythemes.biz/property/retail-store-southwest-186th-street/\">Retail Store – Southwest 186th Street</a></h4>
                                        <figure>
                                            <a href=\"http://realhomes.inspirythemes.biz/property/retail-store-southwest-186th-street/\">
                                            <img width=\"244\" height=\"163\" src=\"";
        // line 636
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/retail-store-244x163.jpg"), "html", null, true);
        echo "\" class=\"attachment-property-thumb-image size-property-thumb-image wp-post-image\" alt=\"\">            </a>
                                            <figcaption class=\"for-rent\">For Rent</figcaption>
                                        </figure>
                                        <div class=\"detail\">
                                            <h5 class=\"price\">
                                                \$4,200 Per Month<small> - Shop</small>            
                                            </h5>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad…</p>
                                            <a class=\"more-details\" href=\"http://realhomes.inspirythemes.biz/property/retail-store-southwest-186th-street/\">More Details <i class=\"fa fa-caret-right\"></i></a>
                                        </div>
                                        <div class=\"property-meta\">
                                            <span>
                                                <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Layer_1\" x=\"0px\" y=\"0px\" width=\"16px\" height=\"16px\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
                                                    <path class=\"path\" d=\"M14 7.001H2.999C1.342 7 0 8.3 0 10v11c0 1.7 1.3 3 3 3H14c1.656 0 3-1.342 3-3V10 C17 8.3 15.7 7 14 7.001z M14.998 21c0 0.551-0.447 1-0.998 1.002H2.999C2.448 22 2 21.6 2 21V10 c0.001-0.551 0.449-0.999 1-0.999H14c0.551 0 1 0.4 1 0.999V21z\"></path>
                                                    <path class=\"path\" d=\"M14.266 0.293c-0.395-0.391-1.034-0.391-1.429 0c-0.395 0.39-0.395 1 0 1.415L13.132 2H3.869l0.295-0.292 c0.395-0.391 0.395-1.025 0-1.415c-0.394-0.391-1.034-0.391-1.428 0L0 3l2.736 2.707c0.394 0.4 1 0.4 1.4 0 c0.395-0.391 0.395-1.023 0-1.414L3.869 4.001h9.263l-0.295 0.292c-0.395 0.392-0.395 1 0 1.414s1.034 0.4 1.4 0L17 3 L14.266 0.293z\"></path>
                                                    <path class=\"path\" d=\"M18.293 9.734c-0.391 0.395-0.391 1 0 1.429s1.023 0.4 1.4 0L20 10.868v9.263l-0.292-0.295 c-0.392-0.395-1.024-0.395-1.415 0s-0.391 1 0 1.428L21 24l2.707-2.736c0.391-0.394 0.391-1.033 0-1.428s-1.023-0.395-1.414 0 l-0.292 0.295v-9.263l0.292 0.295c0.392 0.4 1 0.4 1.4 0s0.391-1.034 0-1.429L21 7L18.293 9.734z\"></path>
                                                </svg>
                                                3400&nbsp;Sq Ft
                                            </span>
                                        </div>
                                    </article>
                                </div>
                            </div>
                            <div class=\"pagination\">
                                <a href=\"listing/imobil/page/1/?view=list\" class=\"real-btn current\">1</a> 
                                <a href=\"listing/imobil/page/2/?view=list\" class=\"real-btn\">2</a> 
                                <a href=\"listing/imobil/page/3/?view=list\" class=\"real-btn\">3</a> 
                            </div>
                        </section>
                    </div>
                    <!-- End Main Content -->
                </div>
                <!-- End span9 -->
                <div class=\"span3 sidebar-wrap\">
                    <!-- Sidebar -->
                    <aside class=\"sidebar\">
                        <section id=\"featured_properties_widget-10\" class=\"widget clearfix Featured_Properties_Widget\">
                            <h3 class=\"title\">Featured Properties</h3>
                            <ul class=\"featured-properties\">
                                <li>
                                    <figure>
                                        <a href=\"http://realhomes.inspirythemes.biz/property/florida-5-pinecrest-fl/\">
                                        <img width=\"246\" height=\"162\" src=\"";
        // line 678
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/property-09-246x162.jpg"), "html", null, true);
        echo "\" class=\"attachment-grid-view-image size-grid-view-image wp-post-image\" alt=\"property-09\">                            </a>
                                    </figure>
                                    <h4><a href=\"http://realhomes.inspirythemes.biz/property/florida-5-pinecrest-fl/\">Florida 5, Pinecrest, FL</a></h4>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing… <a href=\"http://realhomes.inspirythemes.biz/property/florida-5-pinecrest-fl/\">Read More</a></p>
                                    <span class=\"price\">\$480,000 </span>                    
                                </li>
                                <li>
                                    <figure>
                                        <a href=\"http://realhomes.inspirythemes.biz/property/villa-in-hialeah-dade-county/\">
                                        <img width=\"246\" height=\"162\" src=\"";
        // line 687
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/property-08-246x162.jpg"), "html", null, true);
        echo "\" class=\"attachment-grid-view-image size-grid-view-image wp-post-image\" alt=\"property 08\">                            </a>
                                    </figure>
                                    <h4><a href=\"http://realhomes.inspirythemes.biz/property/villa-in-hialeah-dade-county/\">Villa in Hialeah, Dade County</a></h4>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing… <a href=\"http://realhomes.inspirythemes.biz/property/villa-in-hialeah-dade-county/\">Read More</a></p>
                                    <span class=\"price\">\$7,500 Per Month</span>                    
                                </li>
                            </ul>
                        </section>
                    </aside>
                    <!-- End Sidebar -->
                </div>
            </div>
            <!-- End contents row -->
        </div>
        <!-- End Content -->
        <!-- Start Footer -->
        <footer id=\"footer-wrapper\">
            <div id=\"footer\" class=\"container\">
                <div class=\"row\">
                    <div class=\"span3\">
                        <section id=\"text-3\" class=\"widget clearfix widget_text\">
                            <h3 class=\"title\">About Real Homes</h3>
                            <div class=\"textwidget\">
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
                                <p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
                            </div>
                        </section>
                    </div>
                    <div class=\"span3\">
                        <section id=\"recent-posts-4\" class=\"widget clearfix widget_recent_entries\">
                            <h3 class=\"title\">Recent Posts</h3>
                            <ul>
                                <li>
                                    <a href=\"http://realhomes.inspirythemes.biz/lorem-post-with-image-format/\">Lorem Post With Image Format</a>
                                </li>
                                <li>
                                    <a href=\"http://realhomes.inspirythemes.biz/example-video-blog-post/\">Example Video Blog Post</a>
                                </li>
                                <li>
                                    <a href=\"http://realhomes.inspirythemes.biz/example-post-with-gallery-post-format/\">Example Post With Gallery Post Format</a>
                                </li>
                                <li>
                                    <a href=\"http://realhomes.inspirythemes.biz/example-post-with-image-post-format/\">Example Post With Image Post Format</a>
                                </li>
                                <li>
                                    <a href=\"http://realhomes.inspirythemes.biz/lorem-ipsum-dolor-sit-amet/\">Lorem Ipsum Dolor Sit Amet</a>
                                </li>
                            </ul>
                        </section>
                    </div>
                    <div class=\"clearfix visible-tablet\"></div>
                    <div class=\"span3\">
                        <section id=\"displaytweetswidget-2\" class=\"widget clearfix widget_displaytweetswidget\">
                            <h3 class=\"title\">Latest Tweets</h3>
                            <p>You should clean out your <a href=\"http://twitter.com/search?q=%23WordPress&amp;src=hash\" target=\"_blank\">#WordPress</a> Themes Directory.
                                <a href=\"https://t.co/bsDsl4qjJA\" target=\"_blank\">https://t.co/bsDsl4qjJA</a><br><small class=\"muted\">- Friday May 13 - 4:59am</small>
                            </p>
                            <p>Both of our <a href=\"http://twitter.com/search?q=%23RealEstate&amp;src=hash\" target=\"_blank\">#RealEstate</a> <a href=\"http://twitter.com/search?q=%23WordPRess&amp;src=hash\" target=\"_blank\">#WordPRess</a> Themes have the latest Visual Composer Plugin 4.11.2.1
                                <a href=\"https://t.co/j9ofmliCmS\" target=\"_blank\">https://t.co/j9ofmliCmS</a>
                                <a href=\"https://t.co/J3iLTWPJ0J\" target=\"_blank\">https://t.co/J3iLTWPJ0J</a><br><small class=\"muted\">- Thursday Apr 28 - 8:53am</small>
                            </p>
                        </section>
                    </div>
                    <div class=\"span3\">
                        <section id=\"text-2\" class=\"widget clearfix widget_text\">
                            <h3 class=\"title\">Contact Info</h3>
                            <div class=\"textwidget\">
                                <p>3015 Grand Ave, Coconut Grove,<br>
                                    Merrick Way, FL 12345
                                </p>
                                <p>Phone: 123-456-7890</p>
                                <p>Email: <a href=\"mailto:info@yourwebsite.com\">info@yourwebsite.com</a></p>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
            <!-- Footer Bottom -->
            <div id=\"footer-bottom\" class=\"container\">
                <div class=\"row\">
                    <div class=\"span6\">
                        <p class=\"copyright\">Copyright © 2015. All Rights Reserved.</p>
                    </div>
                    <div class=\"span6\">
                        <p class=\"designed-by\">A Theme by <a target=\"_blank\" href=\"http://themeforest.net/user/inspirythemes/portfolio\">Inspiry Themes</a></p>
                    </div>
                </div>
            </div>
            <!-- End Footer Bottom -->
        </footer>
        <!-- End Footer -->
        <!-- Login Modal -->
        <div id=\"login-modal\" class=\"forms-modal modal hide fade\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
                <p>You need to log in to use member only features.</p>
            </div>
            <!-- start of modal body -->
            <div class=\"modal-body\">
                <!-- login section -->
                <div class=\"login-section modal-section\">
                    <h4>Login</h4>
                    <form id=\"login-form\" class=\"login-form\" action=\"";
        // line 789
        echo $this->env->getExtension('routing')->getPath("fos_user_security_check");
        echo "\" method=\"post\" enctype=\"multipart/form-data\" novalidate=\"novalidate\">
                        <div class=\"form-option\">
                            <label for=\"username\">User Name<span>*</span></label>
                            <input id=\"username\" name=\"log\" type=\"text\" class=\"required\" title=\"* Provide user name!\" autofocus=\"\" required=\"\">
                        </div>
                        <div class=\"form-option\">
                            <label for=\"password\">Password<span>*</span></label>
                            <input id=\"password\" name=\"pwd\" type=\"password\" class=\"required\" title=\"* Provide password!\" required=\"\">
                        </div>
                        <input type=\"hidden\" name=\"action\" value=\"inspiry_ajax_login\">
                        <input type=\"hidden\" id=\"inspiry-secure-login\" name=\"inspiry-secure-login\" value=\"c64ee438df\"><input type=\"hidden\" name=\"_wp_http_referer\" value=\"/listing/\"><input type=\"hidden\" name=\"redirect_to\" value=\"http://realhomes.inspirythemes.biz/listing/\">                <input type=\"hidden\" name=\"user-cookie\" value=\"1\">
                        <input type=\"submit\" id=\"login-button\" name=\"submit\" value=\"Log in\" class=\"real-btn login-btn\">
                        <img id=\"login-loader\" class=\"modal-loader\" src=\"";
        // line 801
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/ajax-loader.gif"), "html", null, true);
        echo "\" alt=\"Working...\">
                        <div>
                            <div id=\"login-message\" class=\"modal-message\"></div>
                            <div id=\"login-error\" class=\"modal-error\"></div>
                        </div>
                    </form>
                    <p>
                        <a class=\"activate-section\" data-section=\"register-section\" href=\"#\">Register Here</a>
                        <span class=\"divider\">-</span>
                        <a class=\"activate-section\" data-section=\"forgot-section\" href=\"#\">Forgot Password</a>
                    </p>
                </div>
                <!-- forgot section -->
                <div class=\"forgot-section modal-section\">
                    <h4>Reset Password</h4>
                    <form action=\"";
        // line 816
        echo $this->env->getExtension('routing')->getPath("fos_user_security_check");
        echo "\" id=\"forgot-form\" method=\"post\" enctype=\"multipart/form-data\" novalidate=\"novalidate\">
                        <div class=\"form-option\">
                            <label for=\"reset_username_or_email\">Username or Email<span>*</span></label>
                            <input id=\"reset_username_or_email\" name=\"reset_username_or_email\" type=\"text\" class=\"required\" title=\"* Provide username or email!\" required=\"\">
                        </div>
                        <input type=\"hidden\" name=\"action\" value=\"inspiry_ajax_forgot\">
                        <input type=\"hidden\" name=\"user-cookie\" value=\"1\">
                        <input type=\"submit\" id=\"forgot-button\" name=\"user-submit\" value=\"Reset Password\" class=\"real-btn register-btn\">
                        <img id=\"forgot-loader\" class=\"modal-loader\" src=\"";
        // line 824
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/ajax-loader.gif"), "html", null, true);
        echo "\" alt=\"Working...\">
                        <input type=\"hidden\" id=\"inspiry-secure-reset\" name=\"inspiry-secure-reset\" value=\"cd7167add6\"><input type=\"hidden\" name=\"_wp_http_referer\" value=\"/listing/\">                
                        <div>
                            <div id=\"forgot-message\" class=\"modal-message\"></div>
                            <div id=\"forgot-error\" class=\"modal-error\"></div>
                        </div>
                    </form>
                    <p>
                        <a class=\"activate-section\" data-section=\"login-section\" href=\"#\">Login Here</a>
                        <span class=\"divider\">-</span>
                        <a class=\"activate-section\" data-section=\"register-section\" href=\"#\">Register Here</a>
                    </p>
                </div>
                <!-- register section -->
                <div class=\"register-section modal-section\">
                    <h4>Register</h4>
                    <form action=\"http://realhomes.inspirythemes.biz/wp-admin/admin-ajax.php\" id=\"register-form\" method=\"post\" enctype=\"multipart/form-data\" novalidate=\"novalidate\">
                        <div class=\"form-option\">
                            <label for=\"register_username\" class=\"\">User Name<span>*</span></label>
                            <input id=\"register_username\" name=\"register_username\" type=\"text\" class=\"required\" title=\"* Provide user name!\" required=\"\">
                        </div>
                        <div class=\"form-option\">
                            <label for=\"register_email\" class=\"\">Email<span>*</span></label>
                            <input id=\"register_email\" name=\"register_email\" type=\"text\" class=\"email required\" title=\"* Provide valid email address!\" required=\"\">
                        </div>
                        <input type=\"hidden\" name=\"user-cookie\" value=\"1\">
                        <input type=\"submit\" id=\"register-button\" name=\"user-submit\" value=\"Register\" class=\"real-btn register-btn\">
                        <img id=\"register-loader\" class=\"modal-loader\" src=\"";
        // line 851
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/ajax-loader.gif"), "html", null, true);
        echo "\" alt=\"Working...\">
                        <input type=\"hidden\" name=\"action\" value=\"inspiry_ajax_register\">
                        <input type=\"hidden\" id=\"inspiry-secure-register\" name=\"inspiry-secure-register\" value=\"2abe68f163\"><input type=\"hidden\" name=\"_wp_http_referer\" value=\"/listing/\"><input type=\"hidden\" name=\"redirect_to\" value=\"http://realhomes.inspirythemes.biz/listing/\">
                        <div>
                            <div id=\"register-message\" class=\"modal-message\"></div>
                            <div id=\"register-error\" class=\"modal-error\"></div>
                        </div>
                    </form>
                    <p>
                        <a class=\"activate-section\" data-section=\"login-section\" href=\"http://realhomes.inspirythemes.biz/listing/#\">Login Here</a>
                        <span class=\"divider\">-</span>
                        <a class=\"activate-section\" data-section=\"forgot-section\" href=\"http://realhomes.inspirythemes.biz/listing/#\">Forgot Password</a>
                    </p>
                </div>
            </div>
            <!-- end of modal-body -->
        </div>
        <a href=\"http://realhomes.inspirythemes.biz/listing/#top\" id=\"scroll-top\" style=\"display: block;\"><i class=\"fa fa-chevron-up\"></i></a>
        <script type=\"text/javascript\" src=\"";
        // line 869
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/core.min.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 870
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/widget.min.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 871
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/position.min.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 872
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/menu.min.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 873
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/wp-a11y.min.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\">
            /* <![CDATA[ */
            var uiAutocompleteL10n = {\"noResults\":\"No search results.\",\"oneResult\":\"1 result found. Use up and down arrow keys to navigate.\",\"manyResults\":\"%d results found. Use up and down arrow keys to navigate.\"};
            /* ]]> */
        </script>
        <script type=\"text/javascript\" src=\"";
        // line 879
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/autocomplete.min.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 880
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/comment-reply.min.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 881
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/inspiry-login-register.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\">
            /* <![CDATA[ */
            var localizedSearchParams = {\"rent_slug\":\"for-rent\"};
            var locationData = {\"any_text\":\"Any\",\"any_value\":\"any\",\"all_locations\":[{\"term_id\":27,\"name\":\"Miami\",\"slug\":\"miami\",\"parent\":0},{\"term_id\":41,\"name\":\"Little Havana\",\"slug\":\"little-havana\",\"parent\":27},{\"term_id\":30,\"name\":\"Perrine\",\"slug\":\"perrine\",\"parent\":27},{\"term_id\":40,\"name\":\"Doral\",\"slug\":\"doral\",\"parent\":27},{\"term_id\":48,\"name\":\"Hialeah\",\"slug\":\"hialeah\",\"parent\":27}],\"select_names\":[\"location\",\"child-location\",\"grandchild-location\",\"great-grandchild-location\"],\"select_count\":\"1\",\"locations_in_params\":[]};
            /* ]]> */
        </script>
        <script type=\"text/javascript\" src=\"";
        // line 888
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/inspiry-search-form.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\">
            /* <![CDATA[ */
            var localized = {\"nav_title\":\"Go to...\"};
            /* ]]> */
        </script>
        <script type=\"text/javascript\" src=\"";
        // line 894
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/custom.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 895
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/wp-embed.min.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 896
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/autocomplete.js"), "html", null, true);
        echo "\"></script>
        <div id=\"dsidx_cboxOverlay\" style=\"display: none;\"></div>
        <div id=\"dsidx_colorbox\" class=\"\" role=\"dialog\" tabindex=\"-1\" style=\"display: none;\">
            <div id=\"dsidx_cboxWrapper\">
                <div>
                    <div id=\"dsidx_cboxTopLeft\" style=\"float: left;\"></div>
                    <div id=\"dsidx_cboxTopCenter\" style=\"float: left;\"></div>
                    <div id=\"dsidx_cboxTopRight\" style=\"float: left;\"></div>
                </div>
                <div style=\"clear: left;\">
                    <div id=\"dsidx_cboxMiddleLeft\" style=\"float: left;\"></div>
                    <div id=\"dsidx_cboxContent\" style=\"float: left;\">
                        <div id=\"dsidx_cboxTitle\" style=\"float: left;\"></div>
                        <div id=\"dsidx_cboxCurrent\" style=\"float: left;\"></div>
                        <button type=\"button\" id=\"dsidx_cboxPrevious\"></button><button type=\"button\" id=\"dsidx_cboxNext\"></button><button id=\"dsidx_cboxSlideshow\"></button>
                        <div id=\"dsidx_cboxLoadingOverlay\" style=\"float: left;\"></div>
                        <div id=\"dsidx_cboxLoadingGraphic\" style=\"float: left;\"></div>
                    </div>
                    <div id=\"dsidx_cboxMiddleRight\" style=\"float: left;\"></div>
                </div>
                <div style=\"clear: left;\">
                    <div id=\"dsidx_cboxBottomLeft\" style=\"float: left;\"></div>
                    <div id=\"dsidx_cboxBottomCenter\" style=\"float: left;\"></div>
                    <div id=\"dsidx_cboxBottomRight\" style=\"float: left;\"></div>
                </div>
            </div>
            <div style=\"position: absolute; width: 9999px; visibility: hidden; display: none;\"></div>
        </div>
        <div id=\"wp-a11y-speak-polite\" role=\"status\" aria-live=\"polite\" aria-relevant=\"additions text\" aria-atomic=\"true\" class=\"screen-reader-text wp-a11y-speak-region\"></div>
        <div id=\"wp-a11y-speak-assertive\" role=\"alert\" aria-live=\"assertive\" aria-relevant=\"additions text\" aria-atomic=\"true\" class=\"screen-reader-text wp-a11y-speak-region\"></div>
    </body>
";
        
        $__internal_66ea3f0fe5437d7f33f69baffd0a0f06b14537af01e7cd93347dfa7b6f2df8b2->leave($__internal_66ea3f0fe5437d7f33f69baffd0a0f06b14537af01e7cd93347dfa7b6f2df8b2_prof);

    }

    public function getTemplateName()
    {
        return "executari/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1055 => 896,  1051 => 895,  1047 => 894,  1038 => 888,  1028 => 881,  1024 => 880,  1020 => 879,  1011 => 873,  1007 => 872,  1003 => 871,  999 => 870,  995 => 869,  974 => 851,  944 => 824,  933 => 816,  915 => 801,  900 => 789,  795 => 687,  783 => 678,  738 => 636,  732 => 632,  697 => 603,  684 => 593,  677 => 591,  670 => 587,  662 => 584,  659 => 583,  655 => 582,  600 => 529,  432 => 302,  207 => 79,  198 => 71,  194 => 70,  190 => 69,  186 => 68,  182 => 67,  178 => 66,  174 => 65,  170 => 64,  166 => 63,  162 => 62,  158 => 61,  154 => 60,  150 => 59,  146 => 58,  142 => 57,  138 => 56,  134 => 55,  130 => 54,  123 => 50,  119 => 49,  115 => 48,  111 => 47,  107 => 46,  103 => 45,  99 => 44,  95 => 43,  91 => 42,  87 => 41,  83 => 40,  79 => 39,  57 => 21,  51 => 15,  40 => 6,  34 => 5,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* {#{% block body %}#}*/
/*     {#{% extends 'base.html.twig' %}#}*/
/* */
/* {% block body %}*/
/* <!-- saved from url=(0043)http://realhomes.inspirythemes.biz/listing/ -->*/
/*     <!--<![endif]-->*/
/*     <head>*/
/*         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">*/
/*         <meta name="viewport" content="width=device-width, initial-scale=1.0">*/
/*         <link rel="profile" href="http://gmpg.org/xfn/11">*/
/*         <meta name="format-detection" content="telephone=no">*/
/*         <link rel="shortcut icon" href="http://realhomes.inspirythemes.biz/wp-content/uploads/2015/08/favicon.png">*/
/*         <link rel="pingback" href="http://realhomes.inspirythemes.biz/xmlrpc.php">*/
/*         <link rel="stylesheet" type="text/css" href="{{ asset("bundles/front/yoz.css")}}">*/
/*         <title><span>Listing – Real Homes</span></title>*/
/*         <link rel="alternate" type="application/rss+xml" title="Real Homes » Listing Comments Feed" href="http://realhomes.inspirythemes.biz/listing/feed/">*/
/*         {#<script async="" src="{{asset("{{asset("bundles/front/analytics.js")}}")></script><script type="text/javascript">*/
/*             window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/realhomes.inspirythemes.biz\/wp-includes\/js\/wp-emoji-release.min.js")}}?ver=4.5.2"}};*/
/*             !function(a,b,c){function d(a){var c,d,e,f=b.createElement("canvas"),g=f.getContext&&f.getContext("2d"),h=String.fromCharCode;if(!g||!g.fillText)return!1;switch(g.textBaseline="top",g.font="600 32px Arial",a){case"flag":return g.fillText(h(55356,56806,55356,56826),0,0),f.toDataURL().length>3e3;case"diversity":return g.fillText(h(55356,57221),0,0),c=g.getImageData(16,16,1,1).data,d=c[0]+","+c[1]+","+c[2]+","+c[3],g.fillText(h(55356,57221,55356,57343),0,0),c=g.getImageData(16,16,1,1).data,e=c[0]+","+c[1]+","+c[2]+","+c[3],d!==e;case"simple":return g.fillText(h(55357,56835),0,0),0!==g.getImageData(16,16,1,1).data[0];case"unicode8":return g.fillText(h(55356,57135),0,0),0!==g.getImageData(16,16,1,1).data[0]}return!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i;for(i=Array("simple","flag","unicode8","diversity"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);*/
/*         </script>#}<script src="{{asset("bundles/front/wp-emoji-release.min.js")}}" type="text/javascript"></script>*/
/*         <style type="text/css">*/
/*             img.wp-smiley,*/
/*             img.emoji {*/
/*             display: inline !important;*/
/*             border: none !important;*/
/*             box-shadow: none !important;*/
/*             height: 1em !important;*/
/*             width: 1em !important;*/
/*             margin: 0 .07em !important;*/
/*             vertical-align: -0.1em !important;*/
/*             background: none !important;*/
/*             padding: 0 !important;*/
/*             }*/
/*             .popa ul { list-style-type: none; overflow: hidden; width:auto; margin-left:66px;}*/
/* .popa ul li { float:left; width: 50px; }*/
/* .popa {width:500px;}*/
/*         </style>*/
/*         <link rel="stylesheet" id="dsidx-css" href="{{asset("bundles/front/0e43ba")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="dsidxpress-icons-css" href="{{asset("bundles/front/dsidx-icons.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="dsidxpress-unconditional-css" href="{{asset("bundles/front/client.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="dsidxwidgets-unconditional-css" href="{{asset("bundles/front/client(1).css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="paypal-ipnpublic-css" href="{{asset("bundles/front/paypal-ipn-for-wordpress-public.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="paypal-ipnpublicDataTablecss-css" href="{{asset("bundles/front/jquery.dataTables.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="paypal-ipnpublicDataTable-css" href="{{asset("bundles/front/dataTables.responsive.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="font-awesome-css" href="{{asset("bundles/front/font-awesome.min.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="quick-and-easy-faqs-css" href="{{asset("bundles/front/quick-and-easy-faqs-public.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="quick-and-easy-testimonials-css" href="{{asset("bundles/front/quick-and-easy-testimonials-public.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="lidd_mc-css" href="{{asset("bundles/front/style.css")}}" type="text/css" media="screen">*/
/*         <link rel="stylesheet" id="rs-plugin-settings-css" href="{{asset("bundles/front/settings.css")}}" type="text/css" media="all">*/
/*         <style id="rs-plugin-settings-inline-css" type="text/css">*/
/*             .tp-button.green.custom{font-size:16px;text-transform:uppercase;border-radius:0;box-shadow:none;text-shadow:none;padding:10px 15px; letter-spacing:1px;background:#ec894d}*/
/*         </style>*/
/*         <link rel="stylesheet" id="theme-roboto-css" href="{{asset("bundles/front/css")}} type="text/css" media="all">*/
/*         <link rel="stylesheet" id="theme-lato-css" href="{{asset("bundles/front/css(1)")}} type="text/css" media="all">*/
/*         <link rel="stylesheet" id="bootstrap-css-css" href="{{asset("bundles/front/bootstrap.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="responsive-css-css" href="{{asset("bundles/front/responsive.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="flexslider-css" href="{{asset("bundles/front/flexslider.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="pretty-photo-css-css" href="{{asset("bundles/front/prettyPhoto.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="swipebox-css" href="{{asset("bundles/front/swipebox.min.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="select2-css" href="{{asset("bundles/front/select2.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="main-css-css" href="{{asset("bundles/front/main.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="custom-responsive-css-css" href="{{asset("bundles/front/custom-responsive.css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="parent-default-css" href="{{asset("bundles/front/style(1).css")}}" type="text/css" media="all">*/
/*         <link rel="stylesheet" id="parent-custom-css" href="{{asset("bundles/front/custom.css")}}" type="text/css" media="all">*/
/*         <script type="text/javascript" src="{{asset("bundles/front/jquery.js")}}"></script>*/
/*         <script type="text/javascript" src="{{asset("bundles/front/jquery-migrate.min.js")}}"></script>*/
/*         <script type="text/javascript" src="{{asset("bundles/front/ed75b6")}}"></script>*/
/*         <script type="text/javascript" src="{{asset("bundles/front/paypal-ipn-for-wordpress.js")}}"></script>*/
/*         <script type="text/javascript" src="{{asset("bundles/front/jquery.dataTables.min.js")}}"></script>*/
/*         <script type="text/javascript" src="{{asset("bundles/front/dataTables.responsive.js")}}"></script>*/
/*         <link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://realhomes.inspirythemes.biz/xmlrpc.php?rsd">*/
/*         <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://realhomes.inspirythemes.biz/wp-includes/wlwmanifest.xml">*/
/*         <meta name="generator" content="WordPress 4.5.2">*/
/*         <link rel="canonical" href="http://realhomes.inspirythemes.biz/listing/">*/
/*         <link rel="shortlink" href="http://realhomes.inspirythemes.biz/?p=129">*/
/*         {#<link rel="alternate" type="application/json+oembed" href="http://realhomes.inspirythemes.biz/wp-json/oembed/1.0/embed?url=http%3A%2F%2Frealhomes.inspirythemes.biz%2Flisting%2F">*/
/*         <link rel="alternate" type="text/xml+oembed" href="http://realhomes.inspirythemes.biz/wp-json/oembed/1.0/embed?url=http%3A%2F%2Frealhomes.inspirythemes.biz%2Flisting%2F&amp;format=xml">*/
/*         #}<style type="text/css" id="dynamic-css">*/
/*             .header-wrapper, #currency-switcher #selected-currency, #currency-switcher-list li{*/
/*             background-color:#252A2B;*/
/*             }*/
/*             #logo h2 a{*/
/*             color:#ffffff;*/
/*             }*/
/*             #logo h2 a:hover, #logo h2 a:focus, #logo h2 a:active{*/
/*             color:#4dc7ec;*/
/*             }*/
/*             .tag-line span{*/
/*             color:#8b9293;*/
/*             }*/
/*             .tag-line span{*/
/*             background-color:#343a3b;*/
/*             }*/
/*             .page-head h1.page-title span{*/
/*             color:#394041;*/
/*             }*/
/*             .page-head h1.page-title span{*/
/*             background-color:#f5f4f3;*/
/*             }*/
/*             .page-head p{*/
/*             color:#ffffff;*/
/*             }*/
/*             .page-head p{*/
/*             background-color:#37B3D9;*/
/*             }*/
/*             .header-wrapper, #contact-email, #contact-email a, .user-nav a, .social_networks li a, #currency-switcher #selected-currency, #currency-switcher-list li{*/
/*             color:#929A9B;*/
/*             }*/
/*             #contact-email a:hover, .user-nav a:hover{*/
/*             color:#b0b8b9;*/
/*             }*/
/*             #header-top, .social_networks li a, .user-nav a, .header-wrapper .social_networks, #currency-switcher #selected-currency, #currency-switcher-list li{*/
/*             border-color:#343A3B;*/
/*             }*/
/*             .main-menu ul li a{*/
/*             color:#afb4b5;*/
/*             }*/
/*             .main-menu ul li.current-menu-ancestor > a, .main-menu ul li.current-menu-parent > a, .main-menu ul li.current-menu-item > a, .main-menu ul li.current_page_item > a, .main-menu ul li:hover > a, .main-menu ul li ul, .main-menu ul li ul li ul{*/
/*             background-color:#ec894d;*/
/*             }*/
/*             .main-menu ul li.current-menu-ancestor > a, .main-menu ul li.current-menu-parent > a, .main-menu ul li.current-menu-item > a, .main-menu ul li.current_page_item > a, .main-menu ul li:hover > a, .main-menu ul li ul, .main-menu ul li ul li a, .main-menu ul li ul li ul, .main-menu ul li ul li ul li a{*/
/*             color:#ffffff;*/
/*             }*/
/*             .main-menu ul li ul li:hover > a, .main-menu ul li ul li ul li:hover > a{*/
/*             background-color:#dc7d44;*/
/*             }*/
/*             .slide-description h3, .slide-description h3 a{*/
/*             color:#394041;*/
/*             }*/
/*             .slide-description h3 a:hover, .slide-description h3 a:focus, .slide-description h3 a:active{*/
/*             color:#df5400;*/
/*             }*/
/*             .slide-description p{*/
/*             color:#8b9293;*/
/*             }*/
/*             .slide-description span{*/
/*             color:#df5400;*/
/*             }*/
/*             .slide-description .know-more{*/
/*             color:#ffffff;*/
/*             }*/
/*             .slide-description .know-more{*/
/*             background-color:#37b3d9;*/
/*             }*/
/*             .slide-description .know-more:hover{*/
/*             background-color:#2aa6cc;*/
/*             }*/
/*             .property-item{*/
/*             background-color:#ffffff;*/
/*             }*/
/*             .property-item, .property-item .property-meta, .property-item .property-meta span{*/
/*             border-color:#dedede;*/
/*             }*/
/*             .property-item h4, .property-item h4 a, .es-carousel-wrapper ul li h4 a{*/
/*             color:#394041;*/
/*             }*/
/*             .property-item h4 a:hover, .property-item h4 a:focus, .property-item h4 a:active, .es-carousel-wrapper ul li h4 a:hover, .es-carousel-wrapper ul li h4 a:focus, .es-carousel-wrapper ul li h4 a:active{*/
/*             color:#df5400;*/
/*             }*/
/*             .property-item .price, .es-carousel-wrapper ul li .price, .property-item .price small{*/
/*             color:#ffffff;*/
/*             }*/
/*             .property-item .price, .es-carousel-wrapper ul li .price{*/
/*             background-color:#4dc7ec;*/
/*             }*/
/*             .property-item figure figcaption{*/
/*             color:#ffffff;*/
/*             }*/
/*             .property-item figure figcaption{*/
/*             background-color:#ec894d;*/
/*             }*/
/*             .property-item p, .es-carousel-wrapper ul li p{*/
/*             color:#8b9293;*/
/*             }*/
/*             .more-details, .es-carousel-wrapper ul li p a{*/
/*             color:#394041;*/
/*             }*/
/*             .more-details:hover, .more-details:focus, .more-details:active, .es-carousel-wrapper ul li p a:hover, .es-carousel-wrapper ul li p a:focus, .es-carousel-wrapper ul li p a:active{*/
/*             color:#df5400;*/
/*             }*/
/*             .property-item .property-meta span{*/
/*             color:#394041;*/
/*             }*/
/*             .property-item .property-meta{*/
/*             background-color:#f5f5f5;*/
/*             }*/
/*             #footer .widget .title{*/
/*             color:#394041;*/
/*             }*/
/*             #footer .widget .textwidget, #footer .widget, #footer-bottom p{*/
/*             color:#8b9293;*/
/*             }*/
/*             #footer .widget ul li a, #footer .widget a, #footer-bottom a{*/
/*             color:#75797A;*/
/*             }*/
/*             #footer .widget ul li a:hover, #footer .widget ul li a:focus, #footer.widget ul li a:active, #footer .widget a:hover, #footer .widget a:focus, #footer .widget a:active, #footer-bottom a:hover, #footer-bottom a:focus, #footer-bottom a:active{*/
/*             color:#dc7d44;*/
/*             }*/
/*             #footer-bottom{*/
/*             border-color:#dedede;*/
/*             }*/
/*             .real-btn{*/
/*             color:#ffffff;*/
/*             }*/
/*             .real-btn{*/
/*             background-color:#ec894d;*/
/*             }*/
/*             .real-btn:hover, .real-btn.current{*/
/*             color:#ffffff;*/
/*             }*/
/*             .real-btn:hover, .real-btn.current{*/
/*             background-color:#e3712c;*/
/*             }*/
/*             @media (min-width: 980px) {*/
/*             .contact-number, .contact-number .outer-strip{*/
/*             background-color:#4dc7ec;*/
/*             }*/
/*             .contact-number{*/
/*             color:#e7eff7;*/
/*             }*/
/*             .contact-number .fa-phone{*/
/*             background-color:#37b3d9;*/
/*             }*/
/*             }*/
/*         </style>*/
/*         <script type="text/javascript">*/
/*             var RecaptchaOptions = {*/
/*             	theme : 'custom', custom_theme_widget : 'recaptcha_widget'*/
/*             };*/
/*         </script>*/
/*         <!--[if lt IE 9]>*/
/*         <script src="http://html5shim.googlecode.com/svn/trunk/html5.js")}}"></script>*/
/*         <![endif]-->*/
/*         <meta name="generator" content="Powered by Slider Revolution 5.0.4.1 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface.">*/
/*         <!-- BEGIN GADWP v4.9.3.1 Universal Tracking - https://deconf.com/google-analytics-dashboard-wordpress/ -->*/
/*         <script>*/
/*             (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){*/
/*             (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),*/
/*             m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)*/
/*             })(window,document,'script','//www.google-analytics.com/analytics.js")}}','ga');*/
/*             ga('create', 'UA-31893023-8', 'auto');*/
/*             ga('send', 'pageview');*/
/*         </script>*/
/*         <!-- END GADWP Universal Tracking -->*/
/*         <script type="text/javascript">*/
/*             if (typeof localdsidx == "undefined" || !localdsidx) { var localdsidx = {}; };*/
/*             localdsidx.pluginUrl = "/wp-content/plugins/dsidxpress/";*/
/*             localdsidx.homeUrl = "http://realhomes.inspirythemes.biz";*/
/*         </script>				<script type="text/javascript">*/
/*             if (typeof localdsidx == "undefined" || !localdsidx) { var localdsidx = {}; };*/
/*             localdsidx.pluginUrl = "/wp-content/plugins/dsidxpress/";*/
/*             localdsidx.homeUrl = "http://realhomes.inspirythemes.biz";*/
/*         </script>*/
/*     </head>*/
/*     <body class="page page-id-129 page-template page-template-template-property-listing page-template-template-property-listing-php">*/
/*         <!-- Start Header -->*/
/*         <div class="header-wrapper">*/
/*             <div class="container">*/
/*                 <!-- Start Header Container -->*/
/*                 <header id="header" class="clearfix">*/
/*                     <div id="header-top" class="clearfix">*/
/*                         <h2 id="contact-email">*/
/*                             <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">*/
/*                                 <path class="path" d="M8.174 15.926l-6.799 5.438c-0.431 0.346-0.501 0.975-0.156 1.406s0.974 0.5 1.4 0.156l7.211-5.769L8.174 15.926z"></path>*/
/*                                 <path class="path" d="M15.838 15.936l-1.685 1.214l7.222 5.777c0.433 0.3 1.1 0.3 1.406-0.156c0.345-0.432 0.274-1.061-0.157-1.406 L15.838 15.936z"></path>*/
/*                                 <polygon class="path" points="1,10.2 1.6,10.9 12,2.6 22,10.6 22,22 2,22 2,10.2 1,10.2 1.6,10.9 1,10.2 0,10.2 0,24 24,24 24,9.7 12,0 0,9.7 0,10.2 1,10.2 1,10.2"></polygon>*/
/*                                 <polygon class="path" points="23.6,11.7 12.6,19.7 11.4,19.7 0.4,11.7 0.4,11.7 0.4,11.7 1.6,10.1 12,17.6 22.4,10.1"></polygon>*/
/*                             </svg>*/
/*                             <span id="anda">Email us at :</span>*/
/*                             <a href="mailto:sales@yourwebsite.com">sales@yourwebsite.com</a>*/
/*                         </h2>*/
/*                         <!-- Social Navigation -->*/
/*                         <ul class="social_networks clearfix">*/
/*                             <li class="facebook">*/
/*                                 <a target="_blank" href="https://www.facebook.com/InspiryThemes"><i class="fa fa-facebook fa-lg"></i></a>*/
/*                             </li>*/
/*                             <li class="twitter">*/
/*                                 <a target="_blank" href="https://twitter.com/InspiryThemes"><i class="fa fa-twitter fa-lg"></i></a>*/
/*                             </li>*/
/*                             <li class="linkedin">*/
/*                                 <a target="_blank" href="http://realhomes.inspirythemes.biz/listing/#"><i class="fa fa-linkedin fa-lg"></i></a>*/
/*                             </li>*/
/*                             <li class="gplus">*/
/*                                 <a target="_blank" href="http://realhomes.inspirythemes.biz/listing/#"><i class="fa fa-google-plus fa-lg"></i></a>*/
/*                             </li>*/
/*                             <li class="rss">*/
/*                                 <a target="_blank" href="http://realhomes.inspirythemes.biz/feed/?post_type=property"> <i class="fa fa-rss fa-lg"></i></a>*/
/*                             </li>*/
/*                         </ul>*/
/*                         <!-- User Navigation -->*/
/*                         <div class="user-nav clearfix">*/
/*                             <a href="http://realhomes.inspirythemes.biz/favorites/">*/
/*                             <i class="fa fa-star"></i>Favorites			</a>*/
/*                             <a class="last" href="#" data-toggle="modal"><i class="fa fa-sign-in"></i>*/
/*                               Login / Register</a>	*/
/*                         </div>*/
/*                     </div>*/
/*                     <!-- Logo -->*/
/*                     <div id="logo">*/
/*                         <a title="Real Homes" href="http://realhomes.inspirythemes.biz/">*/
/*                         <img src="{{asset("bundles/front/logo.png")}}" alt="Real Homes">*/
/*                         </a>*/
/*                         <h2 class="logo-heading only-for-print">*/
/*                             <a href="http://realhomes.inspirythemes.biz/" title="Real Homes">*/
/*                             Real Homes                                </a>*/
/*                         </h2>*/
/*                         <div class="tag-line"><span>No. 1 Real Estate Theme</span></div>*/
/*                     </div>*/
/*                     <div class="menu-and-contact-wrap">*/
/*                         <h2 class="contact-number "><i class="fa fa-phone"></i><span class="desktop-version">1-800-555-1234</span><a class="mobile-version" href="tel://1-800-555-1234" title="Make a Call">1-800-555-1234</a><span class="outer-strip" style="right: -27.5px; width: 27.5px;"></span></h2>*/
/*                         <!-- Start Main Menu-->*/
/*                         <nav class="main-menu">*/
/*                             <div class="menu-main-menu-container">*/
/*                                 <ul id="menu-main-menu" class="clearfix">*/
/*                                     <li id="menu-item-3646" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3646">*/
/*                                         <a href="http://realhomes.inspirythemes.biz/">Imobile in Proprietatea bancii</a>*/
/*                                         <ul class="sub-menu">*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/banca//imobile//1">Apartamente</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/banca/imobile/2">Case/Vile</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/banca/imobile/3">Case de vacanta/Cabane</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/banca/imobile/4">Terenuri</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/banca/imobile/5">Ansambluri rezidentiale</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/banca/imobile/6">Spatii comerciale</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/banca/imobile/7">Spatii de birouri</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/banca/imobile/8">Spatii industriale</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/banca/imobile/9">Hoteluri si pensiuni</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/banca/imobile/10">Benzinarii</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/banca/imobile/11">Ferme</a></li>*/
/*                                         </ul>*/
/*                                     </li>*/
/*                                     <li id="menu-item-3654" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-129 current_page_item current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children menu-item-3654">*/
/*                                         <a href="http://realhomes.inspirythemes.biz/listing/"><span style="font-size:13px">Imobile in lichidare patrimoniala</span></a>*/
/*                                         <ul class="sub-menu" style="display: none;">*/
/*                                             */
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/patrimoniu/imobile/1">Apartamente</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/patrimoniu/imobile/2">Case/Vile</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/patrimoniu/imobile/3">Case de vacanta/Cabane</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/patrimoniu/imobile/4">Terenuri</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/patrimoniu/imobile/5">Ansambluri rezidentiale</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/patrimoniu/imobile/6">Spatii comerciale</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/patrimoniu/imobile/7">Spatii de birouri</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/patrimoniu/imobile/8">Spatii industriale</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/patrimoniu/imobile/9">Hoteluri si pensiuni</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/patrimoniu/imobile/10">Benzinarii</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/patrimoniu/imobile/11">Ferme</a></li>*/
/*                                         </ul>*/
/*                                     </li>*/
/*                                     <li id="menu-item-3641" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3641">*/
/*                                         <a href="http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/">Imobile in executare silita</a>*/
/*                                         <ul class="sub-menu"><li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/executare_silita/imobileimobile/1">Apartamente</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/executare_silita/imobileimobile/2">Case/Vile</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/executare_silita/imobileimobile/3">Case de vacanta/Cabane</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/executare_silita/imobileimobile/4">Terenuri</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/executare_silita/imobileimobile/5">Ansambluri rezidentiale</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/executare_silita/imobileimobile/6">Spatii comerciale</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/executare_silita/imobileimobile/7">Spatii de birouri</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/executare_silita/imobileimobile/8">Spatii industriale</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/executare_silita/imobileimobile/9">Hoteluri si pensiuni</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/executare_silita/imobileimobile/10">Benzinarii</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/executari/executare_silita/imobileimobile/11">Ferme</a></li>*/
/*                                         </ul>*/
/*                                     </li>*/
/*                                     <li id="menu-item-3641" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3641">*/
/*                                         <a href="http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/">Bunuri mobile recuperate</a>*/
/*                                         <ul class="sub-menu">*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/genus/showAssets/mobile/1">Autoturisme</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/genus/showAssets/mobile/2">Autocamioane</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/genus/showAssets/mobile/3">Autoutilitare</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/genus/showAssets/mobile/4">Remarci/semiremarci</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/genus/showAssets/mobile/5">Echipamente</a></li>*/
/*                                             <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/genus/showAssets/mobile/6">Altele</a></li>*/
/*                                         </ul>*/
/*                                     </li>*/
/*                                     <li id="menu-item-3656" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3656"><a href="http://realhomes.inspirythemes.biz/contact-us/">Contact </a></li>*/
/*                                 </ul>*/
/*                                 <select class="responsive-nav">*/
/*                                     <option value="" selected="">Go to...</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/"> Home</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/?module=revolution-slider"> -  Home with Revolution Slider</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/?module=properties-map"> -  Home with Google Map</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/?news-on-home=true"> -  Home with News Posts</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/?module=slides-slider"> -  Home with Custom Slider</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/?module=properties-slider"> -  Home with Properties Slider</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/?module=banner"> -  Home with Simple Banner</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/listing/"> Listing</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/listing/"> -  Simple Listing</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/listing/?module=properties-map"> -  Simple Listing with Google Map</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/grid-listing/"> -  Grid Listing</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/grid-listing/?module=properties-map"> -  Grid Listing with Google Map</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/"> Property</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/"> -  Default – Variation</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/?variation=agent-in-sidebar"> -  Agent in Sidebar – Variation</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/property/15421-southwest-39th-terrace/?slider-type=thumb-on-bottom"> -  Gallery – Variation</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/news/"> News</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/3-columns-gallery/"> Gallery</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/2-columns-gallery/"> -  2 Columns Gallery</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/3-columns-gallery/"> -  3 Columns Gallery</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/4-columns-gallery/"> -  4 Columns Gallery</option>*/
/*                                     <option value="#"> Pages</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/agents/"> -  Agents</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/property-status/for-rent/"> -  For Rent</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/property-status/for-sale/"> -  For Sale</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/property-city/miami/"> -  Miami City</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/faqs-filterable/"> -  FAQs</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/faqs-filterable/"> -  -  FAQs – Filterable</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/faqs-toggle-style/"> -  -  FAQs – Toggle Style</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/faqs-simple-list/"> -  -  FAQs – Simple List</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/testimonials/"> -  Testimonials</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/typography/"> -  Typography</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/columns/"> -  Columns</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/idx/"> -  IDX using dsIDXpress Plugin</option>*/
/*                                     <option value="#"> Types</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/property-type/single-family-home/"> -  Single Family Home</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/property-type/apartment-building/"> -  Apartment Building</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/property-type/apartment/"> -  Apartment</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/property-type/office/"> -  Office</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/property-type/shop/"> -  Shop</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/property-type/villa/"> -  Villa</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/property-type/condominium/"> -  Condominium</option>*/
/*                                     <option value="http://realhomes.inspirythemes.biz/contact-us/"> Contact us</option>*/
/*                                 </select>*/
/*                             </div>*/
/*                         </nav>*/
/*                         <!-- End Main Menu -->*/
/*                     </div>*/
/*                 </header>*/
/*             </div>*/
/*             <!-- End Header Container -->*/
/*         </div>*/
/*         <!-- End Header -->*/
/*         <div class="page-head" style="background-repeat: no-repeat;background-position: center top;background-image: url(&#39;http://realhomes.inspirythemes.biz/wp-content/uploads/2015/08/banner.jpg&#39;); background-size: cover; ">*/
/*             <div class="container">*/
/*                 <div class="wrap clearfix">*/
/*                     <h1 class="page-title"><span>Properties Listing</span></h1>*/
/*                     <p>Properties Listing in Simple Layout</p>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         <div style="width:200px">*/
/*         <span class="selectwrap">*/
/* 		   <select name="location" id="location1" class="search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">*/
/* 			  <option value="any" selected="selected">Anyssssssssssssss</option>*/
/* 			  <option value="miami">Miami</option>*/
/* 			  <option value="little-havana">- Little Havana</option>*/
/* 			  <option value="perrine">- Perrine</option>*/
/* 			  <option value="doral">- Doral</option>*/
/* 			  <option value="hialeah">- Hialeah</option>*/
/* 		   </select>*/
/* 		   <span class="select2 select2-container select2-container--default" dir="ltr"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-location-container"><span class="select2-selection__rendered" id="select2-location-container" title="Any">Any</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>*/
/*         </span></div>*/
/*         <div class="popa">*/
/* 	<ul>*/
/* 		<li>*/
/* 		<span class="selectwrap">*/
/* 		   <select name="location" id="location1" class="search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">*/
/* 			  <option value="any" selected="selected">Anyssssssssssssss</option>*/
/* 			  <option value="miami">Miami</option>*/
/* 			  <option value="little-havana">- Little Havana</option>*/
/* 			  <option value="perrine">- Perrine</option>*/
/* 			  <option value="doral">- Doral</option>*/
/* 			  <option value="hialeah">- Hialeah</option>*/
/* 		   </select>*/
/* 		   <span class="select2 select2-container select2-container--default" dir="ltr"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-location-container"><span class="select2-selection__rendered" id="select2-location-container" title="Any">Any</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>*/
/* 		</span>*/
/* 		</li>*/
/* 			{#<li>*/
/* 		<span class="selectwrap">*/
/* 		   <select name="location" id="location2" class="search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">*/
/* 			  <option value="any" selected="selected">Any</option>*/
/* 			  <option value="miami">Miami</option>*/
/* 			  <option value="little-havana">- Little Havana</option>*/
/* 			  <option value="perrine">- Perrine</option>*/
/* 			  <option value="doral">- Doral</option>*/
/* 			  <option value="hialeah">- Hialeah</option>*/
/* 		   </select>*/
/* 		   <span class="select2 select2-container select2-container--default" dir="ltr"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-location-container"><span class="select2-selection__rendered" id="select2-location-container" title="Any">Any</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>*/
/* 		</span>*/
/* 		</li>*/
/* 		<li>*/
/* 		<span class="selectwrap">*/
/* 		   <select name="location" id="location3" class="search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">*/
/* 			  <option value="any" selected="selected">Any</option>*/
/* 			  <option value="miami">Miami</option>*/
/* 			  <option value="little-havana">- Little Havana</option>*/
/* 			  <option value="perrine">- Perrine</option>*/
/* 			  <option value="doral">- Doral</option>*/
/* 			  <option value="hialeah">- Hialeah</option>*/
/* 		   </select>*/
/* 		   <span class="select2 select2-container select2-container--default" dir="ltr"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-location-container"><span class="select2-selection__rendered" id="select2-location-container" title="Any">Any</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>*/
/* 		</span>*/
/* 		</li>*/
/*                 		<li>*/
/* 		<span class="selectwrap">*/
/* 		   <select name="location" id="location3" class="search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">*/
/* 			  <option value="any" selected="selected">Any</option>*/
/* 			  <option value="miami">Miami</option>*/
/* 			  <option value="little-havana">- Little Havana</option>*/
/* 			  <option value="perrine">- Perrine</option>*/
/* 			  <option value="doral">- Doral</option>*/
/* 			  <option value="hialeah">- Hialeah</option>*/
/* 		   </select>*/
/* 		   <span class="select2 select2-container select2-container--default" dir="ltr"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-location-container"><span class="select2-selection__rendered" id="select2-location-container" title="Any">Any</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>*/
/* 		</span>*/
/* 		</li>*/
/*                 		<li>*/
/* 		<span class="selectwrap">*/
/* 		   <select name="location" id="location3" class="search-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">*/
/* 			  <option value="any" selected="selected">Any</option>*/
/* 			  <option value="miami">Miami</option>*/
/* 			  <option value="little-havana">- Little Havana</option>*/
/* 			  <option value="perrine">- Perrine</option>*/
/* 			  <option value="doral">- Doral</option>*/
/* 			  <option value="hialeah">- Hialeah</option>*/
/* 		   </select>*/
/* 		   <span class="select2 select2-container select2-container--default" dir="ltr"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-location-container"><span class="select2-selection__rendered" id="select2-location-container" title="Any">Any</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>*/
/* 		</span>*/
/* 		</li>*/
/* 		<li>*/
/* 			<span class="selectwrap">*/
/* 				<select name="judet4" id="judet4" class="search-select">*/
/* 					<option selected="selected">Pret</option>*/
/* 				</select>*/
/* 			</span>*/
/* 		</li>*/
/* 		<li>*/
/* 			<input type="text" name="property-id" id="property-id-txt" value="" placeholder="Cautare libera">*/
/* 		</li>#}*/
/* 	</ul>*/
/* 		*/
/* 		*/
/* 	</div>*/
/*         <!-- End Page Head -->*/
/*         <!-- Content -->*/
/*         <div class="container contents listing-grid-layout">*/
/*             <div class="row">*/
/*                 <div class="span9 main-wrap">*/
/*                     <!-- Main Content -->*/
/*                     <div class="main">*/
/*                         <section class="listing-layout">*/
/*                             <h3 class="title-heading">Listing</h3>*/
/*                             <div class="view-type clearfix">*/
/*                                 <a class="list active" href="http://realhomes.inspirythemes.biz/listing/?view=list">*/
/*                                     <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">*/
/*                                         <rect class="boxes" x="9" y="3" width="13" height="4"></rect>*/
/*                                         <rect class="boxes" x="9" y="10" width="13" height="4"></rect>*/
/*                                         <rect class="boxes" x="9" y="17" width="13" height="4"></rect>*/
/*                                         <rect class="boxes" x="2" y="3" width="4" height="4"></rect>*/
/*                                         <rect class="boxes" x="2" y="10" width="4" height="4"></rect>*/
/*                                         <rect class="boxes" x="2" y="17" width="4" height="4"></rect>*/
/*                                         <path fill="none" d="M0,0h24v24H0V0z"></path>*/
/*                                     </svg>*/
/*                                 </a>*/
/*                                 <a class="grid " href="http://realhomes.inspirythemes.biz/listing/?view=grid">*/
/*                                     <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">*/
/*                                         <rect class="boxes" x="10" y="3" width="4" height="4"></rect>*/
/*                                         <rect class="boxes" x="10" y="10" width="4" height="4"></rect>*/
/*                                         <rect class="boxes" x="10" y="17" width="4" height="4"></rect>*/
/*                                         <rect class="boxes" x="17" y="3" width="4" height="4"></rect>*/
/*                                         <rect class="boxes" x="17" y="10" width="4" height="4"></rect>*/
/*                                         <rect class="boxes" x="17" y="17" width="4" height="4"></rect>*/
/*                                         <rect class="boxes" x="3" y="3" width="4" height="4"></rect>*/
/*                                         <rect class="boxes" x="3" y="10" width="4" height="4"></rect>*/
/*                                         <rect class="boxes" x="3" y="17" width="4" height="4"></rect>*/
/*                                         <path fill="none" d="M0,0h24v24H0V0z"></path>*/
/*                                     </svg>*/
/*                                 </a>*/
/*                             </div>*/
/*                             <div class="list-container clearfix">*/
/*                                 <div class="sort-controls">*/
/*                                     <strong>Sort By:</strong>*/
/*                                     &nbsp;*/
/*                                     <select name="sort-properties" id="sort-properties">*/
/*                                         <option value="default">Default Order</option>*/
/*                                         <option value="price-asc">Price Low to High</option>*/
/*                                         <option value="price-desc">Price High to Low</option>*/
/*                                         <option value="date-asc">Date Old to New</option>*/
/*                                         <option value="date-desc">Date New to Old</option>*/
/*                                     </select>*/
/*                                 </div>*/
/*                                 <div class="property-item-wrapper">*/
/*                                     {% for imobil in pagination %}*/
/*                                     <article class="property-item clearfix">*/
/*                                         <h4><a href="imobil/getDetails/{{imobil.id}}">, {{imobil.oras}}</a></h4>*/
/*                                         <figure>*/
/*                                             <a href="http://realhomes.inspirythemes.biz/property/villa-in-hialeah-dade-county/">*/
/*                                             <img width="244" height="163" src="{{asset("bundles/front/property-08-244x163.jpg")}}" class="attachment-property-thumb-image size-property-thumb-image wp-post-image" alt="property 08">            </a>*/
/*                                         </figure>*/
/*                                         <div class="detail">*/
/*                                             <h5 class="price">*/
/*                                                 {{imobil.pretInitial}}<small> - {{imobil.tipImobil}}</small>            */
/*                                             </h5>*/
/*                                             <p>{{imobil.descriere}}</p>*/
/*                                             <a class="more-details" href="imobil/getDetails/{imobil.id}">Detalii <i class="fa fa-caret-right"></i></a>*/
/*                                         </div>*/
/*                                         <div class="property-meta">*/
/*                                             <span>*/
/*                                                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">*/
/*                                                     <path class="path" d="M14 7.001H2.999C1.342 7 0 8.3 0 10v11c0 1.7 1.3 3 3 3H14c1.656 0 3-1.342 3-3V10 C17 8.3 15.7 7 14 7.001z M14.998 21c0 0.551-0.447 1-0.998 1.002H2.999C2.448 22 2 21.6 2 21V10 c0.001-0.551 0.449-0.999 1-0.999H14c0.551 0 1 0.4 1 0.999V21z"></path>*/
/*                                                     <path class="path" d="M14.266 0.293c-0.395-0.391-1.034-0.391-1.429 0c-0.395 0.39-0.395 1 0 1.415L13.132 2H3.869l0.295-0.292 c0.395-0.391 0.395-1.025 0-1.415c-0.394-0.391-1.034-0.391-1.428 0L0 3l2.736 2.707c0.394 0.4 1 0.4 1.4 0 c0.395-0.391 0.395-1.023 0-1.414L3.869 4.001h9.263l-0.295 0.292c-0.395 0.392-0.395 1 0 1.414s1.034 0.4 1.4 0L17 3 L14.266 0.293z"></path>*/
/*                                                     <path class="path" d="M18.293 9.734c-0.391 0.395-0.391 1 0 1.429s1.023 0.4 1.4 0L20 10.868v9.263l-0.292-0.295 c-0.392-0.395-1.024-0.395-1.415 0s-0.391 1 0 1.428L21 24l2.707-2.736c0.391-0.394 0.391-1.033 0-1.428s-1.023-0.395-1.414 0 l-0.292 0.295v-9.263l0.292 0.295c0.392 0.4 1 0.4 1.4 0s0.391-1.034 0-1.429L21 7L18.293 9.734z"></path>*/
/*                                                 </svg>*/
/*                                                 {{imobil.arieUtila}}m<sup style="font-size:0.7em;">2</sup>*/
/*                                             </span>*/
/*                                             <span>*/
/*                                                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">*/
/*                                                     <circle class="circle" cx="5" cy="8.3" r="2.2"></circle>*/
/*                                                     <path class="path" d="M0 22.999C0 23.6 0.4 24 1 24S2 23.6 2 22.999V18H2h20h0.001v4.999c0 0.6 0.4 1 1 1 C23.552 24 24 23.6 24 22.999V10C24 9.4 23.6 9 23 9C22.447 9 22 9.4 22 10v1H22h-0.999V10.5 C20.999 8 20 6 17.5 6H11C9.769 6.1 8.2 6.3 8 8v3H2H2V9C2 8.4 1.6 8 1 8S0 8.4 0 9V22.999z M10.021 8.2 C10.19 8.1 10.6 8 11 8h5.5c1.382 0 2.496-0.214 2.5 2.501v0.499h-9L10.021 8.174z M22 16H2v-2.999h20V16z"></path>*/
/*                                                 </svg>*/
/*                                                 4&nbsp;Bedrooms*/
/*                                             </span>*/
/*                                             <span>*/
/*                                                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">*/
/*                                                     <path class="path" d="M23.001 12h-1.513C21.805 11.6 22 11.1 22 10.5C22 9.1 20.9 8 19.5 8S17 9.1 17 10.5 c0 0.6 0.2 1.1 0.5 1.5H2.999c0-0.001 0-0.002 0-0.002V2.983V2.98c0.084-0.169-0.083-0.979 1-0.981h0.006 C4.008 2 4.3 2 4.5 2.104L4.292 2.292c-0.39 0.392-0.39 1 0 1.415c0.391 0.4 1 0.4 1.4 0l2-1.999 c0.39-0.391 0.39-1.025 0-1.415c-0.391-0.391-1.023-0.391-1.415 0L5.866 0.72C5.775 0.6 5.7 0.5 5.5 0.4 C4.776 0 4.1 0 4 0H3.984v0.001C1.195 0 1 2.7 1 2.98v0.019v0.032v8.967c0 0 0 0 0 0.002H0.999 C0.447 12 0 12.4 0 12.999S0.447 14 1 14H1v2.001c0.001 2.6 1.7 4.8 4 5.649V23c0 0.6 0.4 1 1 1s1-0.447 1-1v-1h10v1 c0 0.6 0.4 1 1 1s1-0.447 1-1v-1.102c2.745-0.533 3.996-3.222 4-5.897V14h0.001C23.554 14 24 13.6 24 13 S23.554 12 23 12z M21.001 16.001c-0.091 2.539-0.927 3.97-3.001 3.997H7c-2.208-0.004-3.996-1.79-4-3.997V14h15.173 c-0.379 0.484-0.813 0.934-1.174 1.003c-0.54 0.104-0.999 0.446-0.999 1c0 0.6 0.4 1 1 1 c2.159-0.188 3.188-2.006 3.639-2.999h0.363V16.001z"></path>*/
/*                                                     <rect class="rect" x="6.6" y="4.1" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 15.6319 3.2336)" width="1" height="1.4"></rect>*/
/*                                                     <rect class="rect" x="9.4" y="2.4" transform="matrix(0.7066 0.7076 -0.7076 0.7066 4.9969 -6.342)" width="1.4" height="1"></rect>*/
/*                                                     <rect class="rect" x="9.4" y="6.4" transform="matrix(0.7071 0.7071 -0.7071 0.7071 7.8179 -5.167)" width="1.4" height="1"></rect>*/
/*                                                     <rect class="rect" x="12.4" y="4.4" transform="matrix(0.7069 0.7073 -0.7073 0.7069 7.2858 -7.8754)" width="1.4" height="1"></rect>*/
/*                                                     <rect class="rect" x="13.4" y="7.4" transform="matrix(-0.7064 -0.7078 0.7078 -0.7064 18.5823 23.4137)" width="1.4" height="1"></rect>*/
/*                                                 </svg>*/
/*                                                 3&nbsp;Bathrooms*/
/*                                             </span>*/
/*                                             <span>*/
/*                                                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">*/
/*                                                     <path class="path" d="M23.958 0.885c-0.175-0.64-0.835-1.016-1.475-0.842l-11 3.001c-0.64 0.173-1.016 0.833-0.842 1.5 c0.175 0.6 0.8 1 1.5 0.842L16 4.299V6.2h-0.001H13c-2.867 0-4.892 1.792-5.664 2.891L5.93 11.2H5.024 c-0.588-0.029-2.517-0.02-3.851 1.221C0.405 13.1 0 14.1 0 15.201V18.2v2H2h2.02C4.126 22.3 5.9 24 8 24 c2.136 0 3.873-1.688 3.979-3.801H16V24h2V3.754l5.116-1.396C23.756 2.2 24.1 1.5 24 0.885z M8 22 c-1.104 0-2-0.896-2-2.001s0.896-2 2-2S10 18.9 10 20S9.105 22 8 22.001z M11.553 18.2C10.891 16.9 9.6 16 8 16 c-1.556 0-2.892 0.901-3.553 2.201H2v-2.999c0-0.599 0.218-1.019 0.537-1.315C3.398 13.1 5 13.2 5 13.2h2L9 10.2 c0 0 1.407-1.999 4-1.999h2.999H16v10H11.553z"></path>*/
/*                                                 </svg>*/
/*                                                 2&nbsp;Garages*/
/*                                             </span>*/
/*                                         </div>*/
/*                                     </article>*/
/*                                     {% endfor %}*/
/*                                 </div>*/
/*                                         <h4><a href="http://realhomes.inspirythemes.biz/property/retail-store-southwest-186th-street/">Retail Store – Southwest 186th Street</a></h4>*/
/*                                         <figure>*/
/*                                             <a href="http://realhomes.inspirythemes.biz/property/retail-store-southwest-186th-street/">*/
/*                                             <img width="244" height="163" src="{{asset("bundles/front/retail-store-244x163.jpg")}}" class="attachment-property-thumb-image size-property-thumb-image wp-post-image" alt="">            </a>*/
/*                                             <figcaption class="for-rent">For Rent</figcaption>*/
/*                                         </figure>*/
/*                                         <div class="detail">*/
/*                                             <h5 class="price">*/
/*                                                 $4,200 Per Month<small> - Shop</small>            */
/*                                             </h5>*/
/*                                             <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad…</p>*/
/*                                             <a class="more-details" href="http://realhomes.inspirythemes.biz/property/retail-store-southwest-186th-street/">More Details <i class="fa fa-caret-right"></i></a>*/
/*                                         </div>*/
/*                                         <div class="property-meta">*/
/*                                             <span>*/
/*                                                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">*/
/*                                                     <path class="path" d="M14 7.001H2.999C1.342 7 0 8.3 0 10v11c0 1.7 1.3 3 3 3H14c1.656 0 3-1.342 3-3V10 C17 8.3 15.7 7 14 7.001z M14.998 21c0 0.551-0.447 1-0.998 1.002H2.999C2.448 22 2 21.6 2 21V10 c0.001-0.551 0.449-0.999 1-0.999H14c0.551 0 1 0.4 1 0.999V21z"></path>*/
/*                                                     <path class="path" d="M14.266 0.293c-0.395-0.391-1.034-0.391-1.429 0c-0.395 0.39-0.395 1 0 1.415L13.132 2H3.869l0.295-0.292 c0.395-0.391 0.395-1.025 0-1.415c-0.394-0.391-1.034-0.391-1.428 0L0 3l2.736 2.707c0.394 0.4 1 0.4 1.4 0 c0.395-0.391 0.395-1.023 0-1.414L3.869 4.001h9.263l-0.295 0.292c-0.395 0.392-0.395 1 0 1.414s1.034 0.4 1.4 0L17 3 L14.266 0.293z"></path>*/
/*                                                     <path class="path" d="M18.293 9.734c-0.391 0.395-0.391 1 0 1.429s1.023 0.4 1.4 0L20 10.868v9.263l-0.292-0.295 c-0.392-0.395-1.024-0.395-1.415 0s-0.391 1 0 1.428L21 24l2.707-2.736c0.391-0.394 0.391-1.033 0-1.428s-1.023-0.395-1.414 0 l-0.292 0.295v-9.263l0.292 0.295c0.392 0.4 1 0.4 1.4 0s0.391-1.034 0-1.429L21 7L18.293 9.734z"></path>*/
/*                                                 </svg>*/
/*                                                 3400&nbsp;Sq Ft*/
/*                                             </span>*/
/*                                         </div>*/
/*                                     </article>*/
/*                                 </div>*/
/*                             </div>*/
/*                             <div class="pagination">*/
/*                                 <a href="listing/imobil/page/1/?view=list" class="real-btn current">1</a> */
/*                                 <a href="listing/imobil/page/2/?view=list" class="real-btn">2</a> */
/*                                 <a href="listing/imobil/page/3/?view=list" class="real-btn">3</a> */
/*                             </div>*/
/*                         </section>*/
/*                     </div>*/
/*                     <!-- End Main Content -->*/
/*                 </div>*/
/*                 <!-- End span9 -->*/
/*                 <div class="span3 sidebar-wrap">*/
/*                     <!-- Sidebar -->*/
/*                     <aside class="sidebar">*/
/*                         <section id="featured_properties_widget-10" class="widget clearfix Featured_Properties_Widget">*/
/*                             <h3 class="title">Featured Properties</h3>*/
/*                             <ul class="featured-properties">*/
/*                                 <li>*/
/*                                     <figure>*/
/*                                         <a href="http://realhomes.inspirythemes.biz/property/florida-5-pinecrest-fl/">*/
/*                                         <img width="246" height="162" src="{{asset("bundles/front/property-09-246x162.jpg")}}" class="attachment-grid-view-image size-grid-view-image wp-post-image" alt="property-09">                            </a>*/
/*                                     </figure>*/
/*                                     <h4><a href="http://realhomes.inspirythemes.biz/property/florida-5-pinecrest-fl/">Florida 5, Pinecrest, FL</a></h4>*/
/*                                     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing… <a href="http://realhomes.inspirythemes.biz/property/florida-5-pinecrest-fl/">Read More</a></p>*/
/*                                     <span class="price">$480,000 </span>                    */
/*                                 </li>*/
/*                                 <li>*/
/*                                     <figure>*/
/*                                         <a href="http://realhomes.inspirythemes.biz/property/villa-in-hialeah-dade-county/">*/
/*                                         <img width="246" height="162" src="{{asset("bundles/front/property-08-246x162.jpg")}}" class="attachment-grid-view-image size-grid-view-image wp-post-image" alt="property 08">                            </a>*/
/*                                     </figure>*/
/*                                     <h4><a href="http://realhomes.inspirythemes.biz/property/villa-in-hialeah-dade-county/">Villa in Hialeah, Dade County</a></h4>*/
/*                                     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing… <a href="http://realhomes.inspirythemes.biz/property/villa-in-hialeah-dade-county/">Read More</a></p>*/
/*                                     <span class="price">$7,500 Per Month</span>                    */
/*                                 </li>*/
/*                             </ul>*/
/*                         </section>*/
/*                     </aside>*/
/*                     <!-- End Sidebar -->*/
/*                 </div>*/
/*             </div>*/
/*             <!-- End contents row -->*/
/*         </div>*/
/*         <!-- End Content -->*/
/*         <!-- Start Footer -->*/
/*         <footer id="footer-wrapper">*/
/*             <div id="footer" class="container">*/
/*                 <div class="row">*/
/*                     <div class="span3">*/
/*                         <section id="text-3" class="widget clearfix widget_text">*/
/*                             <h3 class="title">About Real Homes</h3>*/
/*                             <div class="textwidget">*/
/*                                 <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>*/
/*                                 <p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>*/
/*                             </div>*/
/*                         </section>*/
/*                     </div>*/
/*                     <div class="span3">*/
/*                         <section id="recent-posts-4" class="widget clearfix widget_recent_entries">*/
/*                             <h3 class="title">Recent Posts</h3>*/
/*                             <ul>*/
/*                                 <li>*/
/*                                     <a href="http://realhomes.inspirythemes.biz/lorem-post-with-image-format/">Lorem Post With Image Format</a>*/
/*                                 </li>*/
/*                                 <li>*/
/*                                     <a href="http://realhomes.inspirythemes.biz/example-video-blog-post/">Example Video Blog Post</a>*/
/*                                 </li>*/
/*                                 <li>*/
/*                                     <a href="http://realhomes.inspirythemes.biz/example-post-with-gallery-post-format/">Example Post With Gallery Post Format</a>*/
/*                                 </li>*/
/*                                 <li>*/
/*                                     <a href="http://realhomes.inspirythemes.biz/example-post-with-image-post-format/">Example Post With Image Post Format</a>*/
/*                                 </li>*/
/*                                 <li>*/
/*                                     <a href="http://realhomes.inspirythemes.biz/lorem-ipsum-dolor-sit-amet/">Lorem Ipsum Dolor Sit Amet</a>*/
/*                                 </li>*/
/*                             </ul>*/
/*                         </section>*/
/*                     </div>*/
/*                     <div class="clearfix visible-tablet"></div>*/
/*                     <div class="span3">*/
/*                         <section id="displaytweetswidget-2" class="widget clearfix widget_displaytweetswidget">*/
/*                             <h3 class="title">Latest Tweets</h3>*/
/*                             <p>You should clean out your <a href="http://twitter.com/search?q=%23WordPress&amp;src=hash" target="_blank">#WordPress</a> Themes Directory.*/
/*                                 <a href="https://t.co/bsDsl4qjJA" target="_blank">https://t.co/bsDsl4qjJA</a><br><small class="muted">- Friday May 13 - 4:59am</small>*/
/*                             </p>*/
/*                             <p>Both of our <a href="http://twitter.com/search?q=%23RealEstate&amp;src=hash" target="_blank">#RealEstate</a> <a href="http://twitter.com/search?q=%23WordPRess&amp;src=hash" target="_blank">#WordPRess</a> Themes have the latest Visual Composer Plugin 4.11.2.1*/
/*                                 <a href="https://t.co/j9ofmliCmS" target="_blank">https://t.co/j9ofmliCmS</a>*/
/*                                 <a href="https://t.co/J3iLTWPJ0J" target="_blank">https://t.co/J3iLTWPJ0J</a><br><small class="muted">- Thursday Apr 28 - 8:53am</small>*/
/*                             </p>*/
/*                         </section>*/
/*                     </div>*/
/*                     <div class="span3">*/
/*                         <section id="text-2" class="widget clearfix widget_text">*/
/*                             <h3 class="title">Contact Info</h3>*/
/*                             <div class="textwidget">*/
/*                                 <p>3015 Grand Ave, Coconut Grove,<br>*/
/*                                     Merrick Way, FL 12345*/
/*                                 </p>*/
/*                                 <p>Phone: 123-456-7890</p>*/
/*                                 <p>Email: <a href="mailto:info@yourwebsite.com">info@yourwebsite.com</a></p>*/
/*                             </div>*/
/*                         </section>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <!-- Footer Bottom -->*/
/*             <div id="footer-bottom" class="container">*/
/*                 <div class="row">*/
/*                     <div class="span6">*/
/*                         <p class="copyright">Copyright © 2015. All Rights Reserved.</p>*/
/*                     </div>*/
/*                     <div class="span6">*/
/*                         <p class="designed-by">A Theme by <a target="_blank" href="http://themeforest.net/user/inspirythemes/portfolio">Inspiry Themes</a></p>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <!-- End Footer Bottom -->*/
/*         </footer>*/
/*         <!-- End Footer -->*/
/*         <!-- Login Modal -->*/
/*         <div id="login-modal" class="forms-modal modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">*/
/*             <div class="modal-header">*/
/*                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>*/
/*                 <p>You need to log in to use member only features.</p>*/
/*             </div>*/
/*             <!-- start of modal body -->*/
/*             <div class="modal-body">*/
/*                 <!-- login section -->*/
/*                 <div class="login-section modal-section">*/
/*                     <h4>Login</h4>*/
/*                     <form id="login-form" class="login-form" action="{{ path("fos_user_security_check") }}" method="post" enctype="multipart/form-data" novalidate="novalidate">*/
/*                         <div class="form-option">*/
/*                             <label for="username">User Name<span>*</span></label>*/
/*                             <input id="username" name="log" type="text" class="required" title="* Provide user name!" autofocus="" required="">*/
/*                         </div>*/
/*                         <div class="form-option">*/
/*                             <label for="password">Password<span>*</span></label>*/
/*                             <input id="password" name="pwd" type="password" class="required" title="* Provide password!" required="">*/
/*                         </div>*/
/*                         <input type="hidden" name="action" value="inspiry_ajax_login">*/
/*                         <input type="hidden" id="inspiry-secure-login" name="inspiry-secure-login" value="c64ee438df"><input type="hidden" name="_wp_http_referer" value="/listing/"><input type="hidden" name="redirect_to" value="http://realhomes.inspirythemes.biz/listing/">                <input type="hidden" name="user-cookie" value="1">*/
/*                         <input type="submit" id="login-button" name="submit" value="Log in" class="real-btn login-btn">*/
/*                         <img id="login-loader" class="modal-loader" src="{{asset("bundles/front/ajax-loader.gif")}}" alt="Working...">*/
/*                         <div>*/
/*                             <div id="login-message" class="modal-message"></div>*/
/*                             <div id="login-error" class="modal-error"></div>*/
/*                         </div>*/
/*                     </form>*/
/*                     <p>*/
/*                         <a class="activate-section" data-section="register-section" href="#">Register Here</a>*/
/*                         <span class="divider">-</span>*/
/*                         <a class="activate-section" data-section="forgot-section" href="#">Forgot Password</a>*/
/*                     </p>*/
/*                 </div>*/
/*                 <!-- forgot section -->*/
/*                 <div class="forgot-section modal-section">*/
/*                     <h4>Reset Password</h4>*/
/*                     <form action="{{ path("fos_user_security_check") }}" id="forgot-form" method="post" enctype="multipart/form-data" novalidate="novalidate">*/
/*                         <div class="form-option">*/
/*                             <label for="reset_username_or_email">Username or Email<span>*</span></label>*/
/*                             <input id="reset_username_or_email" name="reset_username_or_email" type="text" class="required" title="* Provide username or email!" required="">*/
/*                         </div>*/
/*                         <input type="hidden" name="action" value="inspiry_ajax_forgot">*/
/*                         <input type="hidden" name="user-cookie" value="1">*/
/*                         <input type="submit" id="forgot-button" name="user-submit" value="Reset Password" class="real-btn register-btn">*/
/*                         <img id="forgot-loader" class="modal-loader" src="{{asset("bundles/front/ajax-loader.gif")}}" alt="Working...">*/
/*                         <input type="hidden" id="inspiry-secure-reset" name="inspiry-secure-reset" value="cd7167add6"><input type="hidden" name="_wp_http_referer" value="/listing/">                */
/*                         <div>*/
/*                             <div id="forgot-message" class="modal-message"></div>*/
/*                             <div id="forgot-error" class="modal-error"></div>*/
/*                         </div>*/
/*                     </form>*/
/*                     <p>*/
/*                         <a class="activate-section" data-section="login-section" href="#">Login Here</a>*/
/*                         <span class="divider">-</span>*/
/*                         <a class="activate-section" data-section="register-section" href="#">Register Here</a>*/
/*                     </p>*/
/*                 </div>*/
/*                 <!-- register section -->*/
/*                 <div class="register-section modal-section">*/
/*                     <h4>Register</h4>*/
/*                     <form action="http://realhomes.inspirythemes.biz/wp-admin/admin-ajax.php" id="register-form" method="post" enctype="multipart/form-data" novalidate="novalidate">*/
/*                         <div class="form-option">*/
/*                             <label for="register_username" class="">User Name<span>*</span></label>*/
/*                             <input id="register_username" name="register_username" type="text" class="required" title="* Provide user name!" required="">*/
/*                         </div>*/
/*                         <div class="form-option">*/
/*                             <label for="register_email" class="">Email<span>*</span></label>*/
/*                             <input id="register_email" name="register_email" type="text" class="email required" title="* Provide valid email address!" required="">*/
/*                         </div>*/
/*                         <input type="hidden" name="user-cookie" value="1">*/
/*                         <input type="submit" id="register-button" name="user-submit" value="Register" class="real-btn register-btn">*/
/*                         <img id="register-loader" class="modal-loader" src="{{asset("bundles/front/ajax-loader.gif")}}" alt="Working...">*/
/*                         <input type="hidden" name="action" value="inspiry_ajax_register">*/
/*                         <input type="hidden" id="inspiry-secure-register" name="inspiry-secure-register" value="2abe68f163"><input type="hidden" name="_wp_http_referer" value="/listing/"><input type="hidden" name="redirect_to" value="http://realhomes.inspirythemes.biz/listing/">*/
/*                         <div>*/
/*                             <div id="register-message" class="modal-message"></div>*/
/*                             <div id="register-error" class="modal-error"></div>*/
/*                         </div>*/
/*                     </form>*/
/*                     <p>*/
/*                         <a class="activate-section" data-section="login-section" href="http://realhomes.inspirythemes.biz/listing/#">Login Here</a>*/
/*                         <span class="divider">-</span>*/
/*                         <a class="activate-section" data-section="forgot-section" href="http://realhomes.inspirythemes.biz/listing/#">Forgot Password</a>*/
/*                     </p>*/
/*                 </div>*/
/*             </div>*/
/*             <!-- end of modal-body -->*/
/*         </div>*/
/*         <a href="http://realhomes.inspirythemes.biz/listing/#top" id="scroll-top" style="display: block;"><i class="fa fa-chevron-up"></i></a>*/
/*         <script type="text/javascript" src="{{asset("bundles/front/core.min.js")}}"></script>*/
/*         <script type="text/javascript" src="{{asset("bundles/front/widget.min.js")}}"></script>*/
/*         <script type="text/javascript" src="{{asset("bundles/front/position.min.js")}}"></script>*/
/*         <script type="text/javascript" src="{{asset("bundles/front/menu.min.js")}}"></script>*/
/*         <script type="text/javascript" src="{{asset("bundles/front/wp-a11y.min.js")}}"></script>*/
/*         <script type="text/javascript">*/
/*             /* <![CDATA[ *//* */
/*             var uiAutocompleteL10n = {"noResults":"No search results.","oneResult":"1 result found. Use up and down arrow keys to navigate.","manyResults":"%d results found. Use up and down arrow keys to navigate."};*/
/*             /* ]]> *//* */
/*         </script>*/
/*         <script type="text/javascript" src="{{asset("bundles/front/autocomplete.min.js")}}"></script>*/
/*         <script type="text/javascript" src="{{asset("bundles/front/comment-reply.min.js")}}"></script>*/
/*         <script type="text/javascript" src="{{asset("bundles/front/inspiry-login-register.js")}}"></script>*/
/*         <script type="text/javascript">*/
/*             /* <![CDATA[ *//* */
/*             var localizedSearchParams = {"rent_slug":"for-rent"};*/
/*             var locationData = {"any_text":"Any","any_value":"any","all_locations":[{"term_id":27,"name":"Miami","slug":"miami","parent":0},{"term_id":41,"name":"Little Havana","slug":"little-havana","parent":27},{"term_id":30,"name":"Perrine","slug":"perrine","parent":27},{"term_id":40,"name":"Doral","slug":"doral","parent":27},{"term_id":48,"name":"Hialeah","slug":"hialeah","parent":27}],"select_names":["location","child-location","grandchild-location","great-grandchild-location"],"select_count":"1","locations_in_params":[]};*/
/*             /* ]]> *//* */
/*         </script>*/
/*         <script type="text/javascript" src="{{asset("bundles/front/inspiry-search-form.js")}}"></script>*/
/*         <script type="text/javascript">*/
/*             /* <![CDATA[ *//* */
/*             var localized = {"nav_title":"Go to..."};*/
/*             /* ]]> *//* */
/*         </script>*/
/*         <script type="text/javascript" src="{{asset("bundles/front/custom.js")}}"></script>*/
/*         <script type="text/javascript" src="{{asset("bundles/front/wp-embed.min.js")}}"></script>*/
/*         <script type="text/javascript" src="{{asset("bundles/front/autocomplete.js")}}"></script>*/
/*         <div id="dsidx_cboxOverlay" style="display: none;"></div>*/
/*         <div id="dsidx_colorbox" class="" role="dialog" tabindex="-1" style="display: none;">*/
/*             <div id="dsidx_cboxWrapper">*/
/*                 <div>*/
/*                     <div id="dsidx_cboxTopLeft" style="float: left;"></div>*/
/*                     <div id="dsidx_cboxTopCenter" style="float: left;"></div>*/
/*                     <div id="dsidx_cboxTopRight" style="float: left;"></div>*/
/*                 </div>*/
/*                 <div style="clear: left;">*/
/*                     <div id="dsidx_cboxMiddleLeft" style="float: left;"></div>*/
/*                     <div id="dsidx_cboxContent" style="float: left;">*/
/*                         <div id="dsidx_cboxTitle" style="float: left;"></div>*/
/*                         <div id="dsidx_cboxCurrent" style="float: left;"></div>*/
/*                         <button type="button" id="dsidx_cboxPrevious"></button><button type="button" id="dsidx_cboxNext"></button><button id="dsidx_cboxSlideshow"></button>*/
/*                         <div id="dsidx_cboxLoadingOverlay" style="float: left;"></div>*/
/*                         <div id="dsidx_cboxLoadingGraphic" style="float: left;"></div>*/
/*                     </div>*/
/*                     <div id="dsidx_cboxMiddleRight" style="float: left;"></div>*/
/*                 </div>*/
/*                 <div style="clear: left;">*/
/*                     <div id="dsidx_cboxBottomLeft" style="float: left;"></div>*/
/*                     <div id="dsidx_cboxBottomCenter" style="float: left;"></div>*/
/*                     <div id="dsidx_cboxBottomRight" style="float: left;"></div>*/
/*                 </div>*/
/*             </div>*/
/*             <div style="position: absolute; width: 9999px; visibility: hidden; display: none;"></div>*/
/*         </div>*/
/*         <div id="wp-a11y-speak-polite" role="status" aria-live="polite" aria-relevant="additions text" aria-atomic="true" class="screen-reader-text wp-a11y-speak-region"></div>*/
/*         <div id="wp-a11y-speak-assertive" role="alert" aria-live="assertive" aria-relevant="additions text" aria-atomic="true" class="screen-reader-text wp-a11y-speak-region"></div>*/
/*     </body>*/
/* {% endblock %}*/
