<?php

/* authent_modal.twig */
class __TwigTemplate_1126f18387fe65f5c44ada495f3bc8a39b801eeb6f710066ff551cfe8898b2b2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_78a7f51b97bc643df6a96c4c41da0b88d625de35624d5761d5eea7f344c1ca72 = $this->env->getExtension("native_profiler");
        $__internal_78a7f51b97bc643df6a96c4c41da0b88d625de35624d5761d5eea7f344c1ca72->enter($__internal_78a7f51b97bc643df6a96c4c41da0b88d625de35624d5761d5eea7f344c1ca72_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "authent_modal.twig"));

        // line 1
        echo "<div id=\"login-modal\" class=\"forms-modal modal hide fade\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">

    <div class=\"modal-header\">
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
        <p>You need to log in to use member only features.</p>
    </div>

    <!-- start of modal body -->
    <div class=\"modal-body\">

        <!-- login section -->
        <div class=\"login-section modal-section\">
            <h4>Login</h4>
            <form id=\"login-form\" class=\"login-form\" action=\"http://realhomes.inspirythemes.biz/wp-admin/admin-ajax.php\" method=\"post\" enctype=\"multipart/form-data\">
                <div class=\"form-option\">
                    <label for=\"username\">User Name<span>*</span></label>
                    <input id=\"username\" name=\"log\" type=\"text\" class=\"required\" title=\"* Provide user name!\" autofocus required/>
                </div>
                <div class=\"form-option\">
                    <label for=\"password\">Password<span>*</span></label>
                    <input id=\"password\" name=\"pwd\" type=\"password\" class=\"required\" title=\"* Provide password!\" required/>
                </div>
                <input type=\"hidden\" name=\"action\" value=\"inspiry_ajax_login\" />
                <input type=\"hidden\" id=\"inspiry-secure-login\" name=\"inspiry-secure-login\" value=\"72515f4468\" /><input type=\"hidden\" name=\"_wp_http_referer\" value=\"/property/villa-in-hialeah-dade-county/\" /><input type=\"hidden\" name=\"redirect_to\" value=\"http://realhomes.inspirythemes.biz/property/villa-in-hialeah-dade-county/\" />                <input type=\"hidden\" name=\"user-cookie\" value=\"1\" />
                <input type=\"submit\" id=\"login-button\" name=\"submit\" value=\"Log in\" class=\"real-btn login-btn\" />
                <img id=\"login-loader\" class=\"modal-loader\" src=\"http://realhomes.inspirythemes.biz/wp-content/themes/realhomes/images/ajax-loader.gif\" alt=\"Working...\">
                <div>
                    <div id=\"login-message\" class=\"modal-message\"></div>
                    <div id=\"login-error\" class=\"modal-error\"></div>
                </div>
            </form>
            <p>
                                    <a class=\"activate-section\" data-section=\"register-section\" href=\"#\">Register Here</a>
                    <span class=\"divider\">-</span>
                                <a class=\"activate-section\" data-section=\"forgot-section\" href=\"#\">Forgot Password</a>
            </p>
        </div>

        <!-- forgot section -->
        <div class=\"forgot-section modal-section\">
            <h4>Reset Password</h4>
            <form action=\"http://realhomes.inspirythemes.biz/wp-admin/admin-ajax.php\" id=\"forgot-form\"  method=\"post\" enctype=\"multipart/form-data\">
                <div class=\"form-option\">
                    <label for=\"reset_username_or_email\">Username or Email<span>*</span></label>
                    <input id=\"reset_username_or_email\" name=\"reset_username_or_email\" type=\"text\" class=\"required\" title=\"* Provide username or email!\" required/>
                </div>
                <input type=\"hidden\" name=\"action\" value=\"inspiry_ajax_forgot\" />
                <input type=\"hidden\" name=\"user-cookie\" value=\"1\" />
                <input type=\"submit\"  id=\"forgot-button\" name=\"user-submit\" value=\"Reset Password\" class=\"real-btn register-btn\" />
\t            <img id=\"forgot-loader\" class=\"modal-loader\" src=\"http://realhomes.inspirythemes.biz/wp-content/themes/realhomes/images/ajax-loader.gif\" alt=\"Working...\">
                <input type=\"hidden\" id=\"inspiry-secure-reset\" name=\"inspiry-secure-reset\" value=\"a5876dde24\" /><input type=\"hidden\" name=\"_wp_http_referer\" value=\"/property/villa-in-hialeah-dade-county/\" />                <div>
                    <div id=\"forgot-message\" class=\"modal-message\"></div>
                    <div id=\"forgot-error\" class=\"modal-error\"></div>
                </div>
            </form>
            <p>
                <a class=\"activate-section\" data-section=\"login-section\" href=\"#\">Login Here</a>
                                    <span class=\"divider\">-</span>
                    <a class=\"activate-section\" data-section=\"register-section\" href=\"#\">Register Here</a>
                            </p>
        </div>

                    <!-- register section -->
            <div class=\"register-section modal-section\">
                <h4>Register</h4>
                <form action=\"http://realhomes.inspirythemes.biz/wp-admin/admin-ajax.php\" id=\"register-form\"  method=\"post\" enctype=\"multipart/form-data\">

                    <div class=\"form-option\">
                        <label for=\"register_username\" class=\"\">User Name<span>*</span></label>
                        <input id=\"register_username\" name=\"register_username\" type=\"text\" class=\"required\"
                               title=\"* Provide user name!\" required/>
                    </div>

                    <div class=\"form-option\">
                        <label for=\"register_email\" class=\"\">Email<span>*</span></label>
                        <input id=\"register_email\" name=\"register_email\" type=\"text\" class=\"email required\"
                               title=\"* Provide valid email address!\" required/>
                    </div>

                    <input type=\"hidden\" name=\"user-cookie\" value=\"1\" />
                    <input type=\"submit\" id=\"register-button\" name=\"user-submit\" value=\"Register\" class=\"real-btn register-btn\" />
\t                <img id=\"register-loader\" class=\"modal-loader\" src=\"http://realhomes.inspirythemes.biz/wp-content/themes/realhomes/images/ajax-loader.gif\" alt=\"Working...\">
                    <input type=\"hidden\" name=\"action\" value=\"inspiry_ajax_register\" />
                    <input type=\"hidden\" id=\"inspiry-secure-register\" name=\"inspiry-secure-register\" value=\"f998f8232b\" /><input type=\"hidden\" name=\"_wp_http_referer\" value=\"/property/villa-in-hialeah-dade-county/\" /><input type=\"hidden\" name=\"redirect_to\" value=\"http://realhomes.inspirythemes.biz/property/villa-in-hialeah-dade-county/\" />
                    <div>
                        <div id=\"register-message\" class=\"modal-message\"></div>
                        <div id=\"register-error\" class=\"modal-error\"></div>
                    </div>

                </form>
                <p>
                    <a class=\"activate-section\" data-section=\"login-section\" href=\"#\">Login Here</a>
                    <span class=\"divider\">-</span>
                    <a class=\"activate-section\" data-section=\"forgot-section\" href=\"#\">Forgot Password</a>
                </p>
            </div>
        
    </div>
    <!-- end of modal-body -->

</div>
";
        
        $__internal_78a7f51b97bc643df6a96c4c41da0b88d625de35624d5761d5eea7f344c1ca72->leave($__internal_78a7f51b97bc643df6a96c4c41da0b88d625de35624d5761d5eea7f344c1ca72_prof);

    }

    public function getTemplateName()
    {
        return "authent_modal.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div id="login-modal" class="forms-modal modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">*/
/* */
/*     <div class="modal-header">*/
/*         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>*/
/*         <p>You need to log in to use member only features.</p>*/
/*     </div>*/
/* */
/*     <!-- start of modal body -->*/
/*     <div class="modal-body">*/
/* */
/*         <!-- login section -->*/
/*         <div class="login-section modal-section">*/
/*             <h4>Login</h4>*/
/*             <form id="login-form" class="login-form" action="http://realhomes.inspirythemes.biz/wp-admin/admin-ajax.php" method="post" enctype="multipart/form-data">*/
/*                 <div class="form-option">*/
/*                     <label for="username">User Name<span>*</span></label>*/
/*                     <input id="username" name="log" type="text" class="required" title="* Provide user name!" autofocus required/>*/
/*                 </div>*/
/*                 <div class="form-option">*/
/*                     <label for="password">Password<span>*</span></label>*/
/*                     <input id="password" name="pwd" type="password" class="required" title="* Provide password!" required/>*/
/*                 </div>*/
/*                 <input type="hidden" name="action" value="inspiry_ajax_login" />*/
/*                 <input type="hidden" id="inspiry-secure-login" name="inspiry-secure-login" value="72515f4468" /><input type="hidden" name="_wp_http_referer" value="/property/villa-in-hialeah-dade-county/" /><input type="hidden" name="redirect_to" value="http://realhomes.inspirythemes.biz/property/villa-in-hialeah-dade-county/" />                <input type="hidden" name="user-cookie" value="1" />*/
/*                 <input type="submit" id="login-button" name="submit" value="Log in" class="real-btn login-btn" />*/
/*                 <img id="login-loader" class="modal-loader" src="http://realhomes.inspirythemes.biz/wp-content/themes/realhomes/images/ajax-loader.gif" alt="Working...">*/
/*                 <div>*/
/*                     <div id="login-message" class="modal-message"></div>*/
/*                     <div id="login-error" class="modal-error"></div>*/
/*                 </div>*/
/*             </form>*/
/*             <p>*/
/*                                     <a class="activate-section" data-section="register-section" href="#">Register Here</a>*/
/*                     <span class="divider">-</span>*/
/*                                 <a class="activate-section" data-section="forgot-section" href="#">Forgot Password</a>*/
/*             </p>*/
/*         </div>*/
/* */
/*         <!-- forgot section -->*/
/*         <div class="forgot-section modal-section">*/
/*             <h4>Reset Password</h4>*/
/*             <form action="http://realhomes.inspirythemes.biz/wp-admin/admin-ajax.php" id="forgot-form"  method="post" enctype="multipart/form-data">*/
/*                 <div class="form-option">*/
/*                     <label for="reset_username_or_email">Username or Email<span>*</span></label>*/
/*                     <input id="reset_username_or_email" name="reset_username_or_email" type="text" class="required" title="* Provide username or email!" required/>*/
/*                 </div>*/
/*                 <input type="hidden" name="action" value="inspiry_ajax_forgot" />*/
/*                 <input type="hidden" name="user-cookie" value="1" />*/
/*                 <input type="submit"  id="forgot-button" name="user-submit" value="Reset Password" class="real-btn register-btn" />*/
/* 	            <img id="forgot-loader" class="modal-loader" src="http://realhomes.inspirythemes.biz/wp-content/themes/realhomes/images/ajax-loader.gif" alt="Working...">*/
/*                 <input type="hidden" id="inspiry-secure-reset" name="inspiry-secure-reset" value="a5876dde24" /><input type="hidden" name="_wp_http_referer" value="/property/villa-in-hialeah-dade-county/" />                <div>*/
/*                     <div id="forgot-message" class="modal-message"></div>*/
/*                     <div id="forgot-error" class="modal-error"></div>*/
/*                 </div>*/
/*             </form>*/
/*             <p>*/
/*                 <a class="activate-section" data-section="login-section" href="#">Login Here</a>*/
/*                                     <span class="divider">-</span>*/
/*                     <a class="activate-section" data-section="register-section" href="#">Register Here</a>*/
/*                             </p>*/
/*         </div>*/
/* */
/*                     <!-- register section -->*/
/*             <div class="register-section modal-section">*/
/*                 <h4>Register</h4>*/
/*                 <form action="http://realhomes.inspirythemes.biz/wp-admin/admin-ajax.php" id="register-form"  method="post" enctype="multipart/form-data">*/
/* */
/*                     <div class="form-option">*/
/*                         <label for="register_username" class="">User Name<span>*</span></label>*/
/*                         <input id="register_username" name="register_username" type="text" class="required"*/
/*                                title="* Provide user name!" required/>*/
/*                     </div>*/
/* */
/*                     <div class="form-option">*/
/*                         <label for="register_email" class="">Email<span>*</span></label>*/
/*                         <input id="register_email" name="register_email" type="text" class="email required"*/
/*                                title="* Provide valid email address!" required/>*/
/*                     </div>*/
/* */
/*                     <input type="hidden" name="user-cookie" value="1" />*/
/*                     <input type="submit" id="register-button" name="user-submit" value="Register" class="real-btn register-btn" />*/
/* 	                <img id="register-loader" class="modal-loader" src="http://realhomes.inspirythemes.biz/wp-content/themes/realhomes/images/ajax-loader.gif" alt="Working...">*/
/*                     <input type="hidden" name="action" value="inspiry_ajax_register" />*/
/*                     <input type="hidden" id="inspiry-secure-register" name="inspiry-secure-register" value="f998f8232b" /><input type="hidden" name="_wp_http_referer" value="/property/villa-in-hialeah-dade-county/" /><input type="hidden" name="redirect_to" value="http://realhomes.inspirythemes.biz/property/villa-in-hialeah-dade-county/" />*/
/*                     <div>*/
/*                         <div id="register-message" class="modal-message"></div>*/
/*                         <div id="register-error" class="modal-error"></div>*/
/*                     </div>*/
/* */
/*                 </form>*/
/*                 <p>*/
/*                     <a class="activate-section" data-section="login-section" href="#">Login Here</a>*/
/*                     <span class="divider">-</span>*/
/*                     <a class="activate-section" data-section="forgot-section" href="#">Forgot Password</a>*/
/*                 </p>*/
/*             </div>*/
/*         */
/*     </div>*/
/*     <!-- end of modal-body -->*/
/* */
/* </div>*/
/* */
