<?php

/* list_layout.html.twig */
class __TwigTemplate_0835746172535821a1ba4e59de71a29f5c68482667d0b848fe5f5f6cf9397590 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d7e41f0c09eaf00985de207851cf6cee6fffbcd69a47c70cb5b5fbf0b22edfd1 = $this->env->getExtension("native_profiler");
        $__internal_d7e41f0c09eaf00985de207851cf6cee6fffbcd69a47c70cb5b5fbf0b22edfd1->enter($__internal_d7e41f0c09eaf00985de207851cf6cee6fffbcd69a47c70cb5b5fbf0b22edfd1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "list_layout.html.twig"));

        // line 1
        echo "                            <div class=\"list-container clearfix\">
                                <div class=\"sort-controls\">
                                    <strong>Sort By:</strong>
                                    &nbsp;
                                    <select name=\"sort-properties\" id=\"sort-properties\">
                                        <option value=\"default\">Default Order</option>
                                        <option value=\"price-asc\">Price Low to High</option>
                                        <option value=\"price-desc\">Price High to Low</option>
                                        <option value=\"date-asc\">Date Old to New</option>
                                        <option value=\"date-desc\">Date New to Old</option>
                                    </select>
                                </div>
                                <div class=\"property-item-wrapper\">
                                    ";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")));
        foreach ($context['_seq'] as $context["_key"] => $context["imobil"]) {
            // line 15
            echo "                                    <article class=\"property-item clearfix\">
                                        <h4><a href=\"imobil/getDetails/";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($context["imobil"], "id", array()), "html", null, true);
            echo "\">, ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["imobil"], "oras", array()), "html", null, true);
            echo "</a></h4>
                                        <figure>
                                            <a href=\"http://realhomes.inspirythemes.biz/property/villa-in-hialeah-dade-county/\">
                                            <img width=\"244\" height=\"163\" src=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/front/property-08-244x163.jpg"), "html", null, true);
            echo "\" class=\"attachment-property-thumb-image size-property-thumb-image wp-post-image\" alt=\"property 08\">            </a>
                                        </figure>
                                        <div class=\"detail\">
                                            <h5 class=\"price\">
                                                ";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["imobil"], "pretInitial", array()), "html", null, true);
            echo "<small> - ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["imobil"], "tipImobil", array()), "html", null, true);
            echo "</small>            
                                            </h5>
                                            <p>";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["imobil"], "descriere", array()), "html", null, true);
            echo "</p>
                                            <a class=\"more-details\" href=\"imobil/getDetails/{imobil.id}\">Detalii <i class=\"fa fa-caret-right\"></i></a>
                                        </div>
                                        <div class=\"property-meta\">
                                            <span>
                                                ";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["imobil"], "arieUtila", array()), "html", null, true);
            echo "m<sup style=\"font-size:0.7em;\">2</sup>
                                            </span>
                                            <span>
                                                4&nbsp;Bedrooms
                                            </span>
                                            <span>
                                                3&nbsp;Bathrooms
                                            </span>
                                            <span>
                                                2&nbsp;Garages
                                            </span>
                                        </div>
                                    </article>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['imobil'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "                                </div>
                            </div>";
        
        $__internal_d7e41f0c09eaf00985de207851cf6cee6fffbcd69a47c70cb5b5fbf0b22edfd1->leave($__internal_d7e41f0c09eaf00985de207851cf6cee6fffbcd69a47c70cb5b5fbf0b22edfd1_prof);

    }

    public function getTemplateName()
    {
        return "list_layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 44,  74 => 30,  66 => 25,  59 => 23,  52 => 19,  44 => 16,  41 => 15,  37 => 14,  22 => 1,);
    }
}
/*                             <div class="list-container clearfix">*/
/*                                 <div class="sort-controls">*/
/*                                     <strong>Sort By:</strong>*/
/*                                     &nbsp;*/
/*                                     <select name="sort-properties" id="sort-properties">*/
/*                                         <option value="default">Default Order</option>*/
/*                                         <option value="price-asc">Price Low to High</option>*/
/*                                         <option value="price-desc">Price High to Low</option>*/
/*                                         <option value="date-asc">Date Old to New</option>*/
/*                                         <option value="date-desc">Date New to Old</option>*/
/*                                     </select>*/
/*                                 </div>*/
/*                                 <div class="property-item-wrapper">*/
/*                                     {% for imobil in pagination %}*/
/*                                     <article class="property-item clearfix">*/
/*                                         <h4><a href="imobil/getDetails/{{imobil.id}}">, {{imobil.oras}}</a></h4>*/
/*                                         <figure>*/
/*                                             <a href="http://realhomes.inspirythemes.biz/property/villa-in-hialeah-dade-county/">*/
/*                                             <img width="244" height="163" src="{{asset("bundles/front/property-08-244x163.jpg")}}" class="attachment-property-thumb-image size-property-thumb-image wp-post-image" alt="property 08">            </a>*/
/*                                         </figure>*/
/*                                         <div class="detail">*/
/*                                             <h5 class="price">*/
/*                                                 {{imobil.pretInitial}}<small> - {{imobil.tipImobil}}</small>            */
/*                                             </h5>*/
/*                                             <p>{{imobil.descriere}}</p>*/
/*                                             <a class="more-details" href="imobil/getDetails/{imobil.id}">Detalii <i class="fa fa-caret-right"></i></a>*/
/*                                         </div>*/
/*                                         <div class="property-meta">*/
/*                                             <span>*/
/*                                                 {{imobil.arieUtila}}m<sup style="font-size:0.7em;">2</sup>*/
/*                                             </span>*/
/*                                             <span>*/
/*                                                 4&nbsp;Bedrooms*/
/*                                             </span>*/
/*                                             <span>*/
/*                                                 3&nbsp;Bathrooms*/
/*                                             </span>*/
/*                                             <span>*/
/*                                                 2&nbsp;Garages*/
/*                                             </span>*/
/*                                         </div>*/
/*                                     </article>*/
/*                                     {% endfor %}*/
/*                                 </div>*/
/*                             </div>*/
